import { _decorator, Component, Node } from "cc";
const { ccclass, property } = _decorator;

@ccclass("PlayerController")
export class PlayerController extends Component {
  // [1]
  // dummy = '';

  // [2]
  // @property
  // serializableDummy = 0;

  start() {
    // [3]
  }

  // update (deltaTime: number) {
  //     // [4]
  // }
}
