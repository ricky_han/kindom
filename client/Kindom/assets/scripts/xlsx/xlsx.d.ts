// Generated from map.xlsx

declare type Building = {
	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 建造页签1 生产2 军事3 奇迹
	 */
	readonly sheetType: number;

	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 建造消耗
	 */
	readonly costMeterials: any;

	/**
	 * 资源产出及速度 10s为单位
	 */
	readonly outMeterials: any;

	/**
	 * 资源消耗及速度 10s为单位
	 */
	readonly needMeterials: any;

	/**
	 * 描述
	 */
	readonly des: string;

	/**
	 * 建筑占比,根据比例去建造对应建筑
	 */
	readonly ratio: number;

	/**
	 * 每小时产生多少个只用来显示,10秒容易显示出来小数
	 */
	readonly speedShow: any;
};

// Generated from map.xlsx

declare type CityName = {
	/**
	 * 城市名称
	 */
	readonly cityName: string;
};

// Generated from map.xlsx

declare type Material = {
	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 系列
	 */
	readonly type: string;

	/**
	 * 系列名称
	 */
	readonly typeName: string;

	/**
	 * 价值
	 */
	readonly price: number;

	/**
	 * 资源上限
	 */
	readonly maxLimit: number;

	/**
	 * 说明
	 */
	readonly des: string;
};

// Generated from map.xlsx

declare type Race = {
	/**
	 * 种族名称
	 */
	readonly raceName: string;
};

// Generated from map.xlsx

declare type Soldier = {
	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 种族
	 */
	readonly race: number;

	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 消耗资源速度 10s为单位
	 */
	readonly eatMeterials: any;

	/**
	 * 描述
	 */
	readonly des: string;

	/**
	 * ai出兵的时候判断派遣到攻击城还是防守城依据：攻击兵还是防守兵1 攻击2 防守3 均衡
	 */
	readonly attackOrDefense: number;

	/**
	 * 攻击类型PHYSICAL: 1;    FIRE: 2;    ELECTRIC: 3;    ICE: 4;    LIGHT: 5;    DARK: 6;
	 */
	readonly attackType: number;

	/**
	 * 攻击
	 */
	readonly attack: number;

	/**
	 * 物防
	 */
	readonly physicalDef: number;

	/**
	 * 火系抵抗
	 */
	readonly fireDef: number;

	/**
	 * 电系抵抗
	 */
	readonly electricDef: number;

	/**
	 * 冰系抵抗
	 */
	readonly iceDef: number;

	/**
	 * 光系抵抗
	 */
	readonly lightDef: number;

	/**
	 * 暗系抵抗
	 */
	readonly darkDef: number;

	/**
	 * 运载量
	 */
	readonly load: number;

	/**
	 * 战斗力-出战单位1000方可出战
	 */
	readonly battleNum: number;
};

// Generated from map.xlsx

declare type Terrain = {
	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 地形指定建筑类型
	 */
	readonly buildingIds: number[];

	/**
	 * 描述
	 */
	readonly des: string;
};

declare const XLSX: {
	generationDate: Date;

	/**
	 * Generated from map.xlsx
	 */
	readonly BUILDING: { readonly [id: string]: Building };

	/**
	 * Generated from map.xlsx
	 */
	readonly CITY_NAME: { readonly [id: string]: CityName };

	/**
	 * Generated from map.xlsx
	 */
	readonly MATERIAL: { readonly [id: string]: Material };

	/**
	 * Generated from map.xlsx
	 */
	readonly RACE: { readonly [id: string]: Race };

	/**
	 * Generated from map.xlsx
	 */
	readonly SOLDIER: { readonly [id: string]: Soldier };

	/**
	 * Generated from map.xlsx
	 */
	readonly TERRAIN: { readonly [id: string]: Terrain };

	/**
	 * 将参数以“-”连接作为ID在指定sheet中找到匹配的行
	 * @param sheet XLSX中的表
	 * @param args 将被“-”连接的参数
	 * @returns 匹配行的数据
	 */
	readonly findOne: (sheet: any, ...args: any[]) => any;
};
