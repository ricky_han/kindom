window.XLSX = {
	generationDate: new Date(1649343238637),

	/**
	 * Generated from map.xlsx
	 */
	BUILDING: {
		["building-1"]: {
			name: "薄田",
			sheetType: 1,
			res: "building-1",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-2": 1 },
			des: "产出粮食为主的建筑，建造极其容易，产出少量粮食",
			ratio: 0,
			speedShow: { "material-2": 360 }
		},
		["building-2"]: {
			name: "水田",
			sheetType: 1,
			res: "building-2",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-2": 3 },
			des: "产出粮食，产量比薄田多不少",
			ratio: 0,
			speedShow: { "material-2": 1080 }
		},
		["building-3"]: {
			name: "肥沃之田",
			sheetType: 1,
			res: "building-3",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-2": 10 },
			des: "产出大量粮食，产量很大，但是需要很久才能弄好",
			ratio: 0,
			speedShow: { "material-2": 3600 }
		},
		["building-4"]: {
			name: "灵梯田",
			sheetType: 1,
			res: "building-4",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-2": 6, "material-7": 0.006 },
			des: "产出中量粮食，同时有几率产生稀有药草-黑莲花（灵气不足产量极低，应该如此）",
			ratio: 0,
			speedShow: { "material-2": 2 }
		},
		["building-5"]: {
			name: "铁炉",
			sheetType: 1,
			res: "building-5",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-3": 0.03 },
			des: "产出铁",
			ratio: 0,
			speedShow: { "material-3": 11 }
		},
		["building-6"]: {
			name: "铁场",
			sheetType: 1,
			res: "building-6",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-3": 0.1 },
			des: "产出中量铁",
			ratio: 0,
			speedShow: { "material-3": 36 }
		},
		["building-7"]: {
			name: "大铁厂",
			sheetType: 1,
			res: "building-7",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-3": 0.3 },
			des: "大量铁",
			ratio: 0,
			speedShow: { "material-3": 108 }
		},
		["building-8"]: {
			name: "林地",
			sheetType: 1,
			res: "building-8",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-4": 0.2 },
			des: "产出木头",
			ratio: 0,
			speedShow: { "material-4": 72 }
		},
		["building-9"]: {
			name: "林场",
			sheetType: 1,
			res: "building-9",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-4": 0.6 },
			des: "中量木材",
			ratio: 0,
			speedShow: { "material-4": 216 }
		},
		["building-10"]: {
			name: "大林场",
			sheetType: 1,
			res: "building-10",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-4": 2 },
			des: "大量木材",
			ratio: 0,
			speedShow: { "material-4": 720 }
		},
		["building-11"]: {
			name: "采石坑",
			sheetType: 1,
			res: "building-11",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-5": 0.6 },
			des: "少量石头",
			ratio: 0,
			speedShow: { "material-5": 216 }
		},
		["building-12"]: {
			name: "采石作坊",
			sheetType: 1,
			res: "building-12",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-5": 2 },
			des: "中量石头",
			ratio: 0,
			speedShow: { "material-5": 720 }
		},
		["building-13"]: {
			name: "采石大场",
			sheetType: 1,
			res: "building-13",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-5": 6 },
			des: "大量石头",
			ratio: 0,
			speedShow: { "material-5": 2160 }
		},
		["building-14"]: {
			name: "水晶矿",
			sheetType: 1,
			res: "building-14",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-10": 0.003 },
			des: "能产出珍贵的水晶矿（感觉应该只有特定地块才可以造）",
			ratio: 0,
			speedShow: { "material-10": 1 }
		},
		["building-15"]: {
			name: "大墓地",
			sheetType: 1,
			res: "building-15",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-6": 0.05 },
			des: "产生少量骨骼",
			ratio: 0,
			speedShow: { "material-6": 18 }
		},
		["building-16"]: {
			name: "坟场",
			sheetType: 1,
			res: "building-16",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-6": 0.05 },
			des: "产生骨骼",
			ratio: 0,
			speedShow: { "material-6": 18 }
		},
		["building-17"]: {
			name: "亡者之城",
			sheetType: 1,
			res: "building-17",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-6": 0.05, "material-7": 0.006 },
			des: "大量骨骼，并有机会产生瘟疫花",
			ratio: 0,
			speedShow: { "material-6": 2 }
		},
		["building-18"]: {
			name: "难民营",
			sheetType: 1,
			res: "building-18",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-1": 0.1 },
			des: "每天都能收拢一些无家可归的难民",
			ratio: 0,
			speedShow: { "material-1": 36 }
		},
		["building-19"]: {
			name: "木质联排房屋",
			sheetType: 1,
			res: "building-19",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-1": 0.3 },
			des: "吸引大量的难民来申请居住",
			ratio: 0,
			speedShow: { "material-1": 108 }
		},
		["building-20"]: {
			name: "联排别墅",
			sheetType: 1,
			res: "building-20",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-1": 1 },
			des: "提供舒适的住所，能快速大量的吸引难民，特别是过去有钱的难民",
			ratio: 0,
			speedShow: { "material-1": 360 }
		},
		["building-100"]: {
			name: "冶炼炉",
			sheetType: 1,
			res: "building-100",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-8": 0.01 },
			des: "消耗较多的铁炼制坚硬的钢",
			ratio: 0,
			speedShow: { "material-8": 4 }
		},
		["building-101"]: {
			name: "高炉",
			sheetType: 1,
			res: "building-101",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-9": 0.005 },
			des: "消耗极大量木头和铁炼制珍贵的高碳铁",
			ratio: 0,
			speedShow: { "material-9": 2 }
		},
		["building-102"]: {
			name: "水晶转化器",
			sheetType: 1,
			res: "building-102",
			costMeterials: { "material-1": 1 },
			outMeterials: { "material-10": 0.003 },
			des: "消耗大量石头，木材炼制珍贵的水晶",
			ratio: 0,
			speedShow: { "material-10": 1 }
		},
		["building-200"]: {
			name: "民兵房",
			sheetType: 2,
			res: "building-200",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-1": 2 },
			needMeterials: { "material-1": 8.4, "material-2": 84 },
			des: "民兵",
			ratio: 0,
			speedShow: { "soldier-1": 720 }
		},
		["building-201"]: {
			name: "步兵房",
			sheetType: 2,
			res: "building-201",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-2": 0.769 },
			needMeterials: { "material-1": 6.65, "material-3": 2.22 },
			des: "步兵",
			ratio: 0,
			speedShow: { "soldier-2": 276 }
		},
		["building-202"]: {
			name: "十字军房",
			sheetType: 2,
			res: "building-202",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-3": 0.233 },
			needMeterials: { "material-1": 6.03, "material-8": 0.67, "material-4": 16.07 },
			des: "十字军",
			ratio: 0,
			speedShow: { "soldier-3": 83 }
		},
		["building-203"]: {
			name: "飞行器房",
			sheetType: 2,
			res: "building-203",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-4": 0.588 },
			needMeterials: { "material-1": 5.93, "material-8": 0.66, "material-4": 15.81 },
			des: "飞行器",
			ratio: 0,
			speedShow: { "soldier-4": 211 }
		},
		["building-204"]: {
			name: "钢铁机器人房",
			sheetType: 2,
			res: "building-204",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-5": 0.145 },
			needMeterials: { "material-1": 6.26, "material-8": 0.7, "material-5": 5.57 },
			des: "钢铁机器人",
			ratio: 0,
			speedShow: { "soldier-5": 52 }
		},
		["building-205"]: {
			name: "黑铁作战兵器房",
			sheetType: 2,
			res: "building-205",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-6": 0.055 },
			needMeterials: { "material-9": 0.06, "material-4": 10.09, "material-5": 4.48 },
			des: "黑铁作战兵器",
			ratio: 0,
			speedShow: { "soldier-6": 19 }
		},
		["building-206"]: {
			name: "轻骑兵房",
			sheetType: 2,
			res: "building-206",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-7": 0.4 },
			needMeterials: { "material-1": 9.22, "material-4": 18.43, "material-2": 122.88 },
			des: "轻骑兵",
			ratio: 0,
			speedShow: { "soldier-7": 144 }
		},
		["building-207"]: {
			name: "皇家骑兵房",
			sheetType: 2,
			res: "building-207",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-8": 0.104 },
			needMeterials: { "material-1": 8.35, "material-4": 16.71, "material-2": 111.38 },
			des: "皇家骑兵",
			ratio: 0,
			speedShow: { "soldier-8": 37 }
		},
		["building-208"]: {
			name: "火焰法师房",
			sheetType: 2,
			res: "building-208",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-9": 0.455 },
			needMeterials: { "material-8": 1.39, "material-4": 24.95, "material-5": 11.09 },
			des: "火焰法师",
			ratio: 0,
			speedShow: { "soldier-9": 163 }
		},
		["building-209"]: {
			name: "火魔导房",
			sheetType: 2,
			res: "building-209",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-10": 0.244 },
			needMeterials: { "material-9": 0.14, "material-4": 25.02, "material-5": 11.12 },
			des: "火魔导",
			ratio: 0,
			speedShow: { "soldier-10": 87 }
		},
		["building-210"]: {
			name: "水元素房",
			sheetType: 2,
			res: "building-210",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-11": 0.4 },
			needMeterials: { "material-4": 24.62, "material-2": 123.12 },
			des: "水元素",
			ratio: 0,
			speedShow: { "soldier-11": 144 }
		},
		["building-211"]: {
			name: "魅魔房",
			sheetType: 2,
			res: "building-211",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-12": 0.286 },
			needMeterials: { "material-1": 11.11 },
			des: "魅魔",
			ratio: 0,
			speedShow: { "soldier-12": 102 }
		},
		["building-212"]: {
			name: "天使房",
			sheetType: 2,
			res: "building-212",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-13": 0.08 },
			needMeterials: { "material-1": 11.29 },
			des: "天使",
			ratio: 0,
			speedShow: { "soldier-13": 28 }
		},
		["building-301"]: {
			name: "营寨",
			sheetType: 2,
			res: "building-301",
			costMeterials: { "material-1": 1 },
			des: "简约营寨，消耗大量木头",
			ratio: 0
		},
		["building-302"]: {
			name: "要塞",
			sheetType: 2,
			res: "building-302",
			costMeterials: { "material-1": 1 },
			des: "坚固的营寨。消耗石头",
			ratio: 0
		},
		["building-303"]: {
			name: "城堡",
			sheetType: 2,
			res: "building-303",
			costMeterials: { "material-1": 1 },
			des: "非常坚固的营寨。消耗大量石头。容纳更多的人",
			ratio: 0
		},
		["building-304"]: {
			name: "元素高地",
			sheetType: 2,
			res: "building-304",
			costMeterials: { "material-1": 1 },
			des: "发挥元素们的防御能力",
			ratio: 0
		},
		["building-305"]: {
			name: "奥术尖塔",
			sheetType: 2,
			res: "building-305",
			costMeterials: { "material-1": 1 },
			des: "法师厉害",
			ratio: 0
		},
		["building-306"]: {
			name: "机械城堡",
			sheetType: 2,
			res: "building-306",
			costMeterials: { "material-1": 1 },
			des: "机器部队增强，耗大量黑铁",
			ratio: 0
		},
		["building-307"]: {
			name: "冲锋马场",
			sheetType: 2,
			res: "building-307",
			costMeterials: { "material-1": 1 },
			des: "本城出发的骑兵冲锋能力增强",
			ratio: 0
		},
		["building-400"]: {
			name: "圣盾阿基里斯雕像",
			sheetType: 3,
			res: "building-400",
			costMeterials: { "material-1": 1 },
			des: "为周围城市提供防御点加成",
			ratio: 0
		},
		["building-401"]: {
			name: "战神阿帝斯神殿",
			sheetType: 3,
			res: "building-401",
			costMeterials: { "material-1": 1 },
			des: "对周围城市的进攻士兵提供鼓舞，提升兵营（步兵类）的训练速度",
			ratio: 0
		},
		["building-402"]: {
			name: "伊戈尔铁塔",
			sheetType: 3,
			res: "building-402",
			costMeterials: { "material-1": 1 },
			des: "行军加成，对周围城市行军的士兵，增加速度",
			ratio: 0
		},
		["building-403"]: {
			name: "丰饶之神雕像",
			sheetType: 3,
			res: "building-403",
			costMeterials: { "material-1": 1 },
			des: "周围的农田类建筑产量提升，难民汇集速度提升",
			ratio: 0
		},
		["building-404"]: {
			name: "火神雕像",
			sheetType: 3,
			res: "building-404",
			costMeterials: { "material-1": 1 },
			des: "周围铁，刚，黑铁速度加成",
			ratio: 0
		},
		["building-405"]: {
			name: "森林之神雕像",
			sheetType: 3,
			res: "building-405",
			costMeterials: { "material-1": 1 },
			des: "周围木头产量增加，黑莲花几率提升",
			ratio: 0
		},
		["building-406"]: {
			name: "匠神雕像",
			sheetType: 3,
			res: "building-406",
			costMeterials: { "material-1": 1 },
			des: "周围石头产量增加，车间的加工速度提升",
			ratio: 0
		},
		["building-407"]: {
			name: "天使利维尔雕像",
			sheetType: 3,
			res: "building-407",
			costMeterials: { "material-1": 1 },
			des: "神殿训练加速",
			ratio: 0
		},
		["building-408"]: {
			name: "守护者雕像",
			sheetType: 3,
			res: "building-408",
			costMeterials: { "material-1": 1 },
			des: "法师训练速度提升，水晶转化产量提升",
			ratio: 0
		},
		["building-409"]: {
			name: "幸运女神",
			sheetType: 3,
			res: "building-409",
			costMeterials: { "material-1": 1 },
			des: "战斗的好处微微提升",
			ratio: 0
		},
		["building-410"]: {
			name: "盗王",
			sheetType: 3,
			res: "building-410",
			costMeterials: { "material-1": 1 },
			des: "掠夺加成提升",
			ratio: 0
		},
		["building-0"]: {
			name: "文明议会",
			sheetType: 4,
			res: "building-0",
			costMeterials: { "material-1": 1 },
			des: "打开之后就是科技建筑界面，每个种族只会有一种，也只有一个，被占领则游戏失败。城镇大厅是人类的主城",
			ratio: 0
		},
		["building-2000"]: {
			name: "世界之树",
			sheetType: 4,
			res: "building-2000",
			costMeterials: { "material-1": 1 },
			des: "精灵族主城，精灵对木头依赖严重",
			ratio: 0
		},
		["building-3000"]: {
			name: "亡者之城",
			sheetType: 4,
			res: "building-3000",
			costMeterials: { "material-1": 1 },
			des: "亡灵族主城，亡灵依赖坟地，和大量的尸体",
			ratio: 0
		},
		["building-4000"]: {
			name: "蛮荒营地",
			sheetType: 4,
			res: "building-4000",
			costMeterials: { "material-1": 1 },
			des: "野兽族主城，野兽族拥有最强的属性，但是吃的异常的多，食物链顶端需要吃下层的单位",
			ratio: 0
		},
		["building-5000"]: {
			name: "地下城",
			sheetType: 4,
			res: "building-5000",
			costMeterials: { "material-1": 1 },
			des: "地下龙族主城，",
			ratio: 0
		},
		["building-6000"]: {
			name: "炼狱之池",
			sheetType: 4,
			res: "building-6000",
			costMeterials: { "material-1": 1 },
			des: "地狱族主城，地狱族不需要制造，全部都是自动生长类型；特长是黑暗和火焰",
			ratio: 0
		},
		["building-6201"]: {
			name: "恶魔熔炉",
			sheetType: 2,
			res: "building-6201",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-14": 2 },
			needMeterials: { "material-2": 109.2 },
			des: "造小恶魔",
			ratio: 10,
			speedShow: { "soldier-14": 720 }
		},
		["building-6202"]: {
			name: "洞穴",
			sheetType: 2,
			res: "building-6202",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-15": 1 },
			needMeterials: { "material-2": 84 },
			des: "造地狱犬",
			ratio: 7,
			speedShow: { "soldier-15": 360 }
		},
		["building-6203"]: {
			name: "末日守卫",
			sheetType: 2,
			res: "building-6203",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-16": 0.5 },
			needMeterials: { "material-11": 1.26, "material-12": 0.42 },
			des: "造末日守卫",
			ratio: 6,
			speedShow: { "soldier-16": 180 }
		},
		["building-6204"]: {
			name: "邪恶旅馆",
			sheetType: 2,
			res: "building-6204",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-17": 0.222 },
			needMeterials: { "material-1": 6.72 },
			des: "造魅魔",
			ratio: 5,
			speedShow: { "soldier-17": 79 }
		},
		["building-6205"]: {
			name: "虚空之门",
			sheetType: 2,
			res: "building-6205",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-18": 0.222 },
			needMeterials: { "material-1": 8.4, "material-12": 0.47 },
			des: "造恐惧魔王",
			ratio: 5,
			speedShow: { "soldier-18": 79 }
		},
		["building-6206"]: {
			name: "虚空法阵",
			sheetType: 2,
			res: "building-6206",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-19": 0.333 },
			needMeterials: { "material-4": 18.48, "material-2": 92.4 },
			des: "造虚空行者",
			ratio: 6,
			speedShow: { "soldier-19": 119 }
		},
		["building-6207"]: {
			name: "火焰工厂",
			sheetType: 2,
			res: "building-6207",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-20": 0.222 },
			needMeterials: { "material-5": 7.28, "material-8": 1.21 },
			des: "造地狱火",
			ratio: 3,
			speedShow: { "soldier-20": 79 }
		},
		["building-6208"]: {
			name: "深渊之林",
			sheetType: 2,
			res: "building-6208",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-21": 0.147 },
			needMeterials: { "material-8": 1.11, "material-12": 0.56 },
			des: "造深渊领主",
			ratio: 2,
			speedShow: { "soldier-21": 52 }
		},
		["building-6209"]: {
			name: "诅咒学院",
			sheetType: 2,
			res: "building-6209",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-22": 0.1 },
			needMeterials: { "material-12": 0.51, "material-6": 4.62 },
			des: "造诅咒之王",
			ratio: 2,
			speedShow: { "soldier-22": 36 }
		},
		["building-6210"]: {
			name: "扭曲之门",
			sheetType: 2,
			res: "building-6210",
			costMeterials: { "material-1": 1 },
			outMeterials: { "soldier-23": 0.067 },
			needMeterials: { "material-13": 0.02, "material-9": 0.12, "material-10": 0.01 },
			des: "造堕落泰坦",
			ratio: 1,
			speedShow: { "soldier-23": 24 }
		},
		["building-7000"]: {
			name: "高原塔楼",
			sheetType: 4,
			res: "building-7000",
			costMeterials: { "material-1": 1 },
			des: "巨人族主城，闪电一族",
			ratio: 0
		},
		["building-8000"]: {
			name: "能量漩涡",
			sheetType: 4,
			res: "building-8000",
			costMeterials: { "material-1": 1 },
			des: "元素族主城，物理最弱，",
			ratio: 0
		}
	},

	/**
	 * Generated from map.xlsx
	 */
	CITY_NAME: {
		["1"]: { cityName: "青岛" },
		["2"]: { cityName: "威海" },
		["3"]: { cityName: "烟台" },
		["4"]: { cityName: "济南" },
		["5"]: { cityName: "日照" },
		["6"]: { cityName: "德州" },
		["7"]: { cityName: "菏泽" },
		["8"]: { cityName: "泰安" },
		["9"]: { cityName: "菏泽" }
	},

	/**
	 * Generated from map.xlsx
	 */
	MATERIAL: {
		["material-1"]: {
			name: "难民",
			res: "material-1",
			type: "1",
			typeName: "人类",
			price: 10,
			maxLimit: 100000,
			des: "这个荒凉萧索的世界里，对于人类而言，难民到底是一种资源，还是一种负担？"
		},
		["material-2"]: {
			name: "粮食",
			res: "material-2",
			type: "2",
			typeName: "食物",
			price: 1,
			maxLimit: 100000,
			des: "大多数人类，野兽，等活物需要的生命维持物品"
		},
		["material-3"]: {
			name: "铁矿",
			res: "material-3",
			type: "3",
			typeName: "矿",
			price: 30,
			maxLimit: 100000,
			des: "常见的金属材料"
		},
		["material-4"]: {
			name: "木头",
			res: "material-4",
			type: "4",
			typeName: "木头",
			price: 5,
			maxLimit: 100000,
			des: "常见-基础"
		},
		["material-5"]: {
			name: "石料",
			res: "material-5",
			type: "5",
			typeName: "石头",
			price: 15,
			maxLimit: 100000,
			des: "常见-基础"
		},
		["material-6"]: {
			name: "骨骼",
			res: "material-6",
			type: "6",
			typeName: "尸体",
			price: 20,
			maxLimit: 100000,
			des: "亡灵族常用"
		},
		["material-7"]: {
			name: "黑莲花",
			res: "material-7",
			type: "4",
			typeName: "木头",
			price: 8000,
			maxLimit: 100000,
			des: "蕴含神奇力量的少见花朵，通常有xx守护，灵梯田也有极慢的速度生长而出"
		},
		["material-8"]: {
			name: "钢",
			res: "material-8",
			type: "3",
			typeName: "矿",
			price: 90,
			maxLimit: 100000,
			des: "铁的加强版，消耗木头和铁可炼成"
		},
		["material-9"]: {
			name: "黑铁",
			res: "material-9",
			type: "3",
			typeName: "矿",
			price: 900,
			maxLimit: 100000,
			des: "强度极高而耐热的铁，消耗大量铁炼成"
		},
		["material-10"]: {
			name: "水晶",
			res: "material-10",
			type: "5",
			typeName: "石头",
			price: 10000,
			maxLimit: 100000,
			des: "珍贵的矿石，打造高级兵常用，只要水晶地才可以产"
		},
		["material-11"]: {
			name: "尸体",
			res: "material-11",
			type: "6",
			typeName: "尸体",
			price: 60,
			maxLimit: 100000,
			des: "只能杀敌掉，且是血肉类单位，亡灵族常用"
		},
		["material-12"]: {
			name: "血液",
			res: "material-12",
			type: "6",
			typeName: "尸体",
			price: 180,
			maxLimit: 100000,
			des: "只能杀敌掉，且是血肉类单位，亡灵族，恶魔族常用"
		},
		["material-13"]: {
			name: "龙血",
			res: "material-13",
			type: "6",
			typeName: "尸体",
			price: 5000,
			maxLimit: 100000,
			des: "只能杀血肉类的龙单位才会掉，亡灵族，恶魔族，xx常用"
		},
		["material-14"]: {
			name: "冰之精华",
			res: "material-14",
			type: "6",
			typeName: "尸体",
			price: 15000,
			maxLimit: 100000,
			des: "只有冰系元素怪物才会掉的元素精华"
		},
		["material-15"]: {
			name: "火之精华",
			res: "material-15",
			type: "6",
			typeName: "尸体",
			price: 15000,
			maxLimit: 100000,
			des: "只有火系元素怪物才会掉的元素精华"
		},
		["material-16"]: {
			name: "闪电精华",
			res: "material-16",
			type: "6",
			typeName: "尸体",
			price: 15000,
			maxLimit: 100000,
			des: "只有电系元素怪物才会掉的元素精华"
		},
		["material-17"]: {
			name: "金币",
			res: "material-17",
			type: "8",
			typeName: "金币",
			price: 10000,
			maxLimit: 100000,
			des: "付费币，价值1毛钱"
		}
	},

	/**
	 * Generated from map.xlsx
	 */
	RACE: {
		["1"]: { raceName: "人类" },
		["2"]: { raceName: "精灵" },
		["3"]: { raceName: "亡灵" },
		["4"]: { raceName: "野兽" },
		["5"]: { raceName: "荒原" },
		["6"]: { raceName: "地下城" },
		["7"]: { raceName: "恶魔" },
		["8"]: { raceName: "巨人族" },
		["9"]: { raceName: "元素" }
	},

	/**
	 * Generated from map.xlsx
	 */
	SOLDIER: {
		["soldier-1"]: {
			name: "民兵",
			race: 1,
			res: "soldier-1",
			eatMeterials: { "material-2": 0.2 },
			des: "为了能有口饭吃，可以连命都不要。只要给他们吃的，他们什么都干",
			attackOrDefense: 1,
			attackType: 1,
			attack: 13,
			physicalDef: 13,
			fireDef: 6,
			electricDef: 6,
			iceDef: 6,
			lightDef: 6,
			darkDef: 4,
			load: 20,
			battleNum: 20
		},
		["soldier-2"]: {
			name: "步兵",
			race: 1,
			res: "soldier-2",
			eatMeterials: { "material-2": 0.24 },
			des: "首领并不需要很多头脑灵光的家伙，但需要很多只知道服从命令的步兵",
			attackOrDefense: 2,
			attackType: 1,
			attack: 17,
			physicalDef: 76,
			fireDef: 6,
			electricDef: 6,
			iceDef: 19,
			lightDef: 13,
			darkDef: 6,
			load: 24,
			battleNum: 40
		},
		["soldier-3"]: {
			name: "十字军",
			race: 1,
			res: "soldier-3",
			eatMeterials: { "material-2": 0.32 },
			des: "在这样没落的世界中，依旧有许多战士信仰着，光明的一天，总会到来。",
			attackOrDefense: 1,
			attackType: 1,
			attack: 87,
			physicalDef: 56,
			fireDef: 9,
			electricDef: 9,
			iceDef: 19,
			lightDef: 37,
			darkDef: 56,
			load: 32,
			battleNum: 120
		},
		["soldier-4"]: {
			name: "飞行器",
			race: 1,
			res: "soldier-4",
			eatMeterials: { "material-2": 0.48 },
			des: "它的速度极快，它的威力不强，它的故障率极高，有时候会被一只苍蝇撞毁",
			attackOrDefense: 1,
			attackType: 1,
			attack: 30,
			physicalDef: 4,
			fireDef: 6,
			electricDef: 6,
			iceDef: 11,
			lightDef: 8,
			darkDef: 4,
			load: 48,
			battleNum: 40
		},
		["soldier-5"]: {
			name: "钢铁机器人",
			race: 1,
			res: "soldier-5",
			eatMeterials: { "material-2": 1.2 },
			des: "这种看起来难以摧毁的机器人，其实是烧木头作为能量源的，需要数人才才能操纵",
			attackOrDefense: 3,
			attackType: 1,
			attack: 110,
			physicalDef: 147,
			fireDef: 73,
			electricDef: 18,
			iceDef: 55,
			lightDef: 37,
			darkDef: 37,
			load: 120,
			battleNum: 180
		},
		["soldier-6"]: {
			name: "黑铁作战兵器",
			race: 1,
			res: "soldier-6",
			eatMeterials: { "material-4": 0.36 },
			des: "巨大半机械人，移动是轮子，搭载大炮，科技巅峰，攻防一体。很多人操纵",
			attackOrDefense: 3,
			attackType: 1,
			attack: 260,
			physicalDef: 280,
			fireDef: 140,
			electricDef: 70,
			iceDef: 70,
			lightDef: 70,
			darkDef: 70,
			load: 180,
			battleNum: 400
		},
		["soldier-7"]: {
			name: "骑士",
			race: 1,
			res: "soldier-7",
			eatMeterials: { "material-2": 0.64 },
			des: "每一个骑兵，没有一千金币的家产，在骑兵团会被当成耻辱",
			attackOrDefense: 1,
			attackType: 2,
			attack: 47,
			physicalDef: 71,
			fireDef: 16,
			electricDef: 8,
			iceDef: 24,
			lightDef: 24,
			darkDef: 16,
			load: 64,
			battleNum: 80
		},
		["soldier-8"]: {
			name: "圣骑士",
			race: 1,
			res: "soldier-8",
			eatMeterials: { "material-2": 0.99 },
			des: "圣光不分地域，不分时间，不分贫富，只要虔诚的那一刻，它便降临你的身心。",
			attackOrDefense: 3,
			attackType: 2,
			attack: 149,
			physicalDef: 182,
			fireDef: 49,
			electricDef: 30,
			iceDef: 49,
			lightDef: 91,
			darkDef: 207,
			load: 99,
			battleNum: 270
		},
		["soldier-9"]: {
			name: "火焰法师",
			race: 1,
			res: "soldier-9",
			eatMeterials: { "material-2": 0.41 },
			des: "掌握了火焰能力就自高自大，这种傲慢导致了法术境界止步不前",
			attackOrDefense: 1,
			attackType: 4,
			attack: 57,
			physicalDef: 12,
			fireDef: 49,
			electricDef: 18,
			iceDef: 18,
			lightDef: 12,
			darkDef: 12,
			load: 41,
			battleNum: 80
		},
		["soldier-10"]: {
			name: "火魔导",
			race: 1,
			res: "soldier-10",
			eatMeterials: { "material-2": 0.3 },
			des: "日夜讲究知识和谈论美女，掌握了火焰奥秘，但是不知为何身体却很虚弱",
			attackOrDefense: 1,
			attackType: 1,
			attack: 129,
			physicalDef: 28,
			fireDef: 124,
			electricDef: 41,
			iceDef: 41,
			lightDef: 28,
			darkDef: 14,
			load: 30,
			battleNum: 180
		},
		["soldier-11"]: {
			name: "水元素",
			race: 1,
			res: "soldier-11",
			eatMeterials: { "material-2": 0.18 },
			des: "谁也不知道，神秘的水元素为何会投靠人类。也许它们和人类一样遇到的困境。",
			attackOrDefense: 2,
			attackType: 5,
			attack: 73,
			physicalDef: 73,
			fireDef: 73,
			electricDef: 12,
			iceDef: 73,
			lightDef: 12,
			darkDef: 0,
			load: 18,
			battleNum: 120
		},
		["soldier-12"]: {
			name: "魅魔",
			race: 1,
			res: "soldier-12",
			eatMeterials: { "material-1": 0.48 },
			des: "献祭了大量男性人类才能雇佣的恶魔，也是唯一能与人类合作的恶魔。每年资源为之牺牲的人不计其数。",
			attackOrDefense: 2,
			attackType: 6,
			attack: 130,
			physicalDef: 65,
			fireDef: 22,
			electricDef: 22,
			iceDef: 22,
			lightDef: 11,
			darkDef: 76,
			load: 48,
			battleNum: 180
		},
		["soldier-13"]: {
			name: "天使",
			race: 1,
			res: "soldier-13",
			eatMeterials: { "material-2": 0.32 },
			des: "一生苦难的人类，最后的梦想大概就是死后能成为天使吧。",
			attackOrDefense: 2,
			attackType: 5,
			attack: 239,
			physicalDef: 161,
			fireDef: 97,
			electricDef: 97,
			iceDef: 97,
			lightDef: 97,
			darkDef: 97,
			load: 32,
			battleNum: 400
		},
		["soldier-14"]: {
			name: "小恶魔",
			race: 7,
			res: "soldier-14",
			eatMeterials: { "material-2": 0.26 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 10,
			physicalDef: 9,
			fireDef: 9,
			electricDef: 9,
			iceDef: 8,
			lightDef: 8,
			darkDef: 9,
			load: 26,
			battleNum: 20
		},
		["soldier-15"]: {
			name: "地狱犬",
			race: 7,
			res: "soldier-15",
			eatMeterials: { "material-2": 0.4 },
			des: "1",
			attackOrDefense: 3,
			attackType: 2,
			attack: 24,
			physicalDef: 14,
			fireDef: 14,
			electricDef: 14,
			iceDef: 13,
			lightDef: 13,
			darkDef: 14,
			load: 40,
			battleNum: 40
		},
		["soldier-16"]: {
			name: "末日守卫",
			race: 7,
			res: "soldier-16",
			eatMeterials: { "material-2": 0.72 },
			des: "1",
			attackOrDefense: 1,
			attackType: 1,
			attack: 63,
			physicalDef: 20,
			fireDef: 24,
			electricDef: 8,
			iceDef: 8,
			lightDef: 4,
			darkDef: 16,
			load: 72,
			battleNum: 80
		},
		["soldier-17"]: {
			name: "魅魔",
			race: 7,
			res: "soldier-17",
			eatMeterials: { "material-2": 1.44 },
			des: "1",
			attackOrDefense: 1,
			attackType: 6,
			attack: 130,
			physicalDef: 65,
			fireDef: 22,
			electricDef: 22,
			iceDef: 22,
			lightDef: 11,
			darkDef: 76,
			load: 144,
			battleNum: 180
		},
		["soldier-18"]: {
			name: "恐惧魔王",
			race: 7,
			res: "soldier-18",
			eatMeterials: { "material-2": 1.8 },
			des: "1",
			attackOrDefense: 3,
			attackType: 6,
			attack: 106,
			physicalDef: 60,
			fireDef: 60,
			electricDef: 60,
			iceDef: 56,
			lightDef: 35,
			darkDef: 81,
			load: 180,
			battleNum: 180
		},
		["soldier-19"]: {
			name: "虚空行者",
			race: 7,
			res: "soldier-19",
			eatMeterials: { "material-2": 1.32 },
			des: "1",
			attackOrDefense: 2,
			attackType: 1,
			attack: 36,
			physicalDef: 127,
			fireDef: 42,
			electricDef: 64,
			iceDef: 85,
			lightDef: 21,
			darkDef: 85,
			load: 132,
			battleNum: 120
		},
		["soldier-20"]: {
			name: "地狱火",
			race: 7,
			res: "soldier-20",
			eatMeterials: { "material-2": 2.34 },
			des: "1",
			attackOrDefense: 3,
			attackType: 2,
			attack: 97,
			physicalDef: 119,
			fireDef: 119,
			electricDef: 40,
			iceDef: 20,
			lightDef: 40,
			darkDef: 60,
			load: 234,
			battleNum: 180
		},
		["soldier-21"]: {
			name: "深渊领主",
			race: 7,
			res: "soldier-21",
			eatMeterials: { "material-2": 3.24 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 178,
			physicalDef: 95,
			fireDef: 95,
			electricDef: 38,
			iceDef: 30,
			lightDef: 30,
			darkDef: 91,
			load: 324,
			battleNum: 270
		},
		["soldier-22"]: {
			name: "诅咒之王",
			race: 7,
			res: "soldier-22",
			eatMeterials: { "material-2": 4.4 },
			des: "1",
			attackOrDefense: 1,
			attackType: 6,
			attack: 317,
			physicalDef: 40,
			fireDef: 79,
			electricDef: 59,
			iceDef: 59,
			lightDef: 59,
			darkDef: 99,
			load: 440,
			battleNum: 400
		},
		["soldier-23"]: {
			name: "堕落泰坦",
			race: 7,
			res: "soldier-23",
			eatMeterials: { "material-2": 7.8 },
			des: "1",
			attackOrDefense: 3,
			attackType: 6,
			attack: 378,
			physicalDef: 407,
			fireDef: 204,
			electricDef: 102,
			iceDef: 102,
			lightDef: 51,
			darkDef: 153,
			load: 780,
			battleNum: 600
		},
		["soldier-24"]: {
			name: "野狼",
			race: 5,
			res: "soldier-24",
			eatMeterials: { "material-2": 0.24 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 12,
			physicalDef: 12,
			fireDef: 7,
			electricDef: 7,
			iceDef: 4,
			lightDef: 4,
			darkDef: 6,
			load: 24,
			battleNum: 20
		},
		["soldier-25"]: {
			name: "野猪",
			race: 5,
			res: "soldier-25",
			eatMeterials: { "material-2": 0.4 },
			des: "1",
			attackOrDefense: 2,
			attackType: 1,
			attack: 16,
			physicalDef: 41,
			fireDef: 18,
			electricDef: 12,
			iceDef: 18,
			lightDef: 12,
			darkDef: 18,
			load: 40,
			battleNum: 40
		},
		["soldier-26"]: {
			name: "野蛮人",
			race: 5,
			res: "soldier-26",
			eatMeterials: { "material-2": 1.32 },
			des: "1",
			attackOrDefense: 1,
			attackType: 1,
			attack: 85,
			physicalDef: 45,
			fireDef: 18,
			electricDef: 18,
			iceDef: 27,
			lightDef: 55,
			darkDef: 18,
			load: 132,
			battleNum: 120
		},
		["soldier-27"]: {
			name: "半人马射手",
			race: 5,
			res: "soldier-27",
			eatMeterials: { "material-2": 0.8 },
			des: "1",
			attackOrDefense: 1,
			attackType: 2,
			attack: 58,
			physicalDef: 16,
			fireDef: 16,
			electricDef: 16,
			iceDef: 16,
			lightDef: 16,
			darkDef: 16,
			load: 80,
			battleNum: 80
		},
		["soldier-28"]: {
			name: "流浪巫师",
			race: 5,
			res: "soldier-28",
			eatMeterials: { "material-2": 1.08 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 61,
			physicalDef: 31,
			fireDef: 61,
			electricDef: 77,
			iceDef: 61,
			lightDef: 46,
			darkDef: 31,
			load: 108,
			battleNum: 120
		},
		["soldier-29"]: {
			name: "雷鸟",
			race: 5,
			res: "soldier-29",
			eatMeterials: { "material-2": 1.44 },
			des: "1",
			attackOrDefense: 1,
			attackType: 3,
			attack: 137,
			physicalDef: 34,
			fireDef: 14,
			electricDef: 60,
			iceDef: 21,
			lightDef: 26,
			darkDef: 17,
			load: 144,
			battleNum: 180
		},
		["soldier-30"]: {
			name: "银狮",
			race: 5,
			res: "soldier-30",
			eatMeterials: { "material-2": 2.97 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 143,
			physicalDef: 204,
			fireDef: 58,
			electricDef: 58,
			iceDef: 93,
			lightDef: 117,
			darkDef: 52,
			load: 297,
			battleNum: 270
		},
		["soldier-31"]: {
			name: "迪则古德",
			race: 5,
			res: "soldier-31",
			eatMeterials: { "material-2": 3.24 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 179,
			physicalDef: 82,
			fireDef: 82,
			electricDef: 82,
			iceDef: 77,
			lightDef: 77,
			darkDef: 82,
			load: 324,
			battleNum: 270
		},
		["soldier-32"]: {
			name: "山岭巨人",
			race: 5,
			res: "soldier-32",
			eatMeterials: { "material-2": 5.2 },
			des: "1",
			attackOrDefense: 3,
			attackType: 1,
			attack: 206,
			physicalDef: 515,
			fireDef: 155,
			electricDef: 82,
			iceDef: 124,
			lightDef: 82,
			darkDef: 72,
			load: 520,
			battleNum: 400
		}
	},

	/**
	 * Generated from map.xlsx
	 */
	TERRAIN: {
		["terrian-1"]: { res: "terrian-1", name: "空地", buildingIds: [], des: "空地，可建造建筑" },
		["terrian-2"]: { res: "terrian-2", name: "矿场", buildingIds: [], des: "铁，黑铁，水晶" },
		["terrian-3"]: { res: "terrian-3", name: "岩石地表", buildingIds: [], des: "石头产量提升" },
		["terrian-4"]: { res: "terrian-4", name: "沃土", buildingIds: [], des: "农田产量提升" },
		["terrian-5"]: { res: "terrian-5", name: "水源", buildingIds: [], des: "农田，林地产量提升" },
		["terrian-6"]: { res: "terrian-6", name: "荒芜", buildingIds: [], des: "骨骼产量提升" },
		["terrian-7"]: { res: "terrian-7", name: "灵地", buildingIds: [], des: "黑莲花，元素精华产量提高" },
		["terrian-8"]: { res: "terrian-8", name: "灵地", buildingIds: [], des: "黑莲花，元素精华产量提高" },
		["terrian-9"]: { res: "terrian-9", name: "灵地", buildingIds: [], des: "黑莲花，元素精华产量提高" },
		["terrian-10"]: { res: "terrian-10", name: "灵地", buildingIds: [], des: "黑莲花，元素精华产量提高" },
		["terrian-11"]: { res: "terrian-11", name: "灵地", buildingIds: [], des: "黑莲花，元素精华产量提高" }
	},

	/**
	 * 将参数以“-”连接作为ID在指定sheet中找到匹配的行
	 * @param sheet XLSX中的表
	 * @param args 将被“-”连接的参数
	 * @returns 匹配行的数据
	 */
	findOne: function(sheet) {
		var args = [];
		for (var _i = 1; _i < arguments.length; _i++) {
			args[_i - 1] = arguments[_i];
		}
		var key = args.join("-");
		return sheet[key];
	}
};

for (var key in window.XLSX.BUILDING) {
	Object.defineProperties(window.XLSX.BUILDING[key], {});
}

for (var key in window.XLSX.CITY_NAME) {
	Object.defineProperties(window.XLSX.CITY_NAME[key], {});
}

for (var key in window.XLSX.MATERIAL) {
	Object.defineProperties(window.XLSX.MATERIAL[key], {});
}

for (var key in window.XLSX.RACE) {
	Object.defineProperties(window.XLSX.RACE[key], {});
}

for (var key in window.XLSX.SOLDIER) {
	Object.defineProperties(window.XLSX.SOLDIER[key], {});
}

for (var key in window.XLSX.TERRAIN) {
	Object.defineProperties(window.XLSX.TERRAIN[key], {});
}
