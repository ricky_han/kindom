import { Color } from "cc";
import { timeFormat } from "./TimeUtils";

/** 日志管理器 */
export default class Logger {
    /** Log switch */
    public static enable = true;

    /**
     * 获取时间字符串
     * @returns 获取时间字符串
     */
    private static getTimeString() {
        return "【" + timeFormat(new Date(), "yyyy/MM/dd hh:mm:ss:S") + "】";
    }

    public static format(str: string) {
        return Logger.getTimeString() + str;
    }

    public static enableLog(enable: boolean) {
        enable = enable;
    }

    /**
     * 带自定义颜色的日志
     * @param message 消息
     * @param color 颜色
     */
    public static print(message: string, color?: Color): void {
        if (Logger.enable) {
            if (color) {
                console.log("%c%s", color, Logger.getTimeString() + message);
            } else {
                console.log(Logger.getTimeString() + message);
            }
        }
    }

    public static info(message?: any, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.info(Logger.getTimeString() + message, ...optionalParams);
        }
    }

    public static log(message?: any, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.log(Logger.getTimeString() + message, ...optionalParams);
        }
    }

    public static debug(message?: any, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.debug(Logger.getTimeString() + message, ...optionalParams);
        }
    }

    public static warn(message?: any, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.warn(Logger.getTimeString() + message, ...optionalParams);
        }
    }

    public static error(message?: any, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.error(Logger.getTimeString() + message, ...optionalParams);
        }
    }

    public static assert(condition?: boolean, message?: string, ...data: any[]): void {
        if (Logger.enable) {
            console.assert(condition, message, ...data);
        }
    }

    public static clear(): void {
        if (Logger.enable) {
            console.clear();
        }
    }

    public static count(label?: string): void {
        if (Logger.enable) {
            console.count(label);
        }
    }

    public static dir(value?: any, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.dir(value, ...optionalParams);
        }
    }

    public static dirxml(value: any): void {
        if (Logger.enable) {
            console.dirxml(value);
        }
    }

    public static group(groupTitle?: string, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.group(groupTitle, ...optionalParams);
        }
    }

    public static groupCollapsed(groupTitle?: string, ...optionalParams: any[]): void {
        if (Logger.enable) {
            console.groupCollapsed(groupTitle, ...optionalParams);
        }
    }

    public static groupEnd(): void {
        if (Logger.enable) {
            console.groupEnd();
        }
    }

    public static table(...tabularData: any[]): void {
        if (Logger.enable) {
            console.table(...tabularData);
        }
    }

    public static time(label?: string): void {
        if (Logger.enable) {
            console.time(label);
        }
    }

    public static timeEnd(label?: string): void {
        if (Logger.enable) {
            console.timeEnd(label);
        }
    }

    public static timeStamp(label?: string): void {
        if (Logger.enable) {
            console.timeStamp(label);
        }
    }
}
