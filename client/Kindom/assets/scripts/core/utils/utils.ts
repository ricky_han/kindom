// export const delayCall = (s: number, callFunc: Function) => {
//     setTimeout(callFunc, s);

import { sys } from "cc";

// };
const mathRadom = Math.random;
const mathFloor = Math.floor;
export const delay = (seconds: number) => {
  return new Promise((resolve, reject) => {
    setTimeout(resolve, seconds * 1000);
  });
};

export const getNowTime = () => {
  return Date.now();
};

export const getNetTime = () => {
  return new Promise<string>((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    if (!xhr) {
      xhr = new ActiveXObject("Microsoft.XMLHTTP");
    }

    if (sys.isBrowser) {
      xhr.open("HEAD", location.href, true);
    } else {
      xhr.open("HEAD", "https://www.baidu.com", true);
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        resolve(xhr.getResponseHeader("Date") as string);
      } else {
        // reject("error");
      }
    };
    xhr.send(null);
  });
};

export const getPassTimeForSecond = (startTime: number) => {
  const nowTime = getNowTime();
  return Math.floor((nowTime - startTime) / 1000);
};

// 时间校验 放值作弊，网络时间和本地时间校验

export const getRandom = () => {
  return mathRadom();
};

export const getNumFloor = (f: number) => {
  return mathFloor(f);
};
