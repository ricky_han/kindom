type TDate = Date | string | number;

interface TimeAgoInterface {
	format(date: TDate, locale?: string): string;
	render<T>(nodes: Node | NodeList | JQuery, locale?: string): void;
	setLocale(locale: string): void;
}

interface Factory {
	(): TimeAgoInterface;
	(nowDate: TDate, defaultLocale?: string): TimeAgoInterface;
	cancel(node?: Node | JQuery): void;
	register(locale: string, localeFunc: Function): void;
}

declare const timeago: Factory;