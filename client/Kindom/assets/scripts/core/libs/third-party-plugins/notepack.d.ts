declare const notepack: {
	readonly encode: (value: any) => ArrayBuffer;
	readonly decode: (buffer: ArrayBuffer) => any;
};
