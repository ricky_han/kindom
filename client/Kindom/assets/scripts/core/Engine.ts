import { _decorator } from "cc";
import { ResourceManager } from "./manager/ResourceManager";
import { AudioManager } from "./manager/AudioManager";
import { TimerManager } from "./manager/TimerManager";
import { StorageManager } from "./manager/StorageManager";
import { EventManager } from "./manager/EventManager";
const { ccclass, property } = _decorator;

export class Engine {
    /** 资源管理管理 */
    public static resManager: ResourceManager;
    /** 时间管理 */
    public static timeManager: TimerManager;
    /** 音效管理 */
    public static audioManager: AudioManager;
    /** 存储管理 */
    public static storageManager: StorageManager;
    /** 事件管理 */
    public static eventManager: EventManager;

    public static init() {
        this.resManager = ResourceManager.getInstance();
        this.timeManager = TimerManager.getInstance();
        this.audioManager = AudioManager.getInstance();
        this.storageManager = StorageManager.getInstance();
        this.eventManager = EventManager.getInstance();
    }
}
