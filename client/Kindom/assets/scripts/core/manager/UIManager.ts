import { _decorator, Node, game, Widget, Prefab, instantiate, view } from "cc";
import { BasePanel } from "../component/BasePanel";
import { BusyIndicator } from "../component/BusyIndicator";
import { Engine } from "../Engine";
import Logger from "../utils/Logger";
// 界面所处层级
export enum UI_TYPE {
    UI_LAYER = "UI_LAYER",
    UI_PANEL = "UI_PANEL",
    UI_POPUP = "UI_POPUP",
    UI_GUIDE = "UI_GUIDE",
    UI_ALERT = "UI_ALERT",
    UI_TOAST = "UI_TOAST",
}

/**
 * 界面弹出管理
 * 根据面板的不同类型设置显示层级
 */
class UIManager {
    private static _instance: UIManager;

    private _uiContainer!: Node;
    private _layerMap: { [uiType: string]: Node } = {}; // 层级

    private _currentShowPanel: { [uiName: string]: { uiNode: Node; uiType: string } | undefined } = {}; // 当前处于打开状态的界面
    static getInstance(): UIManager {
        if (!this._instance) {
            this._instance = new UIManager();
        }
        return this._instance;
    }

    init(rootContainer: Node) {
        this._uiContainer = rootContainer;

        for (const type in UI_TYPE) {
            Logger.info(type);
            const layerNode = new Node(type);
            var widget: Widget = layerNode.addComponent(Widget);
            widget.isAlignLeft = widget.isAlignRight = widget.isAlignTop = widget.isAlignBottom = true;
            widget.left = widget.right = widget.top = widget.bottom = 0;
            widget.enabled = true;
            layerNode.parent = this._uiContainer;
            this._layerMap[type] = layerNode;
        }
    }

    show(uiName: string, ...args: any[]) {
        let uiNode: Node | undefined = this._currentShowPanel[uiName]?.uiNode;
        if (!uiNode) {
            BusyIndicator.instance.setIsBusy(true);
            Engine.resManager.loadPath<Prefab>("prefabs/ui/" + uiName).then(
                (prefab: Prefab) => {
                    uiNode = instantiate(prefab);
                    const uiClass = uiNode.getComponent(BasePanel)!;
                    uiNode.parent = this._layerMap[uiClass.uiType];
                    uiNode.active = true;
                    uiClass.show(...args);
                    this._currentShowPanel[uiName] = { uiNode: uiNode, uiType: uiClass.uiType };

                    BusyIndicator.instance.setIsBusy(false);
                },
                (err: string) => {
                    Logger.error(err);
                }
            );
        } else {
            uiNode.active = true;
            const uiClass = uiNode.getComponent(BasePanel)!;
            uiClass.show(...args);
            uiNode.setSiblingIndex(1000); // 最上层
            this._currentShowPanel[uiName] = { uiNode: uiNode, uiType: uiClass.uiType };
        }
    }

    close(uiName: string) {
        if (!this._currentShowPanel[uiName]) {
            Logger.warn("UIManager %s is not open state。", uiName);
            return;
        }

        const uiNode: Node = this._currentShowPanel[uiName]!.uiNode;
        if (uiNode) {
            uiNode.removeFromParent();
            this._currentShowPanel[uiName] = undefined;
        }
    }

    isShow(uiName: string) {
        return !!this._currentShowPanel[uiName];
    }

    getLayer(uiType: UI_TYPE) {
        return this._layerMap[uiType];
    }

    clear() {
        this._currentShowPanel = {};
    }
}

export const uiManager = UIManager.getInstance();
