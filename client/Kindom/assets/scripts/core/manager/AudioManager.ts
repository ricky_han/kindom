import { _decorator, Component, Node, AudioSource, AudioClip, game } from "cc";
import { Engine } from "../Engine";
const { ccclass, property } = _decorator;

export class AudioManager extends Component {
    private static _instance: AudioManager;

    private GAME_PLAYER_MUSIC_TAG!: string;
    private storage_status: {
        music: string;
        audio: string;
        volume: number;
    } | null = null;
    private _musicSource!: AudioSource;
    private _audioSource!: AudioSource;

    private _musicSwitch = true; // 非true 关闭状态
    private _audioSwitch = true;
    private _audioVolume = 1; // 音量

    static getInstance() {
        if (!this._instance) {
            const node = new Node("UIAudioManager");
            game.addPersistRootNode(node);
            this._instance = node.addComponent(AudioManager);

            const musicNode = new Node("UIMusic");
            musicNode.parent = node;
            this._instance._musicSource = musicNode.addComponent(AudioSource);

            const audioNode = new Node("UIAudio");
            audioNode.parent = node;
            this._instance._audioSource = audioNode.addComponent(AudioSource);
        }

        return this._instance;
    }

    // 初始化数据
    init(playerId?: string) {
        if (playerId) {
            this.GAME_PLAYER_MUSIC_TAG = "game_music_status" + "-" + playerId;
        } else {
            this.GAME_PLAYER_MUSIC_TAG = "game_music_status";
        }
        const storageValue = Engine.storageManager.getItem(this.GAME_PLAYER_MUSIC_TAG);
        if (storageValue) {
            this.storage_status = JSON.parse(storageValue);
            this._musicSwitch = this.storage_status!.music !== "off";
            this._audioSwitch = this.storage_status!.audio !== "off";
            this._audioVolume = this.storage_status!.volume;
        } else {
            this.storage_status = {
                music: this._musicSwitch ? "on" : "off",
                audio: this._audioSwitch ? "on" : "off",
                volume: this._audioVolume,
            };
        }
    }

    async playMusic(musicName: string, isLoop?: boolean) {
        if (!this.GAME_PLAYER_MUSIC_TAG) {
            this.init();
        }
        if (!this._musicSwitch) {
            return;
        }
        const audioClip: AudioClip | null = await Engine.resManager.loadAudio("audios/" + musicName);
        if (audioClip) {
            this._musicSource.clip = audioClip;
            this._musicSource.volume = this._audioVolume;
            this._musicSource.play();
        }
    }

    stopMusic() {
        this._musicSource.stop();
    }

    async playAudio(audioName: string, audioScale = 1) {
        if (!this._audioSwitch) {
            return;
        }
        const audioClip: AudioClip | null = await Engine.resManager.loadAudio("audios/" + audioName);
        if (audioClip) {
            this._audioSource.playOneShot(audioClip, audioScale * this._audioVolume);
        }
    }

    resume() {
        if (this._musicSwitch) {
            this._musicSource.play();
        }

        if (this._audioSwitch) {
            this._audioSource.play();
        }
    }

    pause() {
        this._musicSource.pause();
        this._audioSource.pause();
    }

    // 切换状态
    switchMusicStatus(musicState: boolean) {
        this._musicSwitch = musicState;
    }

    // 切换状态
    switchAudioStatus(audioState: boolean) {
        this._audioSwitch = audioState;
    }

    // 修改音量大小
    changeVolume(volume: number) {
        this._audioVolume = volume;
    }

    // 保存状态
    saveAudioStatus() {
        this.storage_status = {
            music: this._musicSwitch ? "on" : "off",
            audio: this._audioSwitch ? "on" : "off",
            volume: this._audioVolume,
        };

        Engine.storageManager.setItem(this.GAME_PLAYER_MUSIC_TAG!, JSON.stringify(this.storage_status));
    }
}
