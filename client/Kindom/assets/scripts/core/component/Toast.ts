import { director, instantiate, NodePool, Prefab, _decorator, Node, Animation, Component, RichText } from "cc";
import { Engine } from "../Engine";
import { uiManager, UI_TYPE } from "../manager/UIManager";
import Logger from "../utils/Logger";

export default class Toast {
    private static toastPool: NodePool = new NodePool();

    static async showToast(tipStr: string) {
        let uiNode = Toast.toastPool.get();
        const toastParentNode: Node = uiManager.getLayer(UI_TYPE.UI_TOAST);
        if (!uiNode) {
            const uiPrefab = await Engine.resManager.loadPath<Prefab>("prefabs/ui/common/toast");
            if (uiPrefab) {
                uiNode = instantiate(uiPrefab);
                uiNode.active = true;
            } else {
                Logger.error("loas toast error!");
                return;
            }
        }
        uiNode.parent = toastParentNode;
        uiNode.getChildByName("text")!.getComponent(RichText)!.string = `${tipStr}`;
        const toastAnim = uiNode!.getComponent(Animation)!;
        toastAnim.on(Animation.EventType.FINISHED, () => {
            // 动画结束
            this.toastPool.put(uiNode!);
        });
        toastAnim.play();
    }

    static hideToast() {}
}
