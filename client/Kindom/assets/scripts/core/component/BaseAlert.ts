import { _decorator, Component, Node, Label, Button, RichText } from "cc";
import { UI_TYPE } from "../manager/UIManager";
import { BaseComponent } from "./BaseComponent";
import { BasePanel } from "./BasePanel";
const { ccclass, property } = _decorator;

export interface AlertDataType {
    title: string;
    content: string;
    okLabel: string;
    cancelLabel: string;
    callFunc: Function;
}

@ccclass("BaseAlert")
export class BaseAlert extends BasePanel {
    public uiType: UI_TYPE = UI_TYPE.UI_ALERT;

    @property({ type: Node, displayName: "标题" })
    title!: Node;

    @property({ type: Node, displayName: "内容描述" })
    contentDes!: Node;

    @property({ type: Button, displayName: "确认按钮" })
    btnSure!: Button;

    @property({ type: Button, displayName: "取消按钮" })
    btnCancel!: Button;

    private alertData!: AlertDataType;
    start() {
        // [3]
    }

    private setLabelString(labelNode: Node, txt: string) {
        let txtLab: Label | RichText | null = labelNode.getComponent(Label);
        if (!txtLab) {
            txtLab = labelNode.getComponent(RichText);
        }
        if (txtLab) {
            txtLab.string = txt;
        }
    }
    show(alertData: AlertDataType): void {
        this.alertData = alertData;
        this.setLabelString(this.title, alertData.title);
        this.setLabelString(this.contentDes, alertData.content);
        if (alertData.okLabel) {
            this.btnSure.node.active = true;
            this.btnCancel.node.active = !!alertData.cancelLabel;
            this.btnSure.node.getChildByName("Label")!.getComponent(Label)!.string = alertData.okLabel;
        }
        if (alertData.cancelLabel) {
            this.btnCancel.node.active = true;
            this.btnCancel.node.getChildByName("Label")!.getComponent(Label)!.string = alertData.cancelLabel;
        }
    }

    protected onSure() {
        if (this.alertData.callFunc) {
            this.alertData.callFunc("ok");
        }
        this.close();
    }

    protected onCancel() {
        if (this.alertData.callFunc) {
            this.alertData.callFunc("cancel");
        }
        this.close();
    }
}
