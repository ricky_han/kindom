import { _decorator, Component, Node } from "cc";
import { UI_TYPE } from "../manager/UIManager";
import { BasePanel } from "./BasePanel";
const { ccclass, property } = _decorator;

@ccclass("BaseLayer")
export class BaseLayer extends BasePanel {
    public uiType: UI_TYPE = UI_TYPE.UI_LAYER;

    start() {
        // [3]
    }
}
