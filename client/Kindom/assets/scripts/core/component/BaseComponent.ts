import { _decorator, Component, Node } from "cc";
import { Engine } from "../Engine";
import { UI_TYPE } from "../manager/UIManager";
const { ccclass, property } = _decorator;

@ccclass("BaseComponent")
export class BaseComponent extends Component {
    start() {
        // [3]
    }

    onDisable() {
        // Engine.eventManager.off()
    }

    onDestroy() {
        Engine.eventManager.targetOff(this);
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}
