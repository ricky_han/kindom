import { _decorator, Component, Node, Vec2, SystemEvent, EventTouch } from "cc";
const { ccclass, property } = _decorator;

@ccclass
export default class ClickDelegate extends Component {
    private isClick = false;
    private func: Function | undefined = undefined;
    private args: any = undefined;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.addTouchEventListeners();
    }

    // onDisable() {
    //     // TODO
    // }

    init(target: any, func: Function, ...args: any[]) {
        this.func = target ? func.bind(target) : func;

        this.args = args;
    }

    private addTouchEventListeners() {
        this.node.on(SystemEvent.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(SystemEvent.EventType.TOUCH_MOVE, this.onTouchMove, this);
        this.node.on(SystemEvent.EventType.TOUCH_END, this.onTouchEnd, this);
        // this.node.on(SystemEvent.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
    }

    private onTouchStart(event: EventTouch) {
        const touches = event.getTouches();
        this.isClick = touches.length === 1;
    }

    private onTouchMove(event: EventTouch) {
        if (!this.isClick) {
            return;
        }

        const touches = event.getTouches();
        if (touches.length > 1) {
            this.isClick = false;
        } else {
            const firstTouch = touches[0];
            const deltaLoc: Vec2 = firstTouch.getDelta();
            if (Math.abs(deltaLoc.x) > 10 || Math.abs(deltaLoc.y) > 10) {
                this.isClick = false;
            }
        }
    }

    private onTouchEnd(event: EventTouch) {
        if (!this.isClick) {
            return;
        }
        if (this.func) {
            this.func(...this.args);
        }
    }
}
