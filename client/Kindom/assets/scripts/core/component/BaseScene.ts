import { _decorator, Component, Node } from "cc";
import { Engine } from "../Engine";
import { uiManager } from "../manager/UIManager";
import { BaseComponent } from "./BaseComponent";
import Toast from "./Toast";
const { ccclass, property } = _decorator;

/**
 *  场景基类
 */
@ccclass("BaseScene")
export class BaseScene extends BaseComponent {
  @property({
    type: Node,
    tooltip: "游戏层",
  })
  public game: Node | null = null;

  @property({
    type: Node,
    tooltip: "ui层",
  })
  public ui: Node | null = null;

  onLoad() {
    if (this.ui) {
      const uiContainer = this.ui.getChildByName("ui_container");
      if (uiContainer) {
        uiManager.init(uiContainer);
      }
    }
  }

  start() {
    // [3]
  }

  showUI(event: CustomEvent, uiName: string) {
    uiManager.show(uiName);
  }

  showToast(event: CustomEvent, uiName: string) {
    Toast.showToast(uiName);
  }

  playMusic() {
    Engine.audioManager.playMusic("Gravel");
  }

  onDestroy() {
    uiManager.clear();
  }
}
