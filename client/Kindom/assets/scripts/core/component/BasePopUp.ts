import { _decorator, Component, Node } from "cc";
import { UI_TYPE } from "../manager/UIManager";
const { ccclass, property } = _decorator;

@ccclass("BasePopUp")
export class BasePopUp extends Component {
    public uiType: UI_TYPE = UI_TYPE.UI_POPUP;

    start() {
        // [3]
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}
