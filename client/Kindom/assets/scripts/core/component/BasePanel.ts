import { _decorator, Component, Node } from "cc";
import { uiManager, UI_TYPE } from "../manager/UIManager";
import { BaseComponent } from "./BaseComponent";
const { ccclass, property } = _decorator;

@ccclass("BasePanel")
export class BasePanel extends BaseComponent {
    public uiType: UI_TYPE = UI_TYPE.UI_PANEL;

    start() {}

    show(...args: any[]) {}

    onDestroy() {}

    close() {
        uiManager.close(this.node.name);
    }
}
