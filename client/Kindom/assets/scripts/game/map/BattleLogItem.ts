import { _decorator, Component, Node, Label, instantiate } from "cc";
import { BaseComponent } from "../../core/component/BaseComponent";
import { getNumFloor } from "../../core/utils/utils";
import { dataManager } from "../data/DataManager";
import { BELONG_TYPE } from "../data/MapData";
const { ccclass, property } = _decorator;

@ccclass("BattleLogItem")
export class BattleLogItem extends BaseComponent {
    @property({ type: Node, displayName: "标题" })
    titleNode!: Node;

    @property({ type: Node, displayName: "战斗结果" })
    battleResult!: Node;

    @property({ type: Node, displayName: "攻击方士兵" })
    attackArmyContainer!: Node;

    @property({ type: Node, displayName: "防守方士兵" })
    defenseArmyContainer!: Node;

    @property({ type: Label, displayName: "攻击方战力" })
    attackPowerLab!: Label;

    @property({ type: Label, displayName: "防守方战力" })
    defensePowerLab!: Label;

    setData(report: {
        belong: BELONG_TYPE;
        cityId: string;
        attack: { [soldierId: string]: number };
        defense: { [soldierId: string]: number };
    }) {
        this.setArmyList(this.attackArmyContainer, report.attack);
        this.setArmyList(this.defenseArmyContainer, report.defense);

        const attackArmyTypePower = dataManager.dataSoldier.calculateArmyAttackPower(report.attack);
        const attackTotalPower = dataManager.dataSoldier.getPowerTotalNum(attackArmyTypePower);

        const defArmyTypePower = dataManager.dataSoldier.calculateArmyDefPower(report.defense, attackArmyTypePower);
        const defTotalPower = dataManager.dataSoldier.getPowerTotalNum(defArmyTypePower);

        this.attackPowerLab.string = `${getNumFloor(attackTotalPower)}`;
        this.defensePowerLab.string = `${getNumFloor(defTotalPower)}`;

        // 进攻方为己方
        // this.titleNode.children.forEach((child) => (child.active = false));
        this.battleResult.children.forEach((child) => (child.active = false));
        // this.titleNode.children[0].active = true;
        if (report.cityId) {
            this.titleNode.getChildByName("city")!.getComponent(Label)!.string = `事件城池:${report.cityId}`;
        }
        if (attackTotalPower > defTotalPower) {
            this.battleResult.children[0].active = true;
        } else {
            this.battleResult.children[1].active = true;
        }
    }

    // 部队
    setArmyList(armyContainer: Node, soldiers: { [soldierId: string]: number }) {
        armyContainer.children.forEach((child) => {
            child.active = false;
        });
        let idx = 0;
        for (const soldierId in soldiers) {
            const soldierNum = soldiers[soldierId];
            let soldierItem = armyContainer.children[idx];
            if (!soldierItem) {
                soldierItem = instantiate(armyContainer.children[0]);
                soldierItem.parent = armyContainer;
            }
            soldierItem.active = true;
            const soldierCfgData = XLSX.SOLDIER[soldierId];
            soldierItem.getChildByName("lab_name")!.getComponent(Label)!.string = `${soldierCfgData.name}`;
            soldierItem.getChildByName("lab_num")!.getComponent(Label)!.string = `x ${soldierNum}`;
            idx += 1;
        }
    }
}
