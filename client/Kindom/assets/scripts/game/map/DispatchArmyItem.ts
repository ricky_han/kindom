import { _decorator, Component, Node, Label, ProgressBar } from "cc";
import { BaseComponent } from "../../core/component/BaseComponent";
import { Engine } from "../../core/Engine";
import { getNowTime, getNumFloor } from "../../core/utils/utils";
import { dataManager } from "../data/DataManager";
import { DispatchArmyInfo } from "../data/SoldierData";
import { EVENT_NAME, GAME_DATA, TIMER_TASK_TAG } from "../GameConstant";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = DispatchArmyItem
 * DateTime = Mon Jan 03 2022 08:37:10 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = DispatchArmyItem.ts
 * FileBasenameNoExtension = DispatchArmyItem
 * URL = db://assets/scripts/game/map/DispatchArmyItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("DispatchArmyItem")
export class DispatchArmyItem extends BaseComponent {
    @property({ type: Label, displayName: "倒计时" })
    labCoolTime!: Label;

    @property({ type: Label, displayName: "出发城和结束城" })
    cityName!: Label;

    @property({ type: ProgressBar, displayName: "进度" })
    barProgress!: ProgressBar;

    armyId!: string;
    armyData!: DispatchArmyInfo;
    leftCoolTime = 0;
    currentDtTime = 0;
    totalCostTime = 0;

    onLoad() {
        Engine.eventManager.on(
            EVENT_NAME.STOP_DISPATCH_ARMY,
            (dispatchArmyId: string) => {
                if (dispatchArmyId === this.armyId) {
                    this.node.active = false;
                }
            },
            this
        );
    }

    start() {
        // [3]
    }

    private getSoldierTotalNum(soldiers: { [soldierId: string]: number }) {
        let soldierNum = 0;
        for (const soldierId in soldiers) {
            soldierNum += soldiers[soldierId];
        }

        return soldierNum;
    }

    setData(armyId: string, armyData: DispatchArmyInfo) {
        this.armyId = armyId;
        this.armyData = armyData;
        const passTime = armyData.passTime;
        const armySoldierNum = this.getSoldierTotalNum(armyData.soldiers);
        this.cityName.string = `${armyData.fromCityId}-${armyData.toCityId}:${armySoldierNum}`;
        const totalLen = dataManager.dataMap.getPathsLen(armyData.paths);
        this.totalCostTime = totalLen / GAME_DATA.ARMY_SPEED;
        this.leftCoolTime = this.totalCostTime - passTime;
        if (this.leftCoolTime <= 0) {
            this.node.active = false;
        } else {
            this.node.active = true;
        }
        this.barProgress.progress = this.leftCoolTime / this.totalCostTime;
    }

    // 加速移动
    accOver() {
        Engine.eventManager.emit(EVENT_NAME.FAST_TO_TARGET_CITY, this.armyId, this.armyData.toCityId);
    }

    update(dt: number) {
        this.currentDtTime += dt;
        if (this.currentDtTime > 1) {
            this.currentDtTime -= 1;
            this.leftCoolTime -= 1;
            if (this.leftCoolTime < 0) {
                this.node.active = false;
            } else {
                this.barProgress.progress = this.leftCoolTime / this.totalCostTime;
                this.labCoolTime.string = `${getNumFloor( this.leftCoolTime)}`;
            }
        }
    }
}
