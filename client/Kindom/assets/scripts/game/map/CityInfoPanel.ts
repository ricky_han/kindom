import { _decorator, Component, Node, Label, Sprite, SpriteFrame, instantiate } from "cc";
import { BasePanel } from "../../core/component/BasePanel";
import { Engine } from "../../core/Engine";
import { uiManager } from "../../core/manager/UIManager";
import Logger from "../../core/utils/Logger";
import { dataManager } from "../data/DataManager";
import { BELONG_TYPE, CityInfoData } from "../data/MapData";
import { EVENT_NAME, UI_NAME } from "../GameConstant";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = CityInfoPanel
 * DateTime = Sun Dec 19 2021 01:19:18 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = CityInfoPanel.ts
 * FileBasenameNoExtension = CityInfoPanel
 * URL = db://assets/scripts/game/map/CityInfoPanel.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("CityInfoPanel")
export class CityInfoPanel extends BasePanel {
    @property({ type: Sprite, displayName: "地块类型图" })
    terrianTypeIcon!: Sprite;

    @property({ type: Label, displayName: "ID" })
    cityIdLab!: Label;

    @property({ type: Label, displayName: "名称" })
    cityNameLab!: Label;

    @property({ type: Label, displayName: "坐标" })
    cityPosLab!: Label;

    @property({ type: Label, displayName: "描述" })
    cityDesLab!: Label;

    @property({ type: Node, displayName: "资源信息" })
    resNode!: Node;

    @property({ type: Node, displayName: "士兵信息" })
    armyNode!: Node;

    @property({ type: Node, displayName: "资源容器" })
    resContainer!: Node;

    @property({ type: Node, displayName: "士兵容器" })
    armyContainer!: Node;

    @property({ type: Node, displayName: "操作按钮" })
    optBtns!: Node;

    @property({ type: Node, displayName: "建造" })
    btnBuild!: Node;
    @property({ type: Node, displayName: "去征服" })
    btnAttack!: Node;
    @property({ type: Node, displayName: "派遣" })
    btnDispatch!: Node;
    @property({ type: Node, displayName: "去掠夺" })
    btnPlunder!: Node;
    @property({ type: Node, displayName: "被征服" })
    btnBeAttack!: Node;
    @property({ type: Node, displayName: "被掠夺" })
    btnBePlunder!: Node;

    private cityDetail: CityInfoData | undefined;
    start() {
        // [3]
    }

    show(cityId: string) {
        const cityDetail: CityInfoData | undefined = dataManager.dataMap.getCityDetail(cityId);
        if (!cityDetail) {
            Logger.error("error cityId:", cityId);
            return;
        }
        this.cityDetail = cityDetail;
        Engine.resManager
            .loadPath<SpriteFrame>("textures/map/terrian-" + cityDetail.type + "/spriteFrame")
            .then((sprFame: SpriteFrame) => {
                this.terrianTypeIcon.spriteFrame = sprFame;
            });

        this.cityIdLab.string = `${cityDetail.id}`;
        this.cityNameLab.string = `${cityDetail.name}`;
        this.cityPosLab.string = `${cityDetail.pos}`;
        this.cityDesLab.string = `${cityDetail.des}`;
        const cityOriginData = dataManager.dataMap.getCityOriginData(cityId);
        this.resNode.active = false;
        if (cityDetail.materialData && Object.keys(cityDetail.materialData).length > 0) {
            this.resNode.active = true;
            this.resContainer.children.forEach((child) => {
                child.active = false;
            });
            let idx = 0;
            for (const materialId in cityDetail.materialData) {
                const materialNum = cityDetail.materialData[materialId];
                let materialItem = this.resContainer.children[idx];
                if (!materialItem) {
                    materialItem = instantiate(this.resContainer.children[0]);
                    materialItem.parent = this.resContainer;
                }
                materialItem.active = true;
                const materialCfgData = XLSX.MATERIAL[materialId];
                materialItem.getChildByName("lab_name")!.getComponent(Label)!.string = `${materialCfgData.name}`;
                materialItem.getChildByName("lab_num")!.getComponent(Label)!.string = `x ${materialNum}`;

                materialItem.getChildByName("lab_speed")!.active = false;
                if (cityOriginData && cityDetail.belong === BELONG_TYPE.MIDDLE_SIDE) {
                    // 一旦被占领 自然增长取消
                    const proInfoData = cityOriginData[materialId];
                    if (proInfoData) {
                        materialItem.getChildByName("lab_speed")!.active = true;
                        materialItem
                            .getChildByName("lab_speed")!
                            .getComponent(Label)!.string = `speed:${proInfoData.speed},max:${proInfoData.limit}`;
                    }
                }
                idx += 1;
            }
        }

        this.armyNode.active = false;
        if (cityDetail.armyData && Object.keys(cityDetail.armyData).length > 0) {
            this.armyNode.active = true;
            this.armyContainer.children.forEach((child) => {
                child.active = false;
            });
            let idx = 0;
            for (const soldierId in cityDetail.armyData) {
                const soldierNum = cityDetail.armyData[soldierId];
                let soldierItem = this.armyContainer.children[idx];
                if (!soldierItem) {
                    soldierItem = instantiate(this.armyContainer.children[0]);
                    soldierItem.parent = this.armyContainer;
                }
                soldierItem.active = true;
                const soldierCfgData = XLSX.SOLDIER[soldierId];
                soldierItem.getChildByName("lab_name")!.getComponent(Label)!.string = `${soldierCfgData.name}`;
                soldierItem.getChildByName("lab_num")!.getComponent(Label)!.string = `x ${soldierNum}`;
                soldierItem.getChildByName("lab_speed")!.active = false;
                if (cityOriginData) {
                    const proInfoData = cityOriginData[soldierId];
                    if (proInfoData) {
                        soldierItem.getChildByName("lab_speed")!.active = true;
                        soldierItem
                            .getChildByName("lab_speed")!
                            .getComponent(Label)!.string = `speed:${proInfoData.speed},max:${proInfoData.limit}`;
                    }
                }
                idx += 1;
            }
        }

        this.optBtns.children.forEach((child) => {
            child.active = false;
        });
        if (this.cityDetail.belong === BELONG_TYPE.SELF_SIDE) {
            // 己方
            this.btnBuild.active = true;
            this.btnAttack.active = true;
            this.btnDispatch.active = true;
            // this.btnPlunder.active = true;
        } else {
            this.btnBeAttack.active = true;
            // this.btnBePlunder.active = true;
        }
    }

    requestBuild() {
        if (this.cityDetail && this.cityDetail.belong === BELONG_TYPE.SELF_SIDE) {
            uiManager.show(UI_NAME.BUILD_LIST_PANEL, this.cityDetail.id, this.cityDetail.requiredBuildTypes);
            this.close();
        }
    }

    requestAttack() {
        if (this.cityDetail && this.cityDetail.belong === BELONG_TYPE.SELF_SIDE) {
            Engine.eventManager.emit(EVENT_NAME.READY_ATTACK_CITY, this.cityDetail.id, this.cityDetail.belong);
            this.close();
        }
    }

    requestBeAttack() {
        if (this.cityDetail && this.cityDetail.belong !== BELONG_TYPE.SELF_SIDE) {
            Engine.eventManager.emit(EVENT_NAME.READY_BE_ATTACK_CITY, this.cityDetail.id, this.cityDetail.belong);
            this.close();
        }
    }
}
