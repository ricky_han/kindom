import { _decorator, Component, Node, Label, Sprite, SpriteFrame, instantiate } from "cc";
import { BasePanel } from "../../core/component/BasePanel";
import { Engine } from "../../core/Engine";
import { uiManager } from "../../core/manager/UIManager";
import Logger from "../../core/utils/Logger";
import { getNumFloor } from "../../core/utils/utils";
import { dataManager } from "../data/DataManager";
import { BELONG_TYPE, BuildInfoData, CityInfoData } from "../data/MapData";
import { EVENT_NAME, UI_NAME } from "../GameConstant";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = CityInfoPanel
 * DateTime = Sun Dec 19 2021 01:19:18 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = CityInfoPanel.ts
 * FileBasenameNoExtension = CityInfoPanel
 * URL = db://assets/scripts/game/map/CityInfoPanel.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("BuildInfoPanel")
export class BuildInfoPanel extends BasePanel {
    @property({ type: Sprite, displayName: "建筑类型图" })
    buildTypeIcon!: Sprite;

    @property({ type: Label, displayName: "ID" })
    cityIdLab!: Label;

    @property({ type: Label, displayName: "名称" })
    buildNameLab!: Label;

    @property({ type: Label, displayName: "坐标" })
    posLab!: Label;

    @property({ type: Label, displayName: "描述" })
    buildDesLab!: Label;

    @property({ type: Label, displayName: "产出" })
    buildOutLab!: Label;

    @property({ type: Node, displayName: "士兵信息" })
    armyContainer!: Node;

    @property({ type: Node, displayName: "操作按钮" })
    optBtns!: Node;

    @property({ type: Node, displayName: "去征服" })
    btnAttack!: Node;
    @property({ type: Node, displayName: "去掠夺" })
    btnPlunder!: Node;
    @property({ type: Node, displayName: "被征服" })
    btnBeAttack!: Node;
    @property({ type: Node, displayName: "被掠夺" })
    btnBePlunder!: Node;
    @property({ type: Node, displayName: "移除" })
    btnRemove!: Node;
    @property({ type: Node, displayName: "派遣" })
    btnDispatch!: Node;

    private buildDetail: BuildInfoData | undefined;
    private cityDetail: CityInfoData | undefined;
    start() {
        // [3]
    }

    show(cityId: string) {
        const buildDetail: BuildInfoData | undefined = dataManager.dataMap.getBuildDetail(cityId);
        if (!buildDetail) {
            Logger.error("error buildId:", cityId);
            return;
        }
        this.buildDetail = buildDetail;
        Engine.resManager
            .loadPath<SpriteFrame>("textures/map/" + this.buildDetail.res + "/spriteFrame")
            .then((sprFame: SpriteFrame) => {
                this.buildTypeIcon.spriteFrame = sprFame;
            });

        this.cityIdLab.string = `${buildDetail.cityId}`;
        this.buildNameLab.string = `${buildDetail.name}`;
        this.buildDesLab.string = `${buildDetail.des}`;

        let outStr = "";
        for (const materialId in buildDetail.outMaterials) {
            const outRateVal = buildDetail.outMaterials[materialId];
            let resCfgData = dataManager.dataMap.getMaterialCfg(materialId);
            if (!resCfgData) {
                resCfgData = dataManager.dataMap.getSoldierCfg(materialId) as any;
            }
            const materialName = resCfgData!.name;
            outStr += `${materialName}:${outRateVal}/10s`;
        }
        this.buildOutLab.string = `${outStr}`;
        const cityDetail: CityInfoData | undefined = dataManager.dataMap.getCityDetail(cityId);
        if (!cityDetail) {
            Logger.error("error cityId:", cityId);
            return;
        }
        this.cityDetail = cityDetail;
        this.posLab.string = `${cityDetail!.pos}`;
        this.armyContainer.children.forEach((child) => {
            child.active = false;
        });
        if (cityDetail.armyData) {
            let idx = 0;
            for (const soldierId in cityDetail.armyData) {
                const soldierNum = cityDetail.armyData[soldierId];
                let soldierItem = this.armyContainer.children[idx];
                if (!soldierItem) {
                    soldierItem = instantiate(this.armyContainer.children[0]);
                    soldierItem.parent = this.armyContainer;
                }
                soldierItem.active = true;
                const soldierCfgData = XLSX.SOLDIER[soldierId];
                soldierItem.getChildByName("lab_name")!.getComponent(Label)!.string = `${soldierCfgData.name}`;
                soldierItem.getChildByName("lab_num")!.getComponent(Label)!.string = `x ${getNumFloor(soldierNum)}`;
                idx += 1;
            }
        }
        // this.armyContainer.string = `${cityDetail?.armyData.armyNum}`;

        this.optBtns.children.forEach((child) => {
            child.active = false;
        });

        if (cityDetail.belong === BELONG_TYPE.SELF_SIDE) {
            // 己方
            this.btnAttack.active = true;
            // this.btnPlunder.active = true;
            this.btnRemove.active = true;
            this.btnDispatch.active = true;
        } else {
            this.btnBeAttack.active = true;
            // this.btnBePlunder.active = true;
        }
    }

    requestAttack() {
        if (this.cityDetail && this.cityDetail.belong === BELONG_TYPE.SELF_SIDE) {
            Engine.eventManager.emit(EVENT_NAME.READY_ATTACK_CITY, this.cityDetail.id, this.cityDetail.belong);
            this.close();
        }
    }

    requestBeAttack() {
        if (this.cityDetail && this.cityDetail.belong !== BELONG_TYPE.SELF_SIDE) {
            Engine.eventManager.emit(EVENT_NAME.READY_BE_ATTACK_CITY, this.cityDetail.id, this.cityDetail.belong);
            this.close();
        }
    }

    requestRemoveBuild() {
        if (this.cityDetail && this.cityDetail.belong === BELONG_TYPE.SELF_SIDE) {
            dataManager.dataPlayer.removeBuilding(this.cityDetail.id);
            this.close();
        }
    }
}
