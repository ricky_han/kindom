import { _decorator, Component, Node, Label, Sprite, SpriteFrame, instantiate } from "cc";
import { BasePanel } from "../../core/component/BasePanel";
import { Engine } from "../../core/Engine";
import { uiManager } from "../../core/manager/UIManager";
import Logger from "../../core/utils/Logger";
import { TabMenu } from "../component/TabMenu";
import { dataManager } from "../data/DataManager";
import { BELONG_TYPE, CityInfoData } from "../data/MapData";
import { EVENT_NAME, UI_NAME } from "../GameConstant";
import { BattleLogItem } from "./BattleLogItem";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = BattleLogPanel
 * DateTime = Sun Dec 19 2021 01:19:18 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = CityInfoPanel.ts
 * FileBasenameNoExtension = CityInfoPanel
 * URL = db://assets/scripts/game/map/CityInfoPanel.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("BattleLogPanel")
export class BattleLogPanel extends BasePanel {
    @property({ type: Node, displayName: "列表容器" })
    container!: Node;

    @property({ type: TabMenu, displayName: "分类" })
    logMenu!: TabMenu;

    private currentTabId = 0;
    start() {
        this.logMenu.initMenus(null, 0, this.filterLogCategory, this);
    }

    show() {
        this.filterLogCategory(0);
    }

    private filterLogCategory(tabIdx: number) {
        this.currentTabId = tabIdx;

        this.container.children.forEach((child) => {
            child.active = false;
        });
        const reportList = dataManager.dataMap.getBattleReportLog()!;
        reportList.forEach((report, idx: number) => {
            if (
                (tabIdx === 0 && report.belong === BELONG_TYPE.SELF_SIDE) ||
                (tabIdx === 1 && report.belong === BELONG_TYPE.ENEMY_SIDE)
            ) {
                let reportItem = this.container.children[idx];
                if (!reportItem) {
                    reportItem = instantiate(this.container.children[0]);
                    reportItem.parent = this.container;
                }
                reportItem.active = true;
                reportItem.getComponent(BattleLogItem)!.setData(report);
            }
        });
    }
}
