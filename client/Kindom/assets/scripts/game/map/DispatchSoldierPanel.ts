import { _decorator, Node, instantiate, Label, Toggle, EditBox } from "cc";
import { BasePanel } from "../../core/component/BasePanel";
import { Engine } from "../../core/Engine";
import { dataManager } from "../data/DataManager";
import { BELONG_TYPE } from "../data/MapData";
import { EVENT_NAME } from "../GameConstant";
import { DispatchSoldierItem } from "./DispatchSoldierItem";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = BuildListPanel
 * DateTime = Sun Dec 19 2021 23:18:29 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = BuildListPanel.ts
 * FileBasenameNoExtension = BuildListPanel
 * URL = db://assets/scripts/game/map/BuildListPanel.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("DispatchSoldierPanel")
export class DispatchSoldierPanel extends BasePanel {
    @property({ type: Node, displayName: "士兵信息" })
    armyContainer!: Node;

    @property({ type: Toggle, displayName: "快速出兵" })
    quickChooseToggle!: Toggle;

    private startCityId!: string;
    private targetCityId!: string;
    private dispatchArmyList: { [soldierId: string]: number } = {};
    onLoad() {
        Engine.eventManager.on(
            EVENT_NAME.CHOOSE_DISPATCH_SOLDIER_NUM,
            (soldierId: string, num: number) => {
                this.dispatchArmyList[soldierId] = num;
            },
            this
        );
    }

    show(startCityId: string, targetCityId: string) {
        this.startCityId = startCityId;
        this.targetCityId = targetCityId;
        const isChecked = this.quickChooseToggle.isChecked;
        let idx = 0;
        for (const soldierId in dataManager.dataMap.cityList[startCityId].soldiers) {
            const soldierNum = dataManager.dataMap.cityList[startCityId].soldiers[soldierId];
            if (soldierNum !== 0) {
                let soldierItem = this.armyContainer.children[idx];
                if (!soldierItem) {
                    soldierItem = instantiate(this.armyContainer.children[0]);
                    soldierItem.parent = this.armyContainer;
                }
                soldierItem.active = true;
                soldierItem.getComponent(DispatchSoldierItem)!.setData(soldierId, soldierNum, isChecked);
                idx += 1;
            }
        }

        this.changeQuickChooseSoldiers();
    }

    // 出兵
    private dispatchArmy() {
        dataManager.dataMap.dispatchArmy(
            this.startCityId,
            this.targetCityId,
            this.dispatchArmyList,
            BELONG_TYPE.SELF_SIDE
        );
        this.close();
    }

    // 改变快速出兵状态
    changeQuickChooseSoldiers() {
        const isChecked = this.quickChooseToggle.isChecked;
        if (isChecked) {
            this.dispatchArmyList = dataManager.dataSoldier.floatSoldierNumToIntSoldiers(
                dataManager.dataMap.cityList[this.startCityId].soldiers
            );
        } else {
            this.dispatchArmyList = {};
        }
        this.armyContainer.children.forEach((soldierItem) => {
            const soldierItemComp = soldierItem.getComponent(DispatchSoldierItem);
            soldierItemComp!.setQuickState(isChecked);
        });
    }
}
