import { _decorator, Component, Node, game } from "cc";
import { Engine } from "../core/Engine";
import { i18n } from "../core/manager/LanguageManager";
import { dataManager } from "./data/DataManager";
import { gameRecordManager } from "./manager/GameRecordManager";

const { ccclass, property } = _decorator;

@ccclass("Game")
export class Game extends Component {
    private _passTime = 0;

    onLoad() {
        Engine.init();
        i18n.setLanguage("en");
        dataManager.init();

        game.addPersistRootNode(this.node);
    }

    start() {
        // [3]
    }

    update(deltaTime: number) {
        this._passTime += deltaTime;
        if (this._passTime >= 1) {
            this._passTime -= 1;
            Engine.timeManager.update(1);
        }
    }
}
