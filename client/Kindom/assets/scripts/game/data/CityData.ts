import { Asset, JsonAsset } from "cc";
import { Engine } from "../../core/Engine";
import Logger from "../../core/utils/Logger";
import { BaseData } from "./BaseData";

/**
 * Predefined variables
 * Name = CityData
 * DateTime = Sun Dec 19 2021 00:07:38 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = CityData.ts
 * FileBasenameNoExtension = CityData
 * URL = db://assets/scripts/game/data/CityData.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */
// 解析每个城池的数据，动态城池数据自增

export class CityData extends BaseData {
    protected init() {}

    public initData() {}
}
