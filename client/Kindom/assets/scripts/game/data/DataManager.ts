import Logger from "../../core/utils/Logger";
import { AccountData } from "./AccountData";
import { EnemyData } from "./EnemyData";
import { MapData } from "./MapData";
import { PlayerData } from "./PlayerData";
import { SoldierData } from "./SoldierData";

/**
 * Predefined variables
 * Name = DataManager
 * DateTime = Sun Dec 19 2021 00:42:55 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = DataManager.ts
 * FileBasenameNoExtension = DataManager
 * URL = db://assets/scripts/game/data/DataManager.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */
class DataManager {
    public static instance = new DataManager();

    public dataAccount!: AccountData;
    public dataMap!: MapData;
    public dataPlayer!: PlayerData;
    public dataEnemy!: EnemyData;
    public dataSoldier!: SoldierData;

    init() {
        this.dataAccount = new AccountData();
        this.dataMap = new MapData();
        this.dataPlayer = new PlayerData();
        Logger.log("11", this.dataPlayer.resourceList);
        this.dataEnemy = new EnemyData();
        this.dataSoldier = new SoldierData();
    }
}

export const dataManager = DataManager.instance;
