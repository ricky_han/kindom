/**
 * Predefined variables
 * Name = BaseData
 * DateTime = Sun Dec 19 2021 00:07:38 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = MapData.ts
 * FileBasenameNoExtension = MapData
 * URL = db://assets/scripts/game/data/MapData.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */
export class BaseData {
    protected dataName: string = "BaseData";
    protected _init = false;
    constructor() {
        this.init();
    }

    // 初始化数据
    protected init() {
        this._init = true;
    }

    public isInit(): boolean {
        return this._init;
    }

    protected buildEventName(option: string, prop?: string) {
        if (prop) {
            return option + this.dataName + prop;
        }
        return option + this.dataName;
    }
}
