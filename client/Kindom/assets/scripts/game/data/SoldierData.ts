import { v2, Vec2 } from "cc";
import { getNowTime, getNumFloor } from "../../core/utils/utils";
import { GAME_CONFIG, GAME_DATA } from "../GameConstant";
import { BaseData } from "./BaseData";
import { dataManager } from "./DataManager";
import { BELONG_TYPE } from "./MapData";

export const ATTACK_TYPE = {
    PHYSICAL: 1,
    FIRE: 2,
    ELECTRIC: 3,
    ICE: 4,
    LIGHT: 5,
    DARK: 6,
};

export const ATTACK_OR_DEFENSE = {
    ATTACK: 1,
    DEFENSE: 2,
};

export interface DispatchArmyInfo {
    armyId: string;
    soldiers: { [soldierId: string]: number };
    fromCityId: string;
    toCityId: string;
    currentCityId: string;
    nextCityId: string;
    currentCityPassTime: number;
    startTime: number;
    passTime: number;
    paths: string[];
    belong: BELONG_TYPE;
}

export class SoldierData extends BaseData {
    protected init() {
        super.init();
        // 初始 资源
    }

    // 士兵信息
    getSoldierCfgData(soldierId: string) {
        const soldierCfg = XLSX.SOLDIER[soldierId];
        if (!soldierCfg) {
            console.error("error soldierId:", soldierId);
        }
        return soldierCfg;
    }

    // 胜利  城池兵力合并
    combineCityArmy(soldierInfo1: { [soldierId: string]: number }, soldierInfo2: { [soldierId: string]: number }) {
        const totalArmy: { [soldierId: string]: number } = {};
        for (const soldierId in soldierInfo1) {
            if (!totalArmy[soldierId]) {
                totalArmy[soldierId] = Number(soldierInfo1[soldierId]);
            } else {
                totalArmy[soldierId] += Number(soldierInfo1[soldierId]);
            }
        }
        for (const soldierId in soldierInfo2) {
            if (!totalArmy[soldierId]) {
                totalArmy[soldierId] = Number(soldierInfo2[soldierId]);
            } else {
                totalArmy[soldierId] += Number(soldierInfo2[soldierId]);
            }
        }
        return totalArmy;
    }

    // 计算部队攻击力
    calculateArmyAttackPower(armyData: { [soldierId: string]: number }) {
        const attackTypePower: { [type: number]: number } = {};
        for (const soldierId in armyData) {
            const soldierCfg = XLSX.SOLDIER[soldierId];
            const soldierNum = armyData[soldierId];
            const addPower = soldierCfg.attack * soldierNum;
            attackTypePower[soldierCfg.attackType] = attackTypePower[soldierCfg.attackType]
                ? attackTypePower[soldierCfg.attackType] + addPower
                : addPower;
        }
        return attackTypePower;
    }

    // 战力数值
    getPowerTotalNum(armyTypePower: { [type: number]: number }) {
        let powerTotalNum = 0;
        for (const key in armyTypePower) {
            powerTotalNum += armyTypePower[key];
        }
        return powerTotalNum;
    }

    // 计算部队防御力（需根据各攻击力比例计算）
    calculateArmyDefPower(armyData: { [soldierId: string]: number }, attackRatio?: { [type: number]: number }) {
        let totalAttackPower = 0;
        if (attackRatio) {
            for (const type in attackRatio) {
                totalAttackPower += attackRatio[type];
            }
        }
        const defTypePower: { [type: number]: number } = {};
        for (const soldierId in armyData) {
            const soldierCfg = XLSX.SOLDIER[soldierId];
            const soldierNum = armyData[soldierId];
            defTypePower[ATTACK_TYPE.PHYSICAL] =
                (defTypePower[ATTACK_TYPE.PHYSICAL] ? defTypePower[ATTACK_TYPE.PHYSICAL] : 0) +
                soldierCfg.physicalDef * soldierNum;
            defTypePower[ATTACK_TYPE.FIRE] =
                (defTypePower[ATTACK_TYPE.FIRE] ? defTypePower[ATTACK_TYPE.FIRE] : 0) + soldierCfg.fireDef * soldierNum;
            defTypePower[ATTACK_TYPE.ELECTRIC] =
                (defTypePower[ATTACK_TYPE.ELECTRIC] ? defTypePower[ATTACK_TYPE.ELECTRIC] : 0) +
                soldierCfg.electricDef * soldierNum;
            defTypePower[ATTACK_TYPE.ICE] =
                (defTypePower[ATTACK_TYPE.ICE] ? defTypePower[ATTACK_TYPE.ICE] : 0) + soldierCfg.iceDef * soldierNum;
            defTypePower[ATTACK_TYPE.LIGHT] =
                (defTypePower[ATTACK_TYPE.LIGHT] ? defTypePower[ATTACK_TYPE.LIGHT] : 0) +
                soldierCfg.lightDef * soldierNum;
            defTypePower[ATTACK_TYPE.DARK] =
                (defTypePower[ATTACK_TYPE.DARK] ? defTypePower[ATTACK_TYPE.DARK] : 0) + soldierCfg.darkDef * soldierNum;
        }

        let effectDefTypePower: { [type: number]: number } = {};
        if (attackRatio) {
            // 对应攻击属性，生效的防御力
            for (const key in attackRatio) {
                const type = Number(ATTACK_TYPE[key as keyof typeof ATTACK_TYPE]);
                const attackTypeRadio = (attackRatio[type] || 0) / totalAttackPower;
                effectDefTypePower[type] = (defTypePower[type] ? defTypePower[type] : 0) * attackTypeRadio;
            }
        } else {
            // 模拟防御力，都计算进去
            effectDefTypePower = defTypePower;
        }
        return effectDefTypePower;
    }

    //  根据时间计算部队的行进位置
    calculateCurrentArmyInfoData(armyData: DispatchArmyInfo) {
        // const paths = armyData.paths;
        // const passTime = armyData.passTime;
        // let leftLen = passTime * GAME_DATA.ARMY_SPEED;
        // let startId = paths[0];
        // const armyPos: Vec2 = new Vec2();
        // let leftPaths: string[] = [];
        // for (let index = 0; index < paths.length; index++) {
        //     if (startId !== paths[index]) {
        //         const cityId = paths[index];
        //         const cityLine = dataManager.dataMap.getTwoCityLine(startId, cityId);
        //         if (!cityLine) {
        //             break;
        //         }
        //         leftLen -= cityLine.len;
        //         if (leftLen <= 0) {
        //             startId = startId;
        //             const startCityData = dataManager.dataMap.getCityDetail(startId)!;
        //             const cityData = dataManager.dataMap.getCityDetail(cityId)!; // terrianList[cityId];
        //             const [startCityX, startCityY] = startCityData.pos.split("-");
        //             const [cityX, cityY] = cityData.pos.split("-");
        //             const passRadio = leftLen / cityLine.len;
        //             armyPos.x = Number(startCityX) + (Number(cityX) - Number(startCityX)) * passRadio;
        //             armyPos.y = Number(startCityY) + (Number(cityY) - Number(startCityY)) * passRadio;
        //             leftPaths = paths.slice(index!);
        //             break;
        //         }
        //     }
        // }
        // armyData.currentCityId = startId;
        // return { leftPaths, armyPos };
        const cityLine = dataManager.dataMap.getTwoCityLine(armyData.currentCityId, armyData.nextCityId);
        if (!cityLine) {
            console.error("no path!", armyData.paths);

            return v2(0, 0);
        }
        const currentCityCostTime = cityLine.len / GAME_DATA.ARMY_SPEED;
        const passRadio = armyData.currentCityPassTime / currentCityCostTime;
        const startCityData = dataManager.dataMap.getCityDetail(armyData.currentCityId)!;
        const [startCityX, startCityY] = startCityData.pos.split("-");
        const cityData = dataManager.dataMap.getCityDetail(armyData.nextCityId)!;
        const [cityX, cityY] = cityData.pos.split("-");
        return v2(
            Number(startCityX) + (Number(cityX) - Number(startCityX)) * passRadio,
            Number(startCityY) + (Number(cityY) - Number(startCityY)) * passRadio
        );
    }

    // 城市士兵转换为派遣士兵，整数派遣
    floatSoldierNumToIntSoldiers(soldiers: { [soldierId: string]: number }) {
        const intSoldiers: { [soldierId: string]: number } = {};
        for (const soldierId in soldiers) {
            const soldierNum = soldiers[soldierId];
            intSoldiers[soldierId] = getNumFloor(soldierNum);
        }
        return intSoldiers;
    }
}
