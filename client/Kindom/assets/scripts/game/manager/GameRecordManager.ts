import { Engine } from "../../core/Engine";
import Logger from "../../core/utils/Logger";
import { dataManager } from "../data/DataManager";
import { GAME_CONFIG, TIMER_TASK_TAG } from "../GameConstant";

export const GAME_ACCOUNT_KEY = {
    STORE_PLAYER_ACCOUNT: "account",
    STORE_PLAYER_ROLES: "roles",
};

// 存储数据key
export const PLAYER_STORAGE_KEY = {
    STORE_PLAYER_LIFE_NUM: "1",
    STORE_PLAYER_RESOURCE: "2",
    STORE_CITY_LIST: "3",
    STORE_BUILD_LIST: "4",
    STORE_RESOURCE_LIST: "5",
    STORE_DISPATCH_ARMY_LIST: "6",

    // 敌兵数据
    STORE_ENEMY_BUILD_LIST: "104",
    STORE_ENEMY_RESOURCE_LIST: "105",
    STORE_ENEMY_DISPATCH_ARMY_LIST: "106",
    STORE_BATTLE_REPORT_LIST: "107",
};

class GameRecordManager {
    //存储到本地的信息
    //存储到云端或其他平台
    private _recordData: { [key: string]: any } = {};
    private _needRefresh = false; // 是否有待存储数据
    private _needRecordData: { [key: string]: any } = {};

    private globalKeys = [
        GAME_ACCOUNT_KEY.STORE_PLAYER_ACCOUNT,
        GAME_ACCOUNT_KEY.STORE_PLAYER_ROLES,
    ]; // 全局

    public static instance: GameRecordManager = new GameRecordManager();
    private constructor() {}

    private getStoreKey(key: string) {
        let storeKey = key;
        if (this.globalKeys.indexOf(key) === -1) {
            storeKey = key + "-" + dataManager.dataAccount.currentRoleId;
        }
        return storeKey;
    }
    private setStorageItem(key: string, value: any) {
        const storeKey = this.getStoreKey(key);
        this._recordData[storeKey] = value;
        if (GAME_CONFIG.PLATFORM == "LOCAL") {
            Engine.storageManager.setItem(storeKey, value + "");
        } else {
            let data = {};
            // data[key] = value;
            // FaceBookSDK.getInstance().setPlayerDataAsync(data);
        }
    }

    //初始化的时候存储在_recordData中了
    private getStorageItem(key: string) {
        const storeKey = this.getStoreKey(key);
        return this._recordData[storeKey];
    }

    private removeStorageItem(key: string) {
        if (GAME_CONFIG.PLATFORM == "LOCAL") {
            Engine.storageManager.removeItem(key + "");
        } else {
            let data = {};
            // data[key] = "";
        }
    }

    async getPlayerCloudData() {
        let fbData = {}; //await FaceBookSDK.getInstance().getPlayerDataAsync(this.STORE_KEY_LSIT);
        return fbData;
    }

    //
    async getStoreLocalData() {
        let localStoreData: { [key: string]: string | null } = {};
        for (const key in PLAYER_STORAGE_KEY) {
            let storeKey =
                PLAYER_STORAGE_KEY[key as keyof typeof PLAYER_STORAGE_KEY];
            storeKey = this.getStoreKey(storeKey);
            localStoreData[storeKey] = Engine.storageManager.getItem(storeKey);
        }
        return localStoreData;
    }

    //初始化游戏数据
    async initGameData() {
        if (GAME_CONFIG.PLATFORM == "LOCAL") {
            this._recordData = await this.getStoreLocalData();
        } else {
            this._recordData = await this.getPlayerCloudData();
        }

        Engine.timeManager.registerTimer(
            TIMER_TASK_TAG.TIMER_GAME_RECORD,
            10,
            0,
            this.setPlayerDataImmediately,
            undefined,
            this
        );
    }

    //重置游戏数据
    resetGameData() {
        for (const key in PLAYER_STORAGE_KEY) {
            const storeKey = this.getStoreKey(
                PLAYER_STORAGE_KEY[key as keyof typeof PLAYER_STORAGE_KEY]
            );
            this.removeStorageItem(storeKey);
        }
        this.globalKeys.forEach((storeKey) => {
            this.removeStorageItem(storeKey);
        });
        this._needRecordData = {};
        this._recordData = {};
    }

    //存储玩家数据
    setPlayerDataByKey(key: string, value: any) {
        value = JSON.stringify(value);
        this._needRecordData[key] = value;
        this.markRefresh();
    }

    // 立即存储到缓存
    setPlayerDataImmediately() {
        if (this._needRefresh) {
            for (const key in this._needRecordData) {
                const value = this._needRecordData[key];
                this.setStorageItem(key, value);
            }
        }
        this._needRecordData = {};
        this._needRefresh = false;
    }

    // 存储标识
    private markRefresh() {
        this._needRefresh = true;
    }

    //读取玩家数据
    getPlayerDataByKey(key: string) {
        let keyValue: string = this.getStorageItem(key);
        return keyValue;
    }
}

export const gameRecordManager = GameRecordManager.instance;
