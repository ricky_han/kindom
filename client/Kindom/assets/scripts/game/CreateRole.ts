import { _decorator, Component, Node, instantiate, EditBox, director, Label } from "cc";
import { BaseScene } from "../core/component/BaseScene";
import ClickDelegate from "../core/component/ClickDelegate";
import { Engine } from "../core/Engine";
import Logger from "../core/utils/Logger";
import { dataManager } from "./data/DataManager";
import { DATA_NAME, DATA_OPTION } from "./GameConstant";
import { gameRecordManager } from "./manager/GameRecordManager";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = CreateRole
 * DateTime = Thu Dec 23 2021 23:36:16 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = CreateRole.ts
 * FileBasenameNoExtension = CreateRole
 * URL = db://assets/scripts/game/CreateRole.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("CreateRole")
export class CreateRole extends BaseScene {
    @property({ type: Node, displayName: "无角色" })
    noRoleNode!: Node;
    @property({ type: EditBox, displayName: "角色id" })
    inputRole!: EditBox;

    @property({ type: Node, displayName: "角色列表" })
    roleList!: Node;

    onLoad() {
        super.onLoad();
        if (dataManager.dataAccount.isInit()) {
            this.resetView();
        } else {
            Engine.eventManager.on(DATA_OPTION.INIT + DATA_NAME.ACCOUNT_DATA, this.resetView, this);
        }
    }

    resetView() {
        if (dataManager.dataAccount.playerRoles) {
            this.noRoleNode.active = false;
            this.roleList.active = true;
            let roleNum = 0;
            for (const roleId in dataManager.dataAccount.playerRoles) {
                const roleData = dataManager.dataAccount.playerRoles[roleId];
                let roleItemNode = this.roleList.children[roleNum];
                if (!roleItemNode) {
                    roleItemNode = instantiate(this.roleList.children[0]);
                    roleItemNode.parent = this.roleList;
                }
                roleItemNode.getChildByName("role_name")!.getComponent(Label)!.string = `${roleId}`;
                roleItemNode.active = true;
                roleItemNode.getComponent(ClickDelegate)!.init(
                    this,
                    (roleId: string) => {
                        this.chooseRole(roleId);
                    },
                    roleId
                );
                roleNum++;
            }
        } else {
            this.noRoleNode.active = true;
            this.roleList.active = false;
        }
    }

    private chooseRole(roleId: string) {
        let loadIdx = 0;
        dataManager.dataAccount.chooseRole(roleId).then(() => {
            loadIdx++;
            if (loadIdx == 2) {
                director.loadScene("WorldMap");
            }
        });
        director.preloadScene("WorldMap", () => {
            loadIdx++;
            if (loadIdx == 2) {
                director.loadScene("WorldMap");
            }
        });
    }

    private creatRole(event: Event) {
        const roleId = this.inputRole.string;
        if (roleId) {
            this.chooseRole(roleId);
        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}
