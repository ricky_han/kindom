import { _decorator, Component, Node, UIComponent, Sprite, Label } from "cc";
import { BaseComponent } from "../../core/component/BaseComponent";
import { getNumFloor } from "../../core/utils/utils";
import { dataManager } from "../data/DataManager";
const { ccclass, property } = _decorator;

/**
 * Predefined variables
 * Name = materialItem
 * DateTime = Sat Dec 25 2021 15:17:13 GMT+0800 (中国标准时间)
 * Author = han2007happy
 * FileBasename = materialItem.ts
 * FileBasenameNoExtension = materialItem
 * URL = db://assets/scripts/game/component/materialItem.ts
 * ManualUrl = https://docs.cocos.com/creator/3.3/manual/en/
 *
 */

@ccclass("MaterialItem")
export class MaterialItem extends BaseComponent {
    @property({ type: Sprite, displayName: "资源图" })
    icon!: Sprite;

    @property({ type: Label, displayName: "资源名称" })
    materialName!: Label;

    @property({ type: Label, displayName: "资源数量" })
    materialQuantity!: Label;

    private materialCfgData: any;

    setData(materialId: string, quantity: number) {
        if (materialId.startsWith("material-")) {
            this.materialCfgData = dataManager.dataMap.getMaterialCfg(materialId);
        } else if (materialId.startsWith("soldier-")) {
            this.materialCfgData = dataManager.dataMap.getSoldierCfg(materialId);
        }

        this.materialName.string = this.materialCfgData.name;
        this.materialQuantity.string = `${getNumFloor(quantity)}`;
    }

    // 数量更新
    updateQuantity(quantity: number) {
        this.materialQuantity.string = `${quantity}`;
    }
}
