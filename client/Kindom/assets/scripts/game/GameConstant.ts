export const UI_NAME = {
    TEST_LAYER: "test_layer",
    TEST_PANEL: "test_panel",
    TEST_POPUP: "test_popup",
    TEST_ALERT: "test_alert",
    COMMON_ALERT: "common_alert",
    CITY_INFO_PANEL: "city_info_panel",
    BUILD_INFO_PANEL: "build_info_panel",
    BUILD_LIST_PANEL: "build_list_panel",
    DISPATCH_SOLDIER_PANEL: "dispatch_soldier_panel",
    BATTLE_LOG_PANEL: "battle_log_panel",
};

export const EVENT_NAME = {
    WORLD_MAP_INITED: "world_map_inited",
    BUILDING_ON_CITY: "building_on_city",
    REMOVE_BUILDING_ON_CITY: "remove_building_on_city",
    UPDATE_CITY: "update_city",
    READY_ATTACK_CITY: "ready_attack_city",
    READY_BE_ATTACK_CITY: "ready_be_attack_city",

    // 出兵部队
    CHOOSE_DISPATCH_SOLDIER_NUM: "choose_dispatch_soldier_num",
    START_DISPATCH_ARMY: "start_dispatch_army",
    STOP_DISPATCH_ARMY: "stop_dispatch_army",
    ARMY_CHANGE_CURRENT_CITY: "army_change_current_city",
    FAST_TO_TARGET_CITY: "fast_to_target_city",

    // 战斗
    BATTLE_START: "battle_start",
    BATTLE_END: "battle_end",

    // 城池归属改变
    CHANGE_CITY_BELONG_TYPE: "change_city_belong_type",

    GAME_OVER: "game_over",
};

export const TIMER_TASK_TAG = {
    TIMER_GAME_RECORD: "timer_game_record",
    TIMER_GAME_RESOURCE_ADD: "timer_game_resource_add",
    TIMER_GAME_DISPATCH_ARMY_UPDATE: "timer_game_dispatch_army_update",
    TIMER_GAME_ENEMY_AI_CHECK: "timer_game_enemy_ai_check",
};

export const GAME_DATA = {
    ARMY_SPEED: 50,
};

export const GAME_CONFIG = {
    PLATFORM: "LOCAL",
};

export const DATA_NAME = {
    ACCOUNT_DATA: "account_data",
    PLAYER_DATA: "player_data",
    ENEMY_DATA: "enemy_data",
    MAP_DATA: "map_data",
};
export const DATA_OPTION = {
    INIT: "init",
    UPDATE: "update",
};

export const DATA_PROPERTY = {
    materialS: "materials",
    soldiers: "soldiers",
    SOLDIER_KILL: "soldier_kill",
};
