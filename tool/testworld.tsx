<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="testworld" tilewidth="512" tileheight="512" tilecount="64" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <image width="512" height="512" source="F:/world/map/block1_1.jpg"/>
 </tile>
 <tile id="2">
  <image width="512" height="512" source="F:/world/map/block1_2.jpg"/>
 </tile>
 <tile id="3">
  <image width="512" height="512" source="F:/world/map/block1_3.jpg"/>
 </tile>
 <tile id="4">
  <image width="512" height="512" source="F:/world/map/block1_4.jpg"/>
 </tile>
 <tile id="5">
  <image width="512" height="512" source="F:/world/map/block1_5.jpg"/>
 </tile>
 <tile id="6">
  <image width="512" height="512" source="F:/world/map/block1_6.jpg"/>
 </tile>
 <tile id="7">
  <image width="512" height="512" source="F:/world/map/block1_7.jpg"/>
 </tile>
 <tile id="8">
  <image width="512" height="512" source="F:/world/map/block1_8.jpg"/>
 </tile>
 <tile id="9">
  <image width="512" height="512" source="F:/world/map/block2_1.jpg"/>
 </tile>
 <tile id="10">
  <image width="512" height="512" source="F:/world/map/block2_2.jpg"/>
 </tile>
 <tile id="11">
  <image width="512" height="512" source="F:/world/map/block2_3.jpg"/>
 </tile>
 <tile id="12">
  <image width="512" height="512" source="F:/world/map/block2_4.jpg"/>
 </tile>
 <tile id="13">
  <image width="512" height="512" source="F:/world/map/block2_5.jpg"/>
 </tile>
 <tile id="14">
  <image width="512" height="512" source="F:/world/map/block2_6.jpg"/>
 </tile>
 <tile id="15">
  <image width="512" height="512" source="F:/world/map/block2_7.jpg"/>
 </tile>
 <tile id="16">
  <image width="512" height="512" source="F:/world/map/block2_8.jpg"/>
 </tile>
 <tile id="17">
  <image width="512" height="512" source="F:/world/map/block3_1.jpg"/>
 </tile>
 <tile id="18">
  <image width="512" height="512" source="F:/world/map/block3_2.jpg"/>
 </tile>
 <tile id="19">
  <image width="512" height="512" source="F:/world/map/block3_3.jpg"/>
 </tile>
 <tile id="20">
  <image width="512" height="512" source="F:/world/map/block3_4.jpg"/>
 </tile>
 <tile id="21">
  <image width="512" height="512" source="F:/world/map/block3_5.jpg"/>
 </tile>
 <tile id="22">
  <image width="512" height="512" source="F:/world/map/block3_6.jpg"/>
 </tile>
 <tile id="23">
  <image width="512" height="512" source="F:/world/map/block3_7.jpg"/>
 </tile>
 <tile id="24">
  <image width="512" height="512" source="F:/world/map/block3_8.jpg"/>
 </tile>
 <tile id="25">
  <image width="512" height="512" source="F:/world/map/block4_1.jpg"/>
 </tile>
 <tile id="26">
  <image width="512" height="512" source="F:/world/map/block4_2.jpg"/>
 </tile>
 <tile id="27">
  <image width="512" height="512" source="F:/world/map/block4_3.jpg"/>
 </tile>
 <tile id="28">
  <image width="512" height="512" source="F:/world/map/block4_4.jpg"/>
 </tile>
 <tile id="29">
  <image width="512" height="512" source="F:/world/map/block4_5.jpg"/>
 </tile>
 <tile id="30">
  <image width="512" height="512" source="F:/world/map/block4_6.jpg"/>
 </tile>
 <tile id="31">
  <image width="512" height="512" source="F:/world/map/block4_7.jpg"/>
 </tile>
 <tile id="32">
  <image width="512" height="512" source="F:/world/map/block4_8.jpg"/>
 </tile>
 <tile id="33">
  <image width="512" height="512" source="F:/world/map/block5_1.jpg"/>
 </tile>
 <tile id="34">
  <image width="512" height="512" source="F:/world/map/block5_2.jpg"/>
 </tile>
 <tile id="35">
  <image width="512" height="512" source="F:/world/map/block5_3.jpg"/>
 </tile>
 <tile id="36">
  <image width="512" height="512" source="F:/world/map/block5_4.jpg"/>
 </tile>
 <tile id="37">
  <image width="512" height="512" source="F:/world/map/block5_5.jpg"/>
 </tile>
 <tile id="38">
  <image width="512" height="512" source="F:/world/map/block5_6.jpg"/>
 </tile>
 <tile id="39">
  <image width="512" height="512" source="F:/world/map/block5_7.jpg"/>
 </tile>
 <tile id="40">
  <image width="512" height="512" source="F:/world/map/block5_8.jpg"/>
 </tile>
 <tile id="41">
  <image width="512" height="512" source="F:/world/map/block6_1.jpg"/>
 </tile>
 <tile id="42">
  <image width="512" height="512" source="F:/world/map/block6_2.jpg"/>
 </tile>
 <tile id="43">
  <image width="512" height="512" source="F:/world/map/block6_3.jpg"/>
 </tile>
 <tile id="44">
  <image width="512" height="512" source="F:/world/map/block6_4.jpg"/>
 </tile>
 <tile id="45">
  <image width="512" height="512" source="F:/world/map/block6_5.jpg"/>
 </tile>
 <tile id="46">
  <image width="512" height="512" source="F:/world/map/block6_6.jpg"/>
 </tile>
 <tile id="47">
  <image width="512" height="512" source="F:/world/map/block6_7.jpg"/>
 </tile>
 <tile id="48">
  <image width="512" height="512" source="F:/world/map/block6_8.jpg"/>
 </tile>
 <tile id="49">
  <image width="512" height="512" source="F:/world/map/block7_1.jpg"/>
 </tile>
 <tile id="50">
  <image width="512" height="512" source="F:/world/map/block7_2.jpg"/>
 </tile>
 <tile id="51">
  <image width="512" height="512" source="F:/world/map/block7_3.jpg"/>
 </tile>
 <tile id="52">
  <image width="512" height="512" source="F:/world/map/block7_4.jpg"/>
 </tile>
 <tile id="53">
  <image width="512" height="512" source="F:/world/map/block7_5.jpg"/>
 </tile>
 <tile id="54">
  <image width="512" height="512" source="F:/world/map/block7_6.jpg"/>
 </tile>
 <tile id="55">
  <image width="512" height="512" source="F:/world/map/block7_7.jpg"/>
 </tile>
 <tile id="56">
  <image width="512" height="512" source="F:/world/map/block7_8.jpg"/>
 </tile>
 <tile id="57">
  <image width="512" height="512" source="F:/world/map/block8_1.jpg"/>
 </tile>
 <tile id="58">
  <image width="512" height="512" source="F:/world/map/block8_2.jpg"/>
 </tile>
 <tile id="59">
  <image width="512" height="512" source="F:/world/map/block8_3.jpg"/>
 </tile>
 <tile id="60">
  <image width="512" height="512" source="F:/world/map/block8_4.jpg"/>
 </tile>
 <tile id="61">
  <image width="512" height="512" source="F:/world/map/block8_5.jpg"/>
 </tile>
 <tile id="62">
  <image width="512" height="512" source="F:/world/map/block8_6.jpg"/>
 </tile>
 <tile id="63">
  <image width="512" height="512" source="F:/world/map/block8_7.jpg"/>
 </tile>
 <tile id="64">
  <image width="512" height="512" source="F:/world/map/block8_8.jpg"/>
 </tile>
</tileset>
