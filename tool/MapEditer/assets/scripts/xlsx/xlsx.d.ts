// Generated from map.xlsx

declare type Building = {
	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 类型
	 */
	readonly type: number;

	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 建造消耗
	 */
	readonly costMeterials: any;

	/**
	 * 资源产出及速度 10s为单位
	 */
	readonly outMeterials: any;

	/**
	 * 描述
	 */
	readonly des: string;
};

// Generated from map.xlsx

declare type Material = {
	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 系列
	 */
	readonly type: string;

	/**
	 * 系列名称
	 */
	readonly typeName: string;

	/**
	 * 价值
	 */
	readonly price: number;

	/**
	 * 资源上限
	 */
	readonly maxLimit: number;

	/**
	 * 说明
	 */
	readonly des: string;
};

// Generated from map.xlsx

declare type Race = {
	/**
	 * 种族名称
	 */
	readonly raceName: string;
};

// Generated from map.xlsx

declare type Soldier = {
	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 种族
	 */
	readonly race: number;

	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 消耗资源速度 10s为单位
	 */
	readonly eatMeterials: any;

	/**
	 * 描述
	 */
	readonly des: string;
};

// Generated from map.xlsx

declare type Terrain = {
	/**
	 * 资源
	 */
	readonly res: string;

	/**
	 * 名称
	 */
	readonly name: string;

	/**
	 * 地形指定建筑类型
	 */
	readonly buildingIds: number[];

	/**
	 * 描述
	 */
	readonly des: string;
};

declare const XLSX: {
	generationDate: Date;

	/**
	 * Generated from map.xlsx
	 */
	readonly BUILDING: { readonly [id: string]: Building };

	/**
	 * Generated from map.xlsx
	 */
	readonly MATERIAL: { readonly [id: string]: Material };

	/**
	 * Generated from map.xlsx
	 */
	readonly RACE: { readonly [id: string]: Race };

	/**
	 * Generated from map.xlsx
	 */
	readonly SOLDIER: { readonly [id: string]: Soldier };

	/**
	 * Generated from map.xlsx
	 */
	readonly TERRAIN: { readonly [id: string]: Terrain };

	/**
	 * 将参数以“-”连接作为ID在指定sheet中找到匹配的行
	 * @param sheet XLSX中的表
	 * @param args 将被“-”连接的参数
	 * @returns 匹配行的数据
	 */
	readonly findOne: (sheet: any, ...args: any[]) => any;
};
