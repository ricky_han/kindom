window.XLSX = {
	generationDate: new Date(1639651028560),

	/**
	 * Generated from map.xlsx
	 */
	BUILDING: {
		["build-1"]: { name: "薄田", type: 1, res: "build-1", des: "产出粮食为主的建筑，建造极其容易，产出少量粮食" },
		["build-2"]: { name: "铁炉", type: 2, res: "build-2", des: "产出铁" },
		["build-3"]: { name: "锻造之城", type: 3, res: "build-3", des: "消耗极大量铁炼制珍贵的黑铁" },
		["build-4"]: { name: "林地", type: 4, res: "build-4", des: "产出木头" },
		["build-5"]: { name: "采石坑", type: 5, res: "build-5", des: "少量石头" },
		["build-6"]: {
			name: "水晶矿",
			type: 6,
			res: "build-6",
			des: "能产出珍贵的水晶矿（感觉应该只有特定地块才可以造）"
		},
		["build-7"]: { name: "大墓地", type: 7, res: "build-7", des: "产生少量骨骼" },
		["build-8"]: { name: "祭坛", type: 8, res: "build-8", des: "献祭大量尸体，产生死灵精华" }
	},

	/**
	 * Generated from map.xlsx
	 */
	MATERIAL: {
		["material-1"]: {
			name: "难民",
			res: "material-1",
			type: "1",
			typeName: "人类",
			price: 10,
			maxLimit: 100000,
			des: "这个荒凉萧索的世界里，对于人类而言，难民到底是一种资源，还是一种负担？"
		},
		["material-2"]: {
			name: "粮食",
			res: "material-2",
			type: "2",
			typeName: "食物",
			price: 1,
			maxLimit: 100000,
			des: "大多数人类，野兽，等活物需要的生命维持物品"
		},
		["material-3"]: {
			name: "铁矿",
			res: "material-3",
			type: "3",
			typeName: "矿",
			price: 30,
			maxLimit: 100000,
			des: "常见的金属材料"
		},
		["material-4"]: {
			name: "木头",
			res: "material-4",
			type: "4",
			typeName: "木头",
			price: 5,
			maxLimit: 100000,
			des: "常见-基础"
		},
		["material-5"]: {
			name: "石料",
			res: "material-5",
			type: "5",
			typeName: "石头",
			price: 15,
			maxLimit: 100000,
			des: "常见-基础"
		},
		["material-6"]: {
			name: "骨骼",
			res: "material-6",
			type: "6",
			typeName: "尸体",
			price: 20,
			maxLimit: 100000,
			des: "亡灵族常用"
		},
		["material-7"]: {
			name: "黑莲花",
			res: "material-7",
			type: "4",
			typeName: "木头",
			price: 8000,
			maxLimit: 100000,
			des: "蕴含神奇力量的少见花朵，通常有xx守护，灵梯田也有极慢的速度生长而出"
		},
		["material-8"]: {
			name: "钢",
			res: "material-8",
			type: "3",
			typeName: "矿",
			price: 90,
			maxLimit: 100000,
			des: "铁的加强版，消耗木头和铁可炼成"
		},
		["material-9"]: {
			name: "黑铁",
			res: "material-9",
			type: "3",
			typeName: "矿",
			price: 900,
			maxLimit: 100000,
			des: "强度极高而耐热的铁，消耗大量铁炼成"
		},
		["material-10"]: {
			name: "水晶",
			res: "material-10",
			type: "5",
			typeName: "石头",
			price: 10000,
			maxLimit: 100000,
			des: "珍贵的矿石，打造高级兵常用，只要水晶地才可以产"
		},
		["material-11"]: {
			name: "尸体",
			res: "material-11",
			type: "6",
			typeName: "尸体",
			price: 60,
			maxLimit: 100000,
			des: "只能杀敌掉，且是血肉类单位，亡灵族常用"
		},
		["material-12"]: {
			name: "血液",
			res: "material-12",
			type: "6",
			typeName: "尸体",
			price: 180,
			maxLimit: 100000,
			des: "只能杀敌掉，且是血肉类单位，亡灵族，恶魔族常用"
		},
		["material-13"]: {
			name: "龙血",
			res: "material-13",
			type: "6",
			typeName: "尸体",
			price: 5000,
			maxLimit: 100000,
			des: "只能杀血肉类的龙单位才会掉，亡灵族，恶魔族，xx常用"
		},
		["material-14"]: {
			name: "冰之精华",
			res: "material-14",
			type: "6",
			typeName: "尸体",
			price: 15000,
			maxLimit: 100000,
			des: "只有冰系元素怪物才会掉的元素精华"
		},
		["material-15"]: {
			name: "火之精华",
			res: "material-15",
			type: "6",
			typeName: "尸体",
			price: 15000,
			maxLimit: 100000,
			des: "只有火系元素怪物才会掉的元素精华"
		},
		["material-16"]: {
			name: "闪电精华",
			res: "material-16",
			type: "6",
			typeName: "尸体",
			price: 15000,
			maxLimit: 100000,
			des: "只有电系元素怪物才会掉的元素精华"
		},
		["material-17"]: {
			name: "金币",
			res: "material-17",
			type: "8",
			typeName: "金币",
			price: 10000,
			maxLimit: 100000,
			des: "付费币，价值1毛钱"
		}
	},

	/**
	 * Generated from map.xlsx
	 */
	RACE: {
		["1"]: { raceName: "人类" },
		["2"]: { raceName: "恶魔" }
	},

	/**
	 * Generated from map.xlsx
	 */
	SOLDIER: {
		["soldier-1"]: {
			name: "民兵",
			race: 1,
			res: "soldier-1",
			eatMeterials: { "material-2": 1 },
			des: "只要给吃的，什么都能干"
		},
		["soldier-2"]: {
			name: "步兵",
			race: 1,
			res: "soldier-2",
			eatMeterials: { "material-2": 1, "material-4": 1 },
			des: "平庸的士兵，攻防均衡"
		},
		["soldier-3"]: {
			name: "轻骑兵",
			race: 1,
			res: "soldier-3",
			eatMeterials: { "material-2": 1, "material-3": 1 },
			des: "速度，掠夺，攻击，防御低。"
		}
	},

	/**
	 * Generated from map.xlsx
	 */
	TERRAIN: {
		["terrian-1"]: { res: "terrian-1", name: "空地", buildingIds: [], des: "空地，可建造建筑" },
		["terrian-2"]: { res: "terrian-2", name: "矿场", buildingIds: [6], des: "铁，黑铁，水晶" },
		["terrian-3"]: { res: "terrian-3", name: "岩石地表", buildingIds: [5], des: "石头产量提升" },
		["terrian-4"]: { res: "terrian-4", name: "沃土", buildingIds: [1], des: "农田产量提升" },
		["terrian-5"]: { res: "terrian-5", name: "水源", buildingIds: [], des: "农田，林地产量提升" },
		["terrian-6"]: { res: "terrian-6", name: "荒芜", buildingIds: [], des: "骨骼产量提升" },
		["terrian-7"]: { res: "terrian-7", name: "灵地", buildingIds: [], des: "黑莲花，元素精华产量提高" }
	},

	/**
	 * 将参数以“-”连接作为ID在指定sheet中找到匹配的行
	 * @param sheet XLSX中的表
	 * @param args 将被“-”连接的参数
	 * @returns 匹配行的数据
	 */
	findOne: function(sheet) {
		var args = [];
		for (var _i = 1; _i < arguments.length; _i++) {
			args[_i - 1] = arguments[_i];
		}
		var key = args.join("-");
		return sheet[key];
	}
};

for (var key in window.XLSX.BUILDING) {
	Object.defineProperties(window.XLSX.BUILDING[key], {});
}

for (var key in window.XLSX.MATERIAL) {
	Object.defineProperties(window.XLSX.MATERIAL[key], {});
}

for (var key in window.XLSX.RACE) {
	Object.defineProperties(window.XLSX.RACE[key], {});
}

for (var key in window.XLSX.SOLDIER) {
	Object.defineProperties(window.XLSX.SOLDIER[key], {});
}

for (var key in window.XLSX.TERRAIN) {
	Object.defineProperties(window.XLSX.TERRAIN[key], {});
}
