import { _decorator, Component, Node, CCObject, Label, EditBox } from "cc";
import { mapData } from "./MapData";
const { ccclass, property } = _decorator;

@ccclass("CityPropertyItem")
export class CityPropertyItem extends Component {
    @property({ type: Label, displayName: "城市属性" })
    cityProName!: Label;

    @property({ type: Label, displayName: "城市属性值" })
    cityProValue!: Label;

    @property({ type: EditBox, displayName: "可修改的属性值" })
    editCityProValue!: EditBox;

    cityId!: string;
    proKey!: string;
    proValue!: any;

    setData(cityId: string, proKey: string, proValue: any) {
        this.cityId = cityId;
        this.proKey = proKey;
        this.proValue = proValue;

        this.cityProName.string = `${proKey}`;
        if (this.cityProValue) {
            this.cityProValue.string = `${proValue}`;
        } else {
            this.editCityProValue.string = `${proValue}`;
        }
    }

    getPropKeyValue() {
        let propVal;
        if (this.cityProValue) {
            propVal = this.cityProValue.string;
        } else {
            propVal = this.editCityProValue.string;
        }
        if (this.proKey !== "cityName") {
            propVal = Number(propVal);
        }
        return { cityId: this.cityId, proKey: this.cityProName.string, proVal: propVal };
    }

    deleteProperty() {
        delete mapData.cityDetails[this.cityId][this.proKey];
        this.node.removeFromParent();
    }
}
