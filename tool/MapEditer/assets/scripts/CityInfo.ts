import { _decorator, Component, Node, Label, instantiate, EditBox, Toggle, Button } from "cc";
import { CityPropertyItem } from "./CityPropertyItem";
import { CityInfoData, mapData } from "./MapData";
const { ccclass, property } = _decorator;

@ccclass("CityInfo")
export class CityInfo extends Component {
    @property({ type: Node, displayName: "属性列表" })
    cityPropList!: Node;

    @property({ type: Node, displayName: "动态属性列表" })
    hasPropList!: Node;

    @property({ type: Node, displayName: "士兵动态属性列表" })
    soldierPropList!: Node;

    @property({ type: Node, displayName: "资源动态属性列表" })
    resPropList!: Node;

    @property({ type: Node, displayName: "属性模板" })
    tempPropertyItem!: Node;

    @property({ type: Node, displayName: "展示属性模板" })
    showPropertyItem!: Node;

    cityId!: string;
    onLoad() {}

    start() {
        // [3]
    }

    setData(cityId: string) {
        this.soldierPropList.removeAllChildren();
        this.resPropList.removeAllChildren();
        this.cityId = cityId;
        const cityInfo = mapData.cityDetails[cityId];
        this.addFixedProperty("cityId", cityId);
        this.addFixedProperty("cityPos", mapData.terrianList[cityId].pos);
        this.addFixedProperty("cityName", cityInfo && cityInfo["cityName"] ? cityInfo["cityName"].proVal : "");

        this.hasPropList.children.forEach((child) => (child.active = false));
        let idx = 0;
        for (const key in cityInfo) {
            if (key !== "cityId" && key !== "cityPos" && key !== "cityName") {
                const proInfo = cityInfo[key];
                let proItem = this.hasPropList.children[idx];
                if (!proItem) {
                    proItem = instantiate(this.showPropertyItem);
                    proItem.parent = this.hasPropList;
                }
                proItem.active = true;
                proItem.getChildByName("btn_remove")!.on("click", () => {
                    this.removePropItem(proItem, key);
                });
                this.setPropertyValue(proItem, proInfo);
                idx++;
            }
        }
    }

    addFixedProperty(proKey: string, propVal: any) {
        const propItemNode = this.cityPropList.getChildByName(proKey);
        if (propItemNode) {
            propItemNode.getComponent(CityPropertyItem)?.setData(this.cityId, proKey, propVal);
        }
    }

    addPropItem(event: Event, type: string) {
        let parentContainer: Node;
        if (type === "soldier") {
            parentContainer = this.soldierPropList;
        } else {
            parentContainer = this.resPropList;
        }
        const propItemNode = instantiate(this.tempPropertyItem);
        propItemNode.active = true;
        propItemNode.parent = parentContainer;
    }

    // 保存属性
    saveProps() {
        const propItemNode = this.cityPropList.getChildByName("cityName");
        if (propItemNode) {
            const { cityId, proKey, proVal } = propItemNode.getComponent(CityPropertyItem)!.getPropKeyValue();
            this.addSaveProp(proKey, proVal, 0, 0);
        }

        this.soldierPropList.children.forEach((propItemNode) => {
            const { propKey, proVal, speed, limit } = this.getPropertyValue(propItemNode);
            this.addSaveProp(propKey, proVal, speed, limit);
        });

        this.resPropList.children.forEach((propItemNode) => {
            const { propKey, proVal, speed, limit } = this.getPropertyValue(propItemNode);
            this.addSaveProp(propKey, proVal, speed, limit);
        });
    }

    getPropertyValue(propItemNode: Node) {
        const propKey = propItemNode.getChildByName("name")!.getComponent(EditBox)!.string;
        const propVal = propItemNode.getChildByName("pro_val")!.getComponent(EditBox)!.string;
        const speed = propItemNode.getChildByName("speed")!.getComponent(EditBox)!.string;
        const limit = propItemNode.getChildByName("limit")!.getComponent(EditBox)!.string;
        return { propKey: propKey, proVal: Number(propVal), speed: Number(speed), limit: Number(limit) };
    }

    setPropertyValue(propItemNode: Node, cityPropInfo: any) {
        propItemNode.getChildByName("name")!.getComponent(Label)!.string = cityPropInfo.proKey;
        propItemNode.getChildByName("pro_val")!.getComponent(Label)!.string = cityPropInfo.proVal;
        propItemNode.getChildByName("speed")!.getComponent(Label)!.string = cityPropInfo.speed;
        propItemNode.getChildByName("limit")!.getComponent(Label)!.string = cityPropInfo.limit;
    }

    removePropItem(propItemNode: Node, propKey: string) {
        propItemNode.removeFromParent();
        delete mapData.cityDetails[this.cityId][propKey];
    }

    addSaveProp(propKey: string, propVal: string | number, speed: number, limit: number) {
        if (propKey === "" || propVal === "") {
            return;
        }
        if (propVal === 0 && speed === 0) {
            return; // 无初始值，并且不增长
        }
        if (!mapData.cityDetails[this.cityId]) {
            mapData.cityDetails[this.cityId] = {};
            mapData.cityDetails[this.cityId][propKey] = {
                proKey: propKey,
                proVal: propVal,
                speed: speed,
                limit: limit,
            };
        } else {
            mapData.cityDetails[this.cityId][propKey] = {
                proKey: propKey,
                proVal: propVal,
                speed: speed,
                limit: limit,
            };
        }
    }

    // update (deltaTime: number) {
    //     // [4]
    // }
}
