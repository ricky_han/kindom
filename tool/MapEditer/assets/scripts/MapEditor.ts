import { _decorator, Component, Node, resources, SpriteFrame, instantiate, Sprite, sys, Label } from "cc";
import { eventCenter, EVENT_NAME } from "./EventCenter";
const { ccclass, property } = _decorator;

@ccclass("MapEditor")
export class MapEditor extends Component {
    @property({
        type: Node,
        displayName: "地图容器",
    })
    mapContent: Node | undefined;

    @property({
        type: Node,
        displayName: "地图cell菜单",
    })
    mapCellContent!: Node;

    @property({
        type: Node,
        displayName: "城池cell菜单",
    })
    cityCellContent!: Node;

    onLoad() {
        if (sys.isNative) {
            var writeable_path = jsb.fileUtils.getWritablePath();
            console.log("writeable_path:" + writeable_path);
            this.node.getChildByName("Label")!.getComponent(Label)!.string = `${writeable_path}`;
        }
    }

    start() {
        // [3]
        eventCenter.on(EVENT_NAME.SELECTED_TEMP_CELL, (selectedNode: Node) => {
            this.mapCellContent.children.forEach((child) => {
                if (child !== selectedNode) {
                    child.getChildByName("selected")!.active = false;
                }
            }, this);
            this.cityCellContent.children.forEach((child) => {
                if (child !== selectedNode) {
                    child.getChildByName("selected")!.active = false;
                }
            }, this);
        });
    }

    onDestroy() {
        eventCenter.off(EVENT_NAME.SELECTED_TEMP_CELL);
        eventCenter.targetOff(this);
    }

    private loadMapRes() {
        resources.loadDir("textures/map", SpriteFrame, null, (err, spriteFrames) => {
            spriteFrames.forEach((spriteFrame, idx) => {
                let parentContent = this.mapCellContent;
                let type = "0";
                if (spriteFrame.name.indexOf("terrian") !== -1) {
                    parentContent = this.cityCellContent;
                    type = spriteFrame.name.split("-")[1];
                }

                let mapCellSprite = parentContent.children[idx];
                if (!mapCellSprite) {
                    mapCellSprite = instantiate(parentContent.children[0]);
                    mapCellSprite.parent = parentContent;
                }
                mapCellSprite.active = true;
                mapCellSprite.getComponent(Sprite)!.spriteFrame = spriteFrame;
                mapCellSprite.getChildByName("name")!.getComponent(Label)!.string = `${type}`;
                mapCellSprite.on(Node.EventType.TOUCH_START, () => {
                    if (mapCellSprite.getChildByName("selected")!.active) {
                        mapCellSprite.getChildByName("selected")!.active = false;
                        eventCenter.emit(EVENT_NAME.CANCEL_TEMP_CELL);
                    } else {
                        mapCellSprite.getChildByName("selected")!.active = true;
                        eventCenter.emit(EVENT_NAME.SELECTED_TEMP_CELL, mapCellSprite);
                    }
                });
            });
        });
    }

    showMenu() {
        this.node.getChildByName("MainMenu")!.active = !this.node.getChildByName("MainMenu")!.active;
    }
}
