import { _decorator, Component, Node, instantiate, Vec2, UITransform, Vec3, v3, JsonAsset, SpriteFrame, Sprite, Label, Prefab, v2 } from "cc";
import { DEBUG } from "cc/env";
import { eventCenter, EVENT_NAME } from "./EventCenter";
import { mapData, MAP_CELL_NUM, MAP_CELL_SIZE } from "./MapData";
import { ResourceManager } from "./ResourceManager";
import { twoPointDegree } from "./uitils";
const { ccclass, property } = _decorator;

@ccclass("MapView")
export class MapView extends Component {
    @property({ type: Node, displayName: "背景容器" })
    mapBgContent!: Node;

    @property({ type: Node, displayName: "城池容器" })
    mapCityContent!: Node;

    @property({ type: Prefab, displayName: "城池预制" })
    cityPrefab!: Prefab;

    private currentSelectedCity: Node | undefined; // 当前选中城池

    start() {
        // [3]
        mapData.loadMapJson(() => {
            this.initMapView();
        });
    }

    initMapView() {
        // 背景
        for (let i = 0; i < MAP_CELL_NUM.tileRow; i++) {
            for (let j = 0; j < MAP_CELL_NUM.tileColno; j++) {
                const bgIdx = i * MAP_CELL_NUM.tileColno + j;
                const blockName = i + "-" + j;
                let bgBlockNode = this.mapBgContent.children[bgIdx];
                if (!bgBlockNode) {
                    bgBlockNode = instantiate(this.mapBgContent.children[0]);
                    bgBlockNode.parent = this.mapBgContent;
                }
                bgBlockNode.position = v3(j * MAP_CELL_SIZE.tileWidth, i * MAP_CELL_SIZE.tileHeight, 0);
                // console.log(i + "-" + j, j * MAP_CELL_SIZE.tileWidth, i * MAP_CELL_SIZE.tileHeight);
                if (DEBUG) {
                    bgBlockNode.getChildByName("block_num")!.getComponent(Label)!.string = blockName;
                }
                const resName = mapData.bgBlockList[blockName] ? mapData.bgBlockList[blockName] : "block" + (MAP_CELL_NUM.tileRow - i) + "_" + (j + 1);
                ResourceManager.getInstance()
                    .loadPath<SpriteFrame>("textures/map/" + resName + "/spriteFrame")
                    .then((sprFame: SpriteFrame) => {
                        bgBlockNode.getComponent(Sprite)!.spriteFrame = sprFame;
                        mapData.bgBlockList[blockName] = sprFame!.name;
                    });
            }
        }

        // 城池
        for (const cityId in mapData.terrianList) {
            const cityData = mapData.terrianList[cityId];
            const [cityX, cityY] = cityData.pos.split("-");
            this.addCity(cityId, cityData.type, v2(Number(cityX), Number(cityY)));
        }
    }

    private addCity(cityName: string, type: number, cityPos: Vec2) {
        const cityNode = instantiate(this.cityPrefab);
        cityNode.name = cityName;
        cityNode.parent = this.mapCityContent;
        cityNode.position = v3(cityPos.x, cityPos.y, 0);
        cityNode.getChildByName("name")!.getComponent(Label)!.string = `${cityName}`;
        ResourceManager.getInstance()
            .loadPath<SpriteFrame>("textures/map/terrian-" + type + "/spriteFrame")
            .then((sprFame: SpriteFrame) => {
                cityNode.getComponent(Sprite)!.spriteFrame = sprFame;
            });
        cityNode.on(Node.EventType.TOUCH_START, () => {
            if (this.currentSelectedCity) {
                if (this.currentSelectedCity !== cityNode) {
                    this.currentSelectedCity.getChildByName("selected")!.active = false;
                    this.currentSelectedCity = undefined;
                } else {
                    this.currentSelectedCity.getChildByName("selected")!.active = false;
                    this.currentSelectedCity = undefined;
                }
                eventCenter.emit(EVENT_NAME.SHOW_CITY_INFO, this.currentSelectedCity, false);
            } else {
                this.currentSelectedCity = cityNode;
                cityNode.getChildByName("selected")!.active = true;
                eventCenter.emit(EVENT_NAME.SHOW_CITY_INFO, this.currentSelectedCity, true);
            }
        });
    }

    private statGame() {
        // this.army
    }
}
