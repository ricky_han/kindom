import { _decorator, Component, Node, EditBox } from "cc";
import { CityInfo } from "./CityInfo";
import { Dijkstra } from "./Dijkstra";
import { eventCenter, EVENT_NAME } from "./EventCenter";
import { mapData } from "./MapData";
import { saveData, saveForBrowser } from "./uitils";
const { ccclass, property } = _decorator;

@ccclass("CityEditor")
export class CityEditor extends Component {
    @property({ type: EditBox, displayName: "起始城池id" })
    startCityInput!: EditBox;

    @property({ type: EditBox, displayName: "目标城池id" })
    endCityInput!: EditBox;

    @property({ type: Node, displayName: "城市信息" })
    cityInfoNode!: Node;

    onLoad() {
        eventCenter.on(EVENT_NAME.SHOW_CITY_INFO, this.showHideCityInfo, this);

        mapData.loadCityJson();
    }

    start() {
        // [3]
    }

    onDestroy() {
        eventCenter.targetOff(this);
    }

    showHideCityInfo(cityNode: Node, isShow: boolean) {
        this.cityInfoNode.active = isShow;
        if (isShow) {
            this.cityInfoNode.getComponent(CityInfo)!.setData(cityNode.name);
        } else {
            this.cityInfoNode.getComponent(CityInfo)!.saveProps();
        }
    }

    /**
     *  保存城池数据
     */

    saveCityData() {
        const cityStr = JSON.stringify(mapData.cityDetails);
        console.log("citys:", cityStr);
        saveData(cityStr, "city.json");
    }

    showMenu() {
        this.node.getChildByName("MainMenu")!.active = !this.node.getChildByName("MainMenu")!.active;
    }
}
