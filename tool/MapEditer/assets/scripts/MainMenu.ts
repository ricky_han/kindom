import { _decorator, Component, Node, director } from "cc";
const { ccclass, property } = _decorator;

@ccclass("MainMenu")
export class MainMenu extends Component {
    // [1]
    // dummy = '';

    // [2]
    // @property
    // serializableDummy = 0;

    start() {
        // [3]
    }

    // 切换场景
    changeScene(event: Event, sceneName: string) {
        director.loadScene(sceneName);
    }
}
