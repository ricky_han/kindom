import { _decorator, Component, Node, Sprite, UITransform, v3, Vec3 } from "cc";
import { eventCenter, EVENT_NAME } from "./EventCenter";
import { mapData } from "./MapData";
import { Toast } from "./Toast";
const { ccclass, property } = _decorator;

export enum ArmyState {
    GARRISON,
    MOVE,
}
@ccclass("Army")
export class Army extends Component {
    @property({ type: Sprite, displayName: "首领形象" })
    avatar!: Sprite;

    currentArmyCityId!: string;
    armyState = ArmyState.GARRISON;
    armyNum = 0;
    armySpeed = 50;

    armySpeedX = 0;
    armySpeedY = 0;

    targetCity!: { cityId: string; pos: Vec3 };
    onLoad() {
        eventCenter.on(EVENT_NAME.SHOW_CITY_INFO, (cityNode: Node, isShow: boolean) => {
            if (isShow) {
                this.goCity(cityNode);
            }
        });
    }

    onDestroy() {
        eventCenter.targetOff(this.node);
    }

    start() {
        // [3]
    }

    setData(cityId: string, armyNum: number) {
        this.currentArmyCityId = cityId;
        this.armyNum = armyNum;
    }

    // 前往城池
    goCity(cityNode: Node) {
        if (this.armyState === ArmyState.MOVE) {
            return;
        }
        if (!this.currentArmyCityId) {
            console.error("状态有误");
            return;
        }
        if (cityNode) {
            const toCityId = cityNode.name;
            let cityLine = mapData.terrianLines[this.currentArmyCityId + "-" + toCityId];
            if (!cityLine) {
                cityLine = mapData.terrianLines[toCityId + "-" + this.currentArmyCityId];
                if (!cityLine) {
                    Toast.showToast(null, "城市间不联通:" + this.currentArmyCityId + "-" + toCityId);
                    console.error("城市间不联通:", this.currentArmyCityId, toCityId);
                    return;
                } else {
                    const cityLineInfo = { startPos: cityLine.toPos, toPos: cityLine.startPos, len: cityLine.len };
                    this.startMoveToTargetCity(toCityId, cityLineInfo);
                }
            } else {
                this.startMoveToTargetCity(toCityId, cityLine);
            }
        }
    }

    startMoveToTargetCity(toCityId: string, cityLine: { startPos: string; toPos: string; len: number }) {
        this.armyState = ArmyState.MOVE;
        const speedTime = cityLine.len / this.armySpeed;
        const fromCityPosXY = cityLine.startPos.split("-");
        const toCityPosXY = cityLine.toPos.split("-");
        this.armySpeedX = (Number(toCityPosXY[0]) - Number(fromCityPosXY[0])) / speedTime;
        this.armySpeedY = (Number(toCityPosXY[1]) - Number(fromCityPosXY[1])) / speedTime;

        this.targetCity = { cityId: toCityId, pos: v3(Number(toCityPosXY[0]), Number(toCityPosXY[1]), 0) };
    }

    // 兵力比拼
    battleWithTargetCity() {
        const targetCityArmyNum = mapData.cityDetails[this.currentArmyCityId].armyNum;
        if (targetCityArmyNum > this.armyNum) {
            Toast.showToast(null, "势力不敌对方");
            this.node.removeFromParent();
        } else {
            Toast.showToast(null, "胜利");
            this.armyNum += targetCityArmyNum;
        }
    }

    update(dt: number) {
        if (this.armyState === ArmyState.MOVE) {
            const orinPos = this.node.position;
            if (Math.abs(this.targetCity.pos.x - orinPos.x) <= Math.abs(this.armySpeedX * dt) || Math.abs(this.targetCity.pos.y - orinPos.y) <= Math.abs(this.armySpeedY * dt)) {
                this.node.position = this.targetCity.pos;
                this.armyState = ArmyState.GARRISON;
                this.currentArmyCityId = this.targetCity.cityId;
                this.battleWithTargetCity();
            } else {
                this.node.position = this.node.position.clone().add(v3(this.armySpeedX * dt, this.armySpeedY * dt, 0));
            }
        }
    }
}
