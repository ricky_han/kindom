import { _decorator, Node, EventTarget, Component } from "cc";

export const EVENT_NAME = {
    SELECTED_TEMP_CELL: "SELECTED_TEMP_CELL",
    CANCEL_TEMP_CELL: "CANCEL_TEMP_CELL",
    LINE_CITY_TO_CITY: "LINE_CITY_TO_CITY",
    SHOW_CITY_INFO: "SHOW_CITY_INFO",
};

class EventCenter {
    private eventTarget = new EventTarget();

    static instance = new EventCenter();
    on(eventName: string, callBack: (...any: any) => void, target?: Object) {
        this.eventTarget.on(eventName, callBack, target);
    }

    emit(eventName: string, ...args: any) {
        this.eventTarget.emit(eventName, ...args);
    }

    off(eventName: string) {
        this.eventTarget.off(eventName);
    }

    targetOff(target: Object) {
        this.eventTarget.targetOff(target);
    }
}

export const eventCenter = EventCenter.instance;
