import { _decorator, Component, Node, Prefab, v2, v3, Label, sys } from "cc";
import { Army } from "./Army";
import { CityInfoData, mapData } from "./MapData";
import { saveData, saveForBrowser } from "./uitils";
const { ccclass, property } = _decorator;

@ccclass("BattleMap")
export class BattleMap extends Component {
    // [1]
    // dummy = '';
    @property({ type: Node, displayName: "部队层" })
    armyLayer!: Node;

    @property({ type: Prefab, displayName: "部队预制" })
    armyPrefab!: Prefab;

    start() {
        if (sys.isNative) {
            var writeable_path = jsb.fileUtils.getWritablePath();
            console.log("writeable_path:" + writeable_path);
            this.node.getChildByName("Label")!.getComponent(Label)!.string = `${writeable_path}`;
        }
    }

    // 随机出生部队在随机城市
    bornArmy() {
        this.armyPrefab.createNode((err: Error | null, node: Node) => {
            if (!err) {
                node.parent = this.armyLayer;

                const cityId: string = "4"; //mapData.getRandomCity();

                const cityPosXY = mapData.terrianList[cityId].pos.split("-");
                node.position = v3(Number(cityPosXY[0]), Number(cityPosXY[1]), 0);
                console.log("randomCityId:", cityId, node.position.x, node.position.y);
                const bornCityArmyNum = mapData.cityDetails[cityId].armyNum;
                const bornArmy: Army = node.getComponent(Army)!;
                bornArmy.setData(cityId, bornCityArmyNum);
            }
        });
    }

    showMenu() {
        this.node.getChildByName("MainMenu")!.active = !this.node.getChildByName("MainMenu")!.active;
    }
}
