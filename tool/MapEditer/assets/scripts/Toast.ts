import { director, instantiate, NodePool, Prefab, _decorator, Node, Animation, Label } from "cc";
import { ResourceManager } from "./ResourceManager";

export class Toast {
    private static toastPool: NodePool = new NodePool();

    static async showToast(parent: Node | null, tipStr: string) {
        let uiNode = Toast.toastPool.get();
        const toastParentNode: Node | null = parent;
        if (!uiNode) {
            const uiPrefab = await ResourceManager.getInstance().loadPath<Prefab>("prefabs/toast");
            if (uiPrefab) {
                uiNode = instantiate(uiPrefab);
                uiNode.active = true;
            } else {
                return;
            }
        }
        uiNode.getChildByName("lab_content")!.getComponent(Label)!.string = `${tipStr}`;
        uiNode.parent = toastParentNode ? toastParentNode : (director.getScene()!.getChildByName("Canvas")! as Node);
        const toastAnim = uiNode!.getComponent(Animation)!;
        toastAnim.on(Animation.EventType.FINISHED, () => {
            // 动画结束
            this.toastPool.put(uiNode!);
        });
        toastAnim.play();
    }

    static hideToast() {}
}
