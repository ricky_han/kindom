"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const prettier = require("prettier");
const UnderscoreString = require("underscore.string");
const fsp = require("./helpers/fs-promises");
const log = require("./helpers/log");
const CellValue = require("./xlsx-cell-value");
const Importer = require("./xlsx-importer");
const isAlphanumeric = require("is-alphanumeric");
const notepack = require("notepack.io");
const xlsxDeclareFilename = "xlsx.d.ts";
let xlsxSheetTypeDeclareContent = "";
let xlsxDeclareContent = "";
const xlsxTsFilename = "xlsx-l10n.ts";
let xlsxTsContent = "export const initL10nForXlsx = () => {\n";
const xlsxDataFilename = "xlsx.msgpack";
let xlsxDataObject = {};
const prettierOptionsForTs = {
    printWidth: 120,
    tabWidth: 4,
    useTabs: true,
    semi: true,
    trailingComma: "none",
    arrowParens: "always",
    parser: "typescript",
    proseWrap: "always"
};
const columnIsExportableInJs = (column) => {
    if (Importer.ExportTarget.None == column.exportTarget) {
        return false;
    }
    if (column.i18nPairNo > 0 && column.isI18nPairKey) {
        return true;
    }
    return 1 == column.indexNo || Importer.ExportTarget.ClientOnly != column.exportTarget;
};
const columnIsExportableInDeclare = (column, sheet) => {
    if (column.i18nPairNo > 0 && !column.isI18nPairKey) {
        const keyColNo = sheet.i18nPairs.get(column.i18nPairNo).keyColumnNo;
        return columnIsExportableInJs(sheet.columns[keyColNo]);
    }
    return columnIsExportableInJs(column);
};
const addSheet = (sheet) => {
    try {
        const sheetName = sheet.name;
        const className = UnderscoreString.classify(sheetName);
        const underscoredName = UnderscoreString.underscored(sheetName).toUpperCase();
        log.i(`Admin：${className} & ${underscoredName}`);
        let fileContent = "";
        let propertyCount = 0;
        let idIsString = false;
        // 生成定义
        fileContent += `// Generated from ${sheet.originalFilename}\n\n`;
        fileContent += `declare type ${className} = {`;
        for (const [columnNo, column] of sheet.columns.entries()) {
            if (0 === columnNo) {
                continue;
            }
            if (!columnIsExportableInDeclare(column, sheet)) {
                if (1 === columnNo) {
                    log.w(`ID列不导出，本表无效。`);
                    return;
                }
                else {
                    continue;
                }
            }
            if (1 === columnNo) {
                if (CellValue.Type.String === column.type) {
                    idIsString = true;
                }
            }
            else {
                // ID列不在属性列表中，但是计入属性数量，这样允许只有ID列的表
                if (column.description.length > 0) {
                    fileContent += `\t/**\n`;
                    fileContent += `\t * ${column.description.replace("\n", "")}\n`;
                    fileContent += `\t */\n`;
                }
                if (isAlphanumeric(column.name)) {
                    fileContent += `readonly ${column.name}: ${CellValue.typeToTsTypeString(column.type)};\n\n`;
                }
                else {
                    fileContent += `readonly ["${column.name}"]: ${CellValue.typeToTsTypeString(column.type)};\n\n`;
                }
            }
            propertyCount++;
        }
        fileContent += `};\n\n`;
        if (propertyCount <= 0) {
            log.w(`${sheetName} 没有任何可导出属性。`);
            return;
        }
        xlsxSheetTypeDeclareContent += fileContent;
        fileContent = undefined;
        // 生成根节点的属性声明
        xlsxDeclareContent += `\n\t/**\n`;
        xlsxDeclareContent += `\t * Generated from ${sheet.originalFilename}\n`;
        xlsxDeclareContent += `\t */\n`;
        if (idIsString) {
            xlsxDeclareContent += `\treadonly ${underscoredName}: { readonly [id: string]: ${className}; };\n`;
        }
        else {
            xlsxDeclareContent += `\treadonly ${underscoredName}: { readonly [id: number]: ${className}; };\n`;
        }
        // 生成JS数据
        xlsxDataObject[underscoredName] = {};
        for (const rowData of sheet.data.values()) {
            let id;
            const obj = {};
            for (const [columnNo, cellData] of rowData.entries()) {
                if (0 === columnNo) {
                    continue;
                }
                if (1 === columnNo) {
                    id = String(cellData);
                    continue;
                }
                const column = sheet.columns[columnNo];
                if (!columnIsExportableInJs(column)) {
                    continue;
                }
                obj[column.name] = cellData;
            }
            xlsxDataObject[underscoredName][id] = obj;
        }
        // 生成国际化列的getter
        if (0 === sheet.i18nPairs.size) {
            return;
        }
        xlsxTsContent += `\n\nfor (const key in XLSX.${underscoredName}) {\n`;
        xlsxTsContent += `if (key) {`;
        xlsxTsContent += `Object.defineProperties(XLSX.${underscoredName}[key], {\n`;
        for (const i18nPair of sheet.i18nPairs.values()) {
            const keyColumnName = sheet.columns[i18nPair.keyColumnNo].name;
            const valueColumnName = sheet.columns[i18nPair.valueColumnNo].name;
            xlsxTsContent += `${JSON.stringify(valueColumnName)}: {
					get: function () {
						return $l10n(this[${JSON.stringify(keyColumnName)}]);
					}
				},`;
        }
        xlsxTsContent += `});}}`;
    }
    catch (err) {
        log.e(err);
    }
};
const localesDeclareFilename = "locales.d.ts";
let localesDeclareContent = "";
const localesDataFilename = "locales.msgpack";
let localesDataObject = {};
const addLocale = (localeMap, localeName) => {
    try {
        // 将zh_CN等转为zh-cn
        localeName = UnderscoreString.dasherize(localeName).toLowerCase();
        if (0 === localesDeclareContent.length) {
            localesDeclareContent += `declare let currentLocale: `;
        }
        else {
            localesDeclareContent += "|";
        }
        localesDeclareContent += `${JSON.stringify(localeName)}`;
        localesDataObject[localeName] = {};
        localeMap.forEach((value, key) => {
            localesDataObject[localeName][key] = value.local;
        });
    }
    catch (err) {
        log.e(err);
    }
};
exports.generate = async (adminSrcDir) => {
    try {
        const sheets = Importer.sheets;
        const zhCnMap = Importer.zhCnMap;
        const zhTwMap = Importer.l10nMaps["zh-tw"];
        const tsDir = path.join(adminSrcDir, "app", "xlsx");
        const msgpackDir = path.join(adminSrcDir, "assets", "xlsx");
        // 保证输出目录存在
        await Promise.all([fsp.mkdirp(tsDir), fsp.mkdirp(msgpackDir)]);
        // 移除旧文件
        await Promise.all([
            fsp.del(path.join(tsDir, xlsxDeclareFilename)),
            fsp.del(path.join(tsDir, xlsxTsFilename)),
            fsp.del(path.join(msgpackDir, xlsxDataFilename)),
            fsp.del(path.join(tsDir, localesDeclareFilename)),
            fsp.del(path.join(msgpackDir, localesDataFilename))
        ]);
        // 配置表初始化
        xlsxDeclareContent = `declare const XLSX: {\ngenerationDate: Date,\n`;
        xlsxDataObject.generationDate = new Date();
        // 配置表逐个加入
        const sheetNames = [];
        for (const sheetName of sheets.keys()) {
            sheetNames.push(sheetName);
        }
        sheetNames.sort();
        sheetNames.forEach(sheetName => {
            addSheet(sheets.get(sheetName));
        });
        // 配置表解析结束
        xlsxDeclareContent += `};`;
        xlsxTsContent += "};";
        // 本地化逐个加入
        addLocale(zhCnMap, "zh-cn");
        addLocale(zhTwMap, "zh-tw");
        // 本地化结束
        localesDeclareContent += `;\n\ndeclare const $l10n: (stringId: string) => string;`;
        // 写文件
        await Promise.all([
            fsp.writeFile(path.join(tsDir, xlsxDeclareFilename), prettier.format(xlsxSheetTypeDeclareContent + xlsxDeclareContent, prettierOptionsForTs)),
            fsp.writeFile(path.join(tsDir, xlsxTsFilename), prettier.format(xlsxTsContent, prettierOptionsForTs)),
            fsp.writeFile(path.join(msgpackDir, xlsxDataFilename), notepack.encode(xlsxDataObject)),
            fsp.writeFile(path.join(tsDir, localesDeclareFilename), prettier.format(localesDeclareContent, prettierOptionsForTs)),
            fsp.writeFile(path.join(msgpackDir, localesDataFilename), notepack.encode(localesDataObject))
        ]);
    }
    catch (err) {
        log.e(err);
    }
};
//# sourceMappingURL=admin-code-generator.js.map