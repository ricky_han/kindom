"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const path = require("path");
const commandLineArgs = require("command-line-args");
const commandLineUsage = require("command-line-usage");
const log = require("./helpers/log");
const xlsx_file_list_1 = require("./xlsx-file-list");
const XlsxImporter = require("./xlsx-importer");
const JsonImporter = require("./json-importer");
const ClientCodeGenerator = require("./client-code-generator");
const ServerCodeGenerator = require("./server-code-generator");
const AdminCodeGenerator = require("./admin-code-generator");
const l10n_file_generator_1 = require("./l10n-file-generator");
const opencc = require("node-opencc");
const IS_WINDOWS = "Windows_NT" == os.type();
const IS_MAC = "Darwin" == os.type();
const IS_LINUX = "Linux" == os.type();
const CONSOLE_ENCODING = IS_WINDOWS ? "gbk" : "utf8";
const ROOT_PATH = path.join(__dirname, "..");
const PACKAGE_INFO = require(path.join(ROOT_PATH, "package.json"));
const APP_NAME = PACKAGE_INFO.fullName;
const APP_VERSION = PACKAGE_INFO.version;
const APP_DESCRIPTION = PACKAGE_INFO.description;
const optionDefinitions = [
    {
        name: "version",
        alias: "v",
        type: Boolean,
        description: `显示版本号。`
    },
    {
        name: "help",
        alias: "h",
        type: Boolean,
        description: `显示此帮助。`
    },
    {
        name: "xlsx-dir",
        alias: "x",
        type: String,
        typeLabel: "{underline xlsx directory path}",
        description: "XLSX文件所在目录。"
    },
    {
        name: "client-cfg-dir",
        type: String,
        typeLabel: "{underline client config directory path}",
        description: "客户端配置目录。"
    },
    {
        name: "server-cfg-dir",
        type: String,
        typeLabel: "{underline server config directory path}",
        description: "服务器端配置目录。"
    },
    {
        name: "client-locales-dir",
        type: String,
        typeLabel: "{underline client locales directory path}",
        description: "客户端本地化文件目录。"
    },
    {
        name: "server-locales-dir",
        type: String,
        typeLabel: "{underline server locales directory path}",
        description: "服务器本地化文件目录。"
    },
    {
        name: "admin-sources-dir",
        alias: "a",
        type: String,
        typeLabel: "{underline admin sources directory path}",
        description: "管理后台代码目录。"
    },
    {
        name: "json-dir",
        alias: "j",
        type: String,
        // multiple: true,
        typeLabel: "{underline json files directory path}",
        description: "JSON格式的配置文件目录。"
    }
];
const sections = [
    {
        header: APP_NAME,
        content: ["Ver. " + APP_VERSION, APP_DESCRIPTION]
    },
    {
        header: "选项",
        optionList: optionDefinitions,
        group: ["_none"]
    }
];
const printLogTitle = (title) => {
    log.yellowBright(`▶▶▶▶▶▶ ${title} ◀◀◀◀◀◀`);
};
(async () => {
    try {
        // 打印版本号和使用帮助
        log.green(commandLineUsage(sections));
        // 配置相关变量
        let xlsxDir = undefined;
        let clientCfgDir = undefined;
        let serverCfgDir = undefined;
        let clientLocalesDir = undefined;
        let serverLocalesDir = undefined;
        let adminSourcesDir = undefined;
        let jsonDir;
        let options = undefined;
        try {
            options = commandLineArgs(optionDefinitions);
        }
        catch (err) {
            log.e(err);
        }
        // 显示版本号或显示帮助则直接退出
        if (options && (options.version || options.help)) {
            return;
        }
        // 解析启动参数
        printLogTitle("启动参数");
        if (options) {
            log.i(options);
            if (options["xlsx-dir"]) {
                xlsxDir = options["xlsx-dir"];
                if (0 == xlsxDir.length) {
                    xlsxDir = undefined;
                }
            }
            if (options["client-cfg-dir"]) {
                clientCfgDir = options["client-cfg-dir"];
                if (0 == clientCfgDir.length) {
                    clientCfgDir = undefined;
                }
            }
            if (options["server-cfg-dir"]) {
                serverCfgDir = options["server-cfg-dir"];
                if (0 == serverCfgDir.length) {
                    serverCfgDir = undefined;
                }
            }
            if (options["client-locales-dir"]) {
                clientLocalesDir = options["client-locales-dir"];
                if (0 == clientLocalesDir.length) {
                    clientLocalesDir = undefined;
                }
            }
            if (options["server-locales-dir"]) {
                serverLocalesDir = options["server-locales-dir"];
                if (0 == serverLocalesDir.length) {
                    serverLocalesDir = undefined;
                }
            }
            if (options["admin-sources-dir"]) {
                adminSourcesDir = options["admin-sources-dir"];
                if (0 == adminSourcesDir.length) {
                    adminSourcesDir = undefined;
                }
            }
            if (options["json-dir"]) {
                jsonDir = options["json-dir"];
                if (0 == jsonDir.length) {
                    jsonDir = undefined;
                }
            }
        }
        else {
            log.e("无启动参数。");
            return;
        }
        if (!xlsxDir || !clientCfgDir || !serverCfgDir || !clientLocalesDir || !serverLocalesDir) {
            log.e(`缺少必需的启动参数。`);
            return;
        }
        log.i(`Excel配置表目录：${xlsxDir} 。`);
        await xlsx_file_list_1.findXlsxFilesInDir(xlsxDir);
        // 导入
        printLogTitle("导入");
        await XlsxImporter.doImportXlsxFiles(xlsx_file_list_1.xlsxFiles);
        if (jsonDir) {
            await JsonImporter.doImport(jsonDir);
        }
        await XlsxImporter.doImportL10nFiles(xlsx_file_list_1.l10nFiles);
        // 翻译繁中
        const zhCnMap = XlsxImporter.zhCnMap;
        const zhTwMap = XlsxImporter.l10nMaps["zh-tw"];
        zhCnMap.forEach((value, key) => {
            const zhTwValue = zhTwMap.get(key);
            // 如果繁中版本的简中与之前不同，则强制重新转换
            if (value.local.length > 0 && (!zhTwValue || !zhTwValue.local || zhTwValue["zh-cn"] !== value.local)) {
                zhTwMap.set(key, {
                    "zh-cn": value.local,
                    local: opencc
                        .simplifiedToTaiwanWithPhrases(value.local)
                        .replace(/\r/g, "")
                        .trim()
                });
                if (zhTwValue) {
                    log.redBright(`由于简中变更 ${zhTwValue["zh-cn"]} -> ${value.local} , 繁中被自动重置。`);
                }
            }
        });
        // 导出
        printLogTitle("导出");
        const promises = [
            ClientCodeGenerator.generate(clientCfgDir, clientLocalesDir),
            ServerCodeGenerator.generate(serverCfgDir, serverLocalesDir),
            l10n_file_generator_1.generateL10nFiles(xlsxDir)
        ];
        if (adminSourcesDir) {
            promises.push(AdminCodeGenerator.generate(adminSourcesDir));
        }
        await Promise.all(promises);
        // 完成
        printLogTitle("完成");
    }
    catch (err) {
        log.e(err);
    }
})();
//# sourceMappingURL=index2.js.map