"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const JSON5 = require("json5");
const prettier = require("prettier");
const UnderscoreString = require("underscore.string");
const fsp = require("./helpers/fs-promises");
const log = require("./helpers/log");
const CellValue = require("./xlsx-cell-value");
const Importer = require("./xlsx-importer");
const isAlphanumeric = require("is-alphanumeric");
const xlsxDeclareFilename = "xlsx.d.ts";
let xlsxSheetTypeDeclareContent = "";
let xlsxDeclareContent = "";
const xlsxJsFilename = "xlsx.js";
let xlsxJsContent = "";
const xlsxJsContentByFirstLetter = {};
const prettierOptionsForTs = {
    printWidth: 120,
    tabWidth: 4,
    useTabs: true,
    semi: true,
    trailingComma: "none",
    arrowParens: "always",
    parser: "typescript",
    proseWrap: "always"
};
const columnIsExportable = (column) => {
    if (Importer.ExportTarget.None == column.exportTarget) {
        return false;
    }
    if (Importer.ExportTarget.ClientOnly == column.exportTarget) {
        return false;
    }
    return true;
};
/*
declare type StarReward = {
    readonly chapterId: string;
};

declare const XLSX: {
    readonly STAR_REWARD: {
        readonly [id: string]: StarReward;
    };
    readonly STAR_REWARD_ARRAY: StarReward[];
};

XLSX = {
    starReward: {}
};

declare const LOCALES: {
    readonly ["zh-cn"]: {
        readonly [id: string]: string
    }
}
*/
const addSheet = (sheet) => {
    try {
        const sheetName = sheet.name;
        const className = UnderscoreString.classify(sheetName);
        const underscoredName = UnderscoreString.underscored(sheetName).toUpperCase();
        log.i(`服务器：${className} & ${underscoredName}`);
        let declareContent = "";
        let propertyCount = 0;
        let idIsString = false;
        // 生成定义
        declareContent += `// Generated from ${sheet.originalFilename}\n\n`;
        declareContent += `declare type ${className} = {`;
        for (const [columnNo, column] of sheet.columns.entries()) {
            if (0 === columnNo) {
                continue;
            }
            if (!columnIsExportable(column)) {
                if (1 === columnNo) {
                    log.w(`ID列不导出，本表无效。`);
                    return;
                }
                else {
                    continue;
                }
            }
            if (1 === columnNo) {
                if (CellValue.Type.String === column.type) {
                    idIsString = true;
                }
            }
            else {
                // ID列不在属性列表中，但是计入属性数量，这样允许只有ID列的表
                if (column.description.length > 0) {
                    declareContent += `\t/**\n`;
                    declareContent += `\t * ${column.description.replace("\n", "")}\n`;
                    declareContent += `\t */\n`;
                }
                if (isAlphanumeric(column.name)) {
                    declareContent += `readonly ${column.name}: ${CellValue.typeToTsTypeString(column.type)};\n\n`;
                }
                else {
                    declareContent += `readonly ["${column.name}"]: ${CellValue.typeToTsTypeString(column.type)};\n\n`;
                }
            }
            propertyCount++;
        }
        declareContent += `};\n\n`;
        if (propertyCount <= 0) {
            log.w(`${sheetName} 没有任何可导出属性。`);
            return;
        }
        xlsxSheetTypeDeclareContent += declareContent;
        declareContent = undefined;
        // 生成根节点的属性声明
        xlsxDeclareContent += `\n\t/**\n`;
        xlsxDeclareContent += `\t * Generated from ${sheet.originalFilename}\n`;
        xlsxDeclareContent += `\t */\n`;
        if (idIsString) {
            xlsxDeclareContent += `\treadonly ${underscoredName}: { readonly [id: string]: ${className}; };\n`;
        }
        else {
            xlsxDeclareContent += `\treadonly ${underscoredName}: { readonly [id: number]: ${className}; };\n`;
        }
        // 生成数据
        let jsContent = "";
        jsContent += `\n\t/**\n`;
        jsContent += `\t * Generated from ${sheet.originalFilename}\n`;
        jsContent += `\t */\n`;
        jsContent += `\tglobal.XLSX.${underscoredName}={\n`;
        for (const rowData of sheet.data.values()) {
            const obj = {};
            for (const [columnNo, cellData] of rowData.entries()) {
                if (0 === columnNo) {
                    continue;
                }
                if (1 === columnNo) {
                    jsContent += `[${JSON.stringify(cellData)}]: `;
                    continue;
                }
                const column = sheet.columns[columnNo];
                if (!columnIsExportable(column)) {
                    continue;
                }
                obj[column.name] = cellData;
            }
            jsContent += `${JSON5.stringify(obj)},\n`;
        }
        jsContent += "};\n";
        const firstLetter = underscoredName.slice(0, 1).toLowerCase();
        if (!xlsxJsContentByFirstLetter[firstLetter]) {
            xlsxJsContentByFirstLetter[firstLetter] = jsContent;
            xlsxJsContent += `\nrequire(\"./xlsx-${firstLetter}\");`;
        }
        else {
            xlsxJsContentByFirstLetter[firstLetter] += jsContent;
        }
    }
    catch (err) {
        log.e(err);
    }
};
const localesDeclareFilename = "locales.d.ts";
let localesDeclareContent = "";
const localesJsFilename = "locales.js";
let localesJsContent = "";
/*
declare const LOCALES: {
    readonly ["zh-cn"]: {
        readonly [id: string]: string
    }
};

LOCALES = {
    ["zh-cn"]: {
        ["A"]: "b"
    }
};
*/
const addLocale = (localeMap, localeName) => {
    try {
        // 将zh_CN等转为zh-cn
        localeName = UnderscoreString.dasherize(localeName).toLowerCase();
        localesDeclareContent += `\n\treadonly [${JSON.stringify(localeName)}]: {\n`;
        localesDeclareContent += `\treadonly [id: string]: string\n`;
        localesDeclareContent += `\t},`;
        localesJsContent += `[${JSON.stringify(localeName)}]: {`;
        const keys = [];
        for (const key of localeMap.keys()) {
            keys.push(key);
        }
        keys.sort();
        keys.forEach(key => {
            const value = localeMap.get(key).local;
            localesJsContent += `\n\t[${JSON.stringify(key)}]: ${JSON.stringify(value)},`;
        });
        localesJsContent += `\n\t},\n\n`;
    }
    catch (err) {
        log.e(err);
    }
};
exports.generate = async (serverCfgDir, serverLocalesDir) => {
    try {
        const sheets = Importer.sheets;
        const zhCnMap = Importer.zhCnMap;
        const zhTwMap = Importer.l10nMaps["zh-tw"];
        // 保证输出目录存在
        await Promise.all([fsp.mkdirp(serverCfgDir), fsp.mkdirp(serverLocalesDir)]);
        // 移除旧文件
        await Promise.all([
            fsp.del(path.join(serverCfgDir, xlsxDeclareFilename)),
            fsp.del(path.join(serverCfgDir, xlsxJsFilename)),
            fsp.del(path.join(serverLocalesDir, localesDeclareFilename)),
            fsp.del(path.join(serverLocalesDir, localesJsFilename))
        ]);
        // 配置表初始化
        xlsxDeclareContent = `declare const XLSX: {\ngenerationDate: Date,\n`;
        xlsxJsContent = `global.XLSX = {\ngenerationDate: new Date(${Date.now()}),\n`;
        // 增加查找函数
        let comment = `\n\t/**\n`;
        comment += `\t * 将参数以“-”连接作为ID在指定sheet中找到匹配的行\n`;
        comment += `\t * @param sheet XLSX中的表\n`;
        comment += `\t * @param args 将被“-”连接的参数\n`;
        comment += `\t * @returns 匹配行的数据\n`;
        comment += `\t */\n`;
        xlsxJsContent += comment;
        xlsxJsContent += `findOne: function (sheet) {
		var args = [];
		for (var _i = 1; _i < arguments.length; _i++) {
			args[_i - 1] = arguments[_i];
		}
		var key = args.join("-");
		return sheet[key];
		}\n`;
        xlsxJsContent += `};\n`;
        // 配置表逐个加入
        const sheetNames = [];
        for (const sheetName of sheets.keys()) {
            sheetNames.push(sheetName);
        }
        sheetNames.sort();
        sheetNames.forEach(sheetName => {
            addSheet(sheets.get(sheetName));
        });
        // 配置表解析结束
        xlsxDeclareContent += comment;
        xlsxDeclareContent += `readonly findOne: (sheet: any, ...args: any[]) => any;\n`;
        xlsxDeclareContent += `};`;
        // 本地化初始化
        localesDeclareContent += `declare const LOCALES: {`;
        localesJsContent += `global.LOCALES = {`;
        // 本地化逐个加入
        addLocale(zhCnMap, "zh-cn");
        addLocale(zhTwMap, "zh-tw");
        // 本地化结束
        localesDeclareContent += `};`;
        localesJsContent += `};`;
        // 写文件
        const promises = [];
        promises.push(fsp.writeFile(path.join(serverCfgDir, xlsxDeclareFilename), prettier.format(xlsxSheetTypeDeclareContent + xlsxDeclareContent, prettierOptionsForTs)), fsp.writeFile(path.join(serverCfgDir, xlsxJsFilename), prettier.format(xlsxJsContent, prettierOptionsForTs)), fsp.writeFile(path.join(serverLocalesDir, localesDeclareFilename), prettier.format(localesDeclareContent, prettierOptionsForTs)), fsp.writeFile(path.join(serverLocalesDir, localesJsFilename), prettier.format(localesJsContent, prettierOptionsForTs)));
        for (const firstLetter in xlsxJsContentByFirstLetter) {
            promises.push(fsp.writeFile(path.join(serverCfgDir, `xlsx-${firstLetter}.js`), prettier.format(xlsxJsContentByFirstLetter[firstLetter], prettierOptionsForTs)));
        }
        await Promise.all(promises);
    }
    catch (err) {
        log.e(err);
    }
};
//# sourceMappingURL=server-code-generator.js.map