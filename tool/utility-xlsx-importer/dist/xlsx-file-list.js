"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fsp = require("./helpers/fs-promises");
const log = require("./helpers/log");
exports.xlsxFiles = new Set();
exports.l10nFiles = new Set();
exports.findXlsxFilesInDir = async (xlsxDir) => {
    try {
        const SUBFILES = await fsp.readDir(xlsxDir);
        for (const SUBFILE of SUBFILES) {
            if (SUBFILE.startsWith(".")) {
                continue;
            }
            try {
                const FULL_PATH = path.join(xlsxDir, SUBFILE);
                const STATS = await fsp.getFsStat(FULL_PATH);
                const IS_DIR = STATS.isDirectory();
                const IS_FILE = STATS.isFile();
                if (IS_DIR) {
                    await exports.findXlsxFilesInDir(FULL_PATH);
                }
                else if (IS_FILE) {
                    const extname = path.extname(SUBFILE);
                    if (".xlsx" == extname) {
                        // 排除旧版的L10n文件
                        if (SUBFILE.toLowerCase().startsWith(`l10n.`)) {
                            exports.l10nFiles.add(FULL_PATH);
                        }
                        else {
                            exports.xlsxFiles.add(FULL_PATH);
                        }
                    }
                }
            }
            catch (err) {
                log.e(err);
            }
        }
    }
    catch (err) {
        log.e(err);
    }
};
//# sourceMappingURL=xlsx-file-list.js.map