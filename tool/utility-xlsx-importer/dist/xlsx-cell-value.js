"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const json5 = require("json5");
const log = require("./helpers/log");
const toNormalizedString = (str) => {
    if (typeof str != "string") {
        return JSON.stringify(str);
    }
    return str
        .replace(/\r/g, "")
        .replace("_x000D_", "")
        .trim();
};
exports.asJson = (cell) => {
    const cellText = toNormalizedString(cell.text);
    if (0 == cellText.length) {
        return undefined;
    }
    try {
        return json5.parse(toNormalizedString(cell.text));
    }
    catch (err) {
        log.e(err);
        return undefined;
    }
};
exports.asString = (cell) => {
    return toNormalizedString(cell.text);
};
exports.asNumber = (cell) => {
    try {
        const finalNumber = Number(toNormalizedString(cell.text));
        if (isNaN(finalNumber)) {
            log.e(`${cell.text} at ${cell.$col$row}: not a number.`);
            return 0;
        }
        else {
            return finalNumber;
        }
    }
    catch (err) {
        log.e(`${cell.text} at ${cell.$col$row}: ${err}`);
        return 0;
    }
};
exports.asNullableInt = (cell) => {
    try {
        const finalNumber = parseInt(toNormalizedString(cell.text));
        if (isNaN(finalNumber)) {
            log.d(`${cell.text} at ${cell.$col$row}: not a number.`);
            return null;
        }
        else {
            return finalNumber;
        }
    }
    catch (err) {
        log.d(`${cell.text} at ${cell.$col$row}: ${err}`);
        return null;
    }
};
exports.asBoolean = (cell) => {
    try {
        const lowerString = toNormalizedString(cell.text).toLowerCase();
        if ("" == lowerString ||
            "false" == lowerString ||
            "0" == lowerString ||
            "null" == lowerString ||
            "undefined" == lowerString) {
            return false;
        }
        else {
            return Boolean(toNormalizedString(cell.text));
        }
    }
    catch (err) {
        log.e(err);
        return false;
    }
};
const parseStringAsStringArray = (str, sep) => {
    if (0 === str.length) {
        return [];
    }
    if (!sep) {
        sep = "|";
    }
    let strings = str.split(sep);
    for (const key in strings) {
        strings[key] = strings[key].trim();
    }
    return strings;
};
const parseStringAsNumberArray = (str, sep) => {
    const strings = parseStringAsStringArray(str, sep);
    let numbers = [];
    for (const key in strings) {
        const num = Number(strings[key]);
        if (isNaN(num)) {
            log.e(`${strings[key]}: not a number.`);
            numbers[key] = 0;
        }
        else {
            numbers[key] = num;
        }
    }
    return numbers;
};
const parseStringAsBooleanArray = (str, sep) => {
    const strings = parseStringAsStringArray(str, sep);
    let booleans = [];
    for (const key in strings) {
        booleans[key] = Boolean(strings[key]);
    }
    return booleans;
};
exports.asStringArray = (cell) => {
    return parseStringAsStringArray(exports.asString(cell));
};
exports.asNumberArray = (cell) => {
    return parseStringAsNumberArray(exports.asString(cell));
};
exports.asBooleanArray = (cell) => {
    return parseStringAsBooleanArray(exports.asString(cell));
};
exports.asStringArray2D = (cell) => {
    const lev1arr = exports.asStringArray(cell);
    const ret = [];
    for (const key in lev1arr) {
        ret[key] = parseStringAsStringArray(lev1arr[key], "`");
    }
    return ret;
};
exports.asNumberArray2D = (cell) => {
    const lev1arr = exports.asStringArray(cell);
    const ret = [];
    for (const key in lev1arr) {
        ret[key] = parseStringAsNumberArray(lev1arr[key], "`");
    }
    return ret;
};
exports.asBooleanArray2D = (cell) => {
    const lev1arr = exports.asStringArray(cell);
    const ret = [];
    for (const key in lev1arr) {
        ret[key] = parseStringAsBooleanArray(lev1arr[key], "`");
    }
    return ret;
};
var Type;
(function (Type) {
    Type[Type["Json"] = 0] = "Json";
    Type[Type["Number"] = 1] = "Number";
    Type[Type["String"] = 2] = "String";
    Type[Type["Boolean"] = 3] = "Boolean";
    Type[Type["NumberArray"] = 4] = "NumberArray";
    Type[Type["StringArray"] = 5] = "StringArray";
    Type[Type["BooleanArray"] = 6] = "BooleanArray";
    Type[Type["NumberArray2D"] = 7] = "NumberArray2D";
    Type[Type["StringArray2D"] = 8] = "StringArray2D";
    Type[Type["BooleanArray2D"] = 9] = "BooleanArray2D";
})(Type = exports.Type || (exports.Type = {}));
const typeToTsTypeStringMap = new Map([
    [Type.Json, "any"],
    [Type.Number, "number"],
    [Type.String, "string"],
    [Type.Boolean, "boolean"],
    [Type.NumberArray, "number[]"],
    [Type.StringArray, "string[]"],
    [Type.BooleanArray, "boolean[]"],
    [Type.NumberArray2D, "number[][]"],
    [Type.StringArray2D, "string[][]"],
    [Type.BooleanArray2D, "boolean[][]"]
]);
const isJsonType = (typeString) => {
    return typeString === "json";
};
const isNumberType = (typeString) => {
    return (typeString.startsWith("byte") ||
        typeString.startsWith("short") ||
        typeString.startsWith("int") ||
        typeString.startsWith("long") ||
        typeString.startsWith("float") ||
        typeString.startsWith("double") ||
        typeString.startsWith("num") ||
        false);
};
const isBooleanType = (typeString) => {
    return typeString.startsWith("bool");
};
const isArrayType = (typeString) => {
    return (typeString.endsWith("arr") ||
        typeString.endsWith("array") ||
        typeString.endsWith("arr1d") ||
        typeString.endsWith("array1d") ||
        false);
};
const isArray2DType = (typeString) => {
    return typeString.endsWith("arr2d") || typeString.endsWith("array2d") || false;
};
exports.getTypeFromTypeString = (typeString) => {
    if (!typeString || typeString.length < 1) {
        return Type.String;
    }
    typeString = typeString.toLowerCase();
    if (isJsonType(typeString)) {
        return Type.Json;
    }
    const typeStringIsNumberType = isNumberType(typeString);
    const typeStringIsBooleanType = isBooleanType(typeString);
    const typeStringIsArrayType = isArrayType(typeString);
    const typeStringIsArray2DType = isArray2DType(typeString);
    if (typeStringIsNumberType) {
        if (typeStringIsArray2DType) {
            return Type.NumberArray2D;
        }
        else if (typeStringIsArrayType) {
            return Type.NumberArray;
        }
        else {
            return Type.Number;
        }
    }
    if (typeStringIsBooleanType) {
        if (typeStringIsArray2DType) {
            return Type.BooleanArray2D;
        }
        else if (typeStringIsArrayType) {
            return Type.BooleanArray;
        }
        else {
            return Type.Boolean;
        }
    }
    if (typeStringIsArray2DType) {
        return Type.StringArray2D;
    }
    else if (typeStringIsArrayType) {
        return Type.StringArray;
    }
    else {
        return Type.String;
    }
};
exports.typeToTsTypeString = (type) => {
    if (typeToTsTypeStringMap.has(type)) {
        return typeToTsTypeStringMap.get(type);
    }
    else {
        log.e(`未知类型 ${type} 。`);
        return "any";
    }
};
//# sourceMappingURL=xlsx-cell-value.js.map