"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const fsp = require("./helpers/fs-promises");
const log = require("./helpers/log");
const xlsx_importer_1 = require("./xlsx-importer");
exports.fromMap = new Map();
const containsChinese = require("contains-chinese");
exports.jsonFiles = new Map();
exports.findJsonFilesInDir = async (jsonDir, dirRelativePath = "") => {
    try {
        const SUBFILES = await fsp.readDir(jsonDir);
        for (const SUBFILE of SUBFILES) {
            if (SUBFILE.startsWith(".")) {
                continue;
            }
            try {
                const FULL_PATH = path.join(jsonDir, SUBFILE);
                const STATS = await fsp.getFsStat(FULL_PATH);
                const IS_DIR = STATS.isDirectory();
                const IS_FILE = STATS.isFile();
                if (IS_DIR) {
                    await exports.findJsonFilesInDir(FULL_PATH, `${dirRelativePath}${SUBFILE}/`);
                }
                else if (IS_FILE) {
                    const extname = path.extname(SUBFILE);
                    if (".json" == extname && !SUBFILE.endsWith(".schema.json")) {
                        const basename = path.basename(SUBFILE, extname);
                        if ("test" != basename) {
                            exports.jsonFiles.set(`${dirRelativePath}${basename}/`, FULL_PATH);
                        }
                    }
                }
            }
            catch (err) {
                log.e(err);
            }
        }
    }
    catch (err) {
        log.e(err);
    }
};
const findChineseStringInJson = (jsonObj, relativePath = "") => {
    for (const key in jsonObj) {
        if ("trigger" === key) {
            continue;
        }
        const value = jsonObj[key];
        if (!value) {
            continue;
        }
        if ("string" === typeof value && containsChinese(value)) {
            const mapKey = `${relativePath}${key}`;
            const localString = value
                .trim()
                .replace(/\r/g, "")
                .replace(/\t/g, "");
            xlsx_importer_1.zhCnMap.set(mapKey, {
                "zh-cn": localString,
                local: localString
            });
            if ("text" === key && jsonObj["from"]) {
                exports.fromMap.set(mapKey, jsonObj["from"].trim());
            }
        }
        else if ("object" === typeof value) {
            findChineseStringInJson(value, `${relativePath}${key}/`);
        }
    }
};
exports.doImport = async (jsonDir) => {
    await exports.findJsonFilesInDir(jsonDir);
    log.w(`============ 解析JSON ============`);
    exports.jsonFiles.forEach((value, key) => {
        log.i(`------------ ${key} => ${value} ------------`);
        try {
            findChineseStringInJson(require(path.resolve(value)), "json/" + key);
        }
        catch (err) {
            log.e(err);
        }
    });
};
//# sourceMappingURL=json-importer.js.map