"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const util = require("util");
const del_1 = require("./del");
const _mkdirp = require("mkdirp");
const xlsx = require("node-xlsx");
const _copy = require("copy");
const _rmdir = require("rmdir");
const path = require("path");
exports.replaceAll = (find, replace, str) => {
    var find = find.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
    return str.replace(new RegExp(find, "g"), replace);
};
exports.readDataFromFile = util.promisify(fs.readFile);
exports.readTextFromFile = (path) => {
    return new Promise((resolve, reject) => {
        fs.readFile(path, "utf8", (err, data) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
};
exports.writeFile = (path, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, data, err => {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
};
exports.readDir = (path) => {
    return new Promise((resolve, reject) => {
        fs.readdir(path, (err, files) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(files);
            }
        });
    });
};
exports.getFsStat = (path) => {
    return new Promise((resolve, reject) => {
        fs.stat(path, (err, stats) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(stats);
            }
        });
    });
};
exports.isFile = async (path) => {
    try {
        return (await exports.getFsStat(path)).isFile();
    }
    catch (err) {
        return false;
    }
};
exports.isDirectory = async (path) => {
    try {
        return (await exports.getFsStat(path)).isDirectory();
    }
    catch (err) {
        return false;
    }
};
exports.rename = (oldPath, newPath) => {
    return new Promise((resolve, reject) => {
        fs.rename(oldPath, newPath, err => {
            if (err) {
                reject(err);
            }
            else {
                resolve();
            }
        });
    });
};
exports.unlink = util.promisify(fs.unlink);
exports.mkdirp = (dir) => {
    return new Promise((resolve, reject) => {
        _mkdirp(dir, (err, made) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(made);
            }
        });
    });
};
exports.rmdir = (path) => {
    return new Promise((resolve, reject) => {
        _rmdir(path, (err, dirs, files) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(dirs.concat(files));
            }
        });
    });
};
exports.copy = (src, dst) => {
    return new Promise((resolve, reject) => {
        _copy(src, dst, (err, files) => {
            if (err) {
                reject(err);
            }
            else {
                resolve(files);
            }
        });
    });
};
exports.del = (_path) => {
    return del_1._del(_path, { force: true });
};
//递归创建目录 同步方法
exports.mkdirsSync = (dirname) => {
    if (fs.existsSync(dirname)) {
        return true;
    }
    else {
        if (exports.mkdirsSync(path.dirname(dirname))) {
            console.log("mkdirsSync = " + dirname);
            fs.mkdirSync(dirname);
            return true;
        }
    }
};
exports.copyDirFiles = (src, dist) => {
    var paths = fs.readdirSync(src);
    paths.forEach(function (p) {
        var _src = src + "/" + p;
        var _dist = dist + "/" + p;
        var stat = fs.statSync(_src);
        if (stat.isFile()) {
            // 判断是文件还是目录
            fs.writeFileSync(_dist, fs.readFileSync(_src));
        }
        else if (stat.isDirectory()) {
            exports.copyDir(_src, _dist); // 当是目录是，递归复制
        }
    });
};
/*
 * 复制目录、子目录，及其中的文件
 * @param src {String} 要复制的目录
 * @param dist {String} 复制到目标目录
 */
exports.copyDir = (src, dist) => {
    var b = fs.existsSync(dist);
    console.log("dist = " + dist);
    if (!b) {
        console.log("mk dist = ", dist);
        exports.mkdirsSync(dist); //创建目录
    }
    console.log("_copy start");
    _copy(src, dist);
};
// 导出excel
exports.writeXls = (datas, exlName) => {
    var buffer = xlsx.build([{ name: "Groups", data: datas }]);
    fs.writeFileSync(exlName + ".csv", buffer, "binary");
};
// 查找子串出现的次数
exports.findSunStrCount = (source, target) => {
    let count = 0;
    //每次查询开始的位置
    let fromIndex = 0;
    while ((fromIndex = source.indexOf(target, fromIndex)) != -1) {
        count++;
        fromIndex = fromIndex + target.length;
    }
    return count;
};
//# sourceMappingURL=fs-promises.js.map