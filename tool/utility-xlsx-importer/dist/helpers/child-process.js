"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const childProcess = require("child_process");
const util = require("util");
exports.exec = util.promisify(childProcess.exec);
exports.execFile = util.promisify(childProcess.execFile);
//# sourceMappingURL=child-process.js.map