"use strict";
// ====================================
// 提供方便的延时函数
// ====================================
Object.defineProperty(exports, "__esModule", { value: true });
exports.ms = (milliseconds) => {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, milliseconds);
    });
};
exports.s = (seconds) => {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, seconds * 1000);
    });
};
//# sourceMappingURL=delay.js.map