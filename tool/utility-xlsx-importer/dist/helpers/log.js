"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const clc = require("cli-color");
const log4js = require("log4js");
const logger = log4js.getLogger();
logger.level = "all";
const objectStringify = (obj) => {
    return 'object' === typeof obj ? JSON.stringify(obj, null, '  ') : obj;
};
exports.setLoggerLevel = (level) => {
    logger.level = level;
};
exports.configure = (config) => {
    /*
    log4js.configure({
        appenders: {
            console: {
                type: 'console',
                layout: {
                    type: 'basic'
                }
            }
        },
        categories: { default: { appenders: ['console'], level: 'all' } }
    });
    */
    log4js.configure(config);
};
exports.f = logger.fatal.bind(logger);
exports.e = logger.error.bind(logger);
exports.w = logger.warn.bind(logger);
exports.i = logger.info.bind(logger);
exports.d = logger.debug.bind(logger);
exports.t = logger.trace.bind(logger);
exports.red = (...message) => {
    if (1 !== message.length) {
        console.log(clc.red(...message));
    }
    else {
        console.log(clc.red(objectStringify(message[0])));
    }
};
exports.redBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.redBright(...message));
    }
    else {
        console.log(clc.redBright(objectStringify(message[0])));
    }
};
exports.green = (...message) => {
    if (1 !== message.length) {
        console.log(clc.green(...message));
    }
    else {
        console.log(clc.green(objectStringify(message[0])));
    }
};
exports.greenBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.greenBright(...message));
    }
    else {
        console.log(clc.greenBright(objectStringify(message[0])));
    }
};
exports.blue = (...message) => {
    if (1 !== message.length) {
        console.log(clc.blue(...message));
    }
    else {
        console.log(clc.blue(objectStringify(message[0])));
    }
};
exports.blueBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.blueBright(...message));
    }
    else {
        console.log(clc.blueBright(objectStringify(message[0])));
    }
};
exports.yellow = (...message) => {
    if (1 !== message.length) {
        console.log(clc.yellow(...message));
    }
    else {
        console.log(clc.yellow(objectStringify(message[0])));
    }
};
exports.yellowBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.yellowBright(...message));
    }
    else {
        console.log(clc.yellowBright(objectStringify(message[0])));
    }
};
exports.white = (...message) => {
    if (1 !== message.length) {
        console.log(clc.white(...message));
    }
    else {
        console.log(clc.white(objectStringify(message[0])));
    }
};
exports.whiteBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.whiteBright(...message));
    }
    else {
        console.log(clc.whiteBright(objectStringify(message[0])));
    }
};
exports.magenta = (...message) => {
    if (1 !== message.length) {
        console.log(clc.magenta(...message));
    }
    else {
        console.log(clc.magenta(objectStringify(message[0])));
    }
};
exports.magentaBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.magentaBright(...message));
    }
    else {
        console.log(clc.magentaBright(objectStringify(message[0])));
    }
};
exports.cyan = (...message) => {
    if (1 !== message.length) {
        console.log(clc.cyan(...message));
    }
    else {
        console.log(clc.cyan(objectStringify(message[0])));
    }
};
exports.cyanBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.cyanBright(...message));
    }
    else {
        console.log(clc.cyanBright(objectStringify(message[0])));
    }
};
exports.black = (...message) => {
    if (1 !== message.length) {
        console.log(clc.black(...message));
    }
    else {
        console.log(clc.black(objectStringify(message[0])));
    }
};
exports.blackBright = (...message) => {
    if (1 !== message.length) {
        console.log(clc.blackBright(...message));
    }
    else {
        console.log(clc.blackBright(objectStringify(message[0])));
    }
};
exports.raw = console.log.bind(console);
//# sourceMappingURL=log.js.map