"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const Excel = require("exceljs");
const UnderscoreString = require("underscore.string");
const isAlphanumeric = require("is-alphanumeric");
const log = require("./helpers/log");
const CellValue = require("./xlsx-cell-value");
var ExportTarget;
(function (ExportTarget) {
    ExportTarget[ExportTarget["All"] = 0] = "All";
    ExportTarget[ExportTarget["None"] = 1] = "None";
    ExportTarget[ExportTarget["ClientOnly"] = 2] = "ClientOnly";
    ExportTarget[ExportTarget["ServerOnly"] = 3] = "ServerOnly";
})(ExportTarget = exports.ExportTarget || (exports.ExportTarget = {}));
exports.sheets2 = new Map();
exports.zhCnMap = new Map();
exports.l10nMaps = {
    en: new Map(),
    "zh-tw": new Map()
};
const camelize = (str) => {
    str = UnderscoreString.decapitalize(UnderscoreString.camelize(str));
    // ID => Id
    if (str.endsWith(`ID`)) {
        str = str.substr(0, str.length - 1) + "d";
    }
    return str;
};
const importXlsxFile = async (xlsxFile) => {
    try {
        const xlsxBasename = path.basename(xlsxFile);
        log.i(`============ ${xlsxBasename} ============`);
        const workbook = new Excel.Workbook();
        await workbook.xlsx.readFile(xlsxFile);
        // 实测以下是同步方法，请放心使用
        workbook.eachSheet((worksheet, sheetId) => {
            const originalSheetName = worksheet.name.trim();
            log.i(`------ ${originalSheetName} ------`);
            if (!originalSheetName || !isAlphanumeric(originalSheetName)) {
                log.e(`${originalSheetName} 不是合法的sheet名。`);
                return;
            }
            const worksheetName = camelize(originalSheetName);
            const columnCount = worksheet.actualColumnCount;
            const rowCount = worksheet.rowCount;
            const sheet = {
                name: worksheetName,
                originalFilename: xlsxBasename,
                columns: [null],
                i18nPairs: new Map(),
                data: new Map()
            };
            log.i(`worksheetName: ${worksheetName}`);
            log.i(`xlsxBasename: ${xlsxBasename}`);
            log.i(`columnCount: ${columnCount}`);
            log.i(`rowCount: ${rowCount}`);
            if (columnCount < 1 || rowCount < 6) {
                return;
            }
            let actualColumnCount;
            let rowNo = 0;
            let row;
            // 列名
            row = worksheet.getRow(++rowNo);
            for (let i = 1; i <= columnCount; i++) {
                let colName = CellValue.asString(row.getCell(i));
                if (1 == i) {
                    colName = colName.toLowerCase();
                    if ("id" != colName) {
                        log.e("不是配置表！");
                        return;
                    }
                }
                else {
                    // 只有命名规范的列名才视作有效列名，不规范列名按照原样导出
                    if (isAlphanumeric(colName)) {
                        colName = camelize(colName);
                    }
                }
                if (colName.length > 0) {
                    sheet.columns.push({
                        name: colName,
                        type: CellValue.Type.String,
                        exportTarget: ExportTarget.None,
                        i18nPairNo: 0,
                        isI18nPairKey: false,
                        indexNo: 0
                    });
                }
                else {
                    break;
                }
            }
            actualColumnCount = sheet.columns.length - 1; // 减1是为了减掉头部的null
            log.i(`actualColumnCount: ${actualColumnCount}`);
            if (actualColumnCount < 1 || "id" != sheet.columns[1].name) {
                log.e("不是配置表！");
                return;
            }
            // 数据类型
            row = worksheet.getRow(++rowNo);
            for (let i = 1; i <= actualColumnCount; i++) {
                sheet.columns[i].type = CellValue.getTypeFromTypeString(CellValue.asString(row.getCell(i)));
            }
            if (CellValue.Type.Number != sheet.columns[1].type && CellValue.Type.String != sheet.columns[1].type) {
                log.e("ID列不是数字或字符串！");
                return;
            }
            // 是否需要导出
            row = worksheet.getRow(++rowNo);
            let exportableColumnFound = false;
            for (let i = 1; i <= actualColumnCount; i++) {
                const columnNameLower = sheet.columns[i].name.toLowerCase();
                let exportTarget;
                // 当名为name、des等，强制不导出
                if (i > 1 &&
                    (columnNameLower.endsWith("name") ||
                        "des" == columnNameLower ||
                        columnNameLower.endsWith("description"))) {
                    exportTarget = ExportTarget.None;
                }
                else {
                    exportTarget = CellValue.asNumber(row.getCell(i));
                }
                sheet.columns[i].exportTarget = exportTarget;
                if (ExportTarget.None !== exportTarget) {
                    exportableColumnFound = true;
                }
                else if (1 === i) {
                    log.e("ID列不导出，此表无效。");
                    return;
                }
            }
            if (!exportableColumnFound) {
                log.e("没有可导出的列。");
                return;
            }
            // 翻译组
            row = worksheet.getRow(++rowNo);
            for (let i = 1; i <= actualColumnCount; i++) {
                const colNameLowerCase = sheet.columns[i].name.toLowerCase();
                const cellValue = CellValue.asNumber(row.getCell(i));
                if (cellValue > 0) {
                    const i18nPairNo = cellValue;
                    const isI18nPairKey = colNameLowerCase.endsWith("id");
                    // 如果是字符串ID而不导出，则视作不需要翻译
                    if (isI18nPairKey && ExportTarget.None == sheet.columns[i].exportTarget) {
                        continue;
                    }
                    // 如果是字符串，则强制不导出
                    if (!isI18nPairKey) {
                        sheet.columns[i].exportTarget = ExportTarget.None;
                    }
                    sheet.columns[i].i18nPairNo = i18nPairNo;
                    sheet.columns[i].isI18nPairKey = isI18nPairKey;
                    sheet.columns[i].type = CellValue.Type.String; // 国际化列，强制字符串
                    if (!sheet.i18nPairs.has(i18nPairNo)) {
                        sheet.i18nPairs.set(i18nPairNo, {});
                    }
                    const i18nPair = sheet.i18nPairs.get(i18nPairNo);
                    if (isI18nPairKey) {
                        i18nPair.keyColumnNo = i;
                    }
                    else {
                        i18nPair.valueColumnNo = i;
                    }
                }
            }
            // 检查缺失的翻译组
            const i18nPairNumbersShouldBeMoved = new Set();
            for (const [i18nPairNo, i18nPair] of sheet.i18nPairs.entries()) {
                if (!i18nPair.keyColumnNo) {
                    log.e(`翻译组 ${i18nPairNo} 缺少key！`);
                    i18nPairNumbersShouldBeMoved.add(i18nPairNo);
                    sheet.columns[i18nPair.valueColumnNo].i18nPairNo = 0;
                }
                else if (!i18nPair.valueColumnNo) {
                    log.e(`翻译组 ${i18nPairNo} 缺少value！`);
                    i18nPairNumbersShouldBeMoved.add(i18nPairNo);
                    sheet.columns[i18nPair.keyColumnNo].i18nPairNo = 0;
                    sheet.columns[i18nPair.keyColumnNo].isI18nPairKey = false;
                }
            }
            for (const i18nPairNo of i18nPairNumbersShouldBeMoved) {
                sheet.i18nPairs.delete(i18nPairNo);
            }
            // 索引列
            row = worksheet.getRow(++rowNo);
            for (let i = 1; i <= actualColumnCount; i++) {
                const indexNo = CellValue.asNumber(row.getCell(i));
                if (ExportTarget.None != sheet.columns[i].exportTarget) {
                    sheet.columns[i].indexNo = indexNo;
                }
            }
            // 列描述
            row = worksheet.getRow(++rowNo);
            for (let i = 1; i <= actualColumnCount; i++) {
                sheet.columns[i].description = CellValue.asString(row.getCell(i));
            }
            // 打印列信息
            log.cyan(`列信息：`);
            log.magenta(sheet.columns);
            log.cyan(`翻译组信息：`);
            for (const [i18nPairNo, i18nPair] of sheet.i18nPairs.entries()) {
                log.magenta(`[${i18nPairNo}]: ${i18nPair.keyColumnNo} - ${i18nPair.valueColumnNo}`);
            }
            // 单元格数据
            for (++rowNo; rowNo <= rowCount; rowNo++) {
                row = worksheet.getRow(rowNo);
                let rowData = new Array(actualColumnCount + 1);
                let rowIsInvaild = false;
                for (let i = 0; i <= actualColumnCount; i++) {
                    // 不导出的列，且非中文列，则完全不读取
                    if (0 === i ||
                        (ExportTarget.None == sheet.columns[i].exportTarget && 0 == sheet.columns[i].i18nPairNo)) {
                        rowData[i] = null;
                        continue;
                    }
                    const cell = row.getCell(i);
                    let cellValue;
                    switch (sheet.columns[i].type) {
                        case CellValue.Type.Json:
                            cellValue = CellValue.asJson(cell);
                            break;
                        case CellValue.Type.Number:
                            cellValue = 1 === i ? CellValue.asNullableInt(cell) : CellValue.asNumber(cell);
                            break;
                        case CellValue.Type.Boolean:
                            cellValue = CellValue.asBoolean(cell);
                            break;
                        case CellValue.Type.NumberArray:
                            cellValue = CellValue.asNumberArray(cell);
                            break;
                        case CellValue.Type.StringArray:
                            cellValue = CellValue.asStringArray(cell);
                            break;
                        case CellValue.Type.BooleanArray:
                            cellValue = CellValue.asBooleanArray(cell);
                            break;
                        case CellValue.Type.NumberArray2D:
                            cellValue = CellValue.asNumberArray2D(cell);
                            break;
                        case CellValue.Type.StringArray2D:
                            cellValue = CellValue.asStringArray2D(cell);
                            break;
                        case CellValue.Type.BooleanArray2D:
                            cellValue = CellValue.asBooleanArray2D(cell);
                            break;
                        default:
                            cellValue = CellValue.asString(cell);
                            break;
                    }
                    // 判断行有效性
                    if (1 == i) {
                        // 数字为空时，视作有效行已经结束
                        if (CellValue.Type.Number == sheet.columns[1].type && null === cellValue) {
                            rowIsInvaild = true;
                            break;
                        }
                        // 字符串为空时，视作有效行已经结束
                        if (CellValue.Type.String == sheet.columns[1].type && 0 === cellValue.length) {
                            rowIsInvaild = true;
                            break;
                        }
                    }
                    rowData[i] = cellValue;
                }
                if (rowIsInvaild) {
                    log.d("此行无效，本表解析停止。");
                    break;
                }
                else {
                    sheet.data.set(rowData[1], rowData);
                    for (const i18nPair of sheet.i18nPairs.values()) {
                        const zhCnKey = rowData[i18nPair.keyColumnNo];
                        const zhCnValue = rowData[i18nPair.valueColumnNo];
                        if (zhCnKey.length <= 0) {
                            log.d("字符串ID为空。");
                        }
                        else if (exports.zhCnMap.has(zhCnKey)) {
                            if (zhCnValue.length > exports.zhCnMap.get(zhCnKey).local.length) {
                                exports.zhCnMap.set(zhCnKey, {
                                    "zh-cn": zhCnValue,
                                    local: zhCnValue
                                });
                            }
                        }
                        else {
                            exports.zhCnMap.set(zhCnKey, {
                                "zh-cn": zhCnValue,
                                local: zhCnValue
                            });
                        }
                    }
                }
            }
            sheets.set(sheet.name, sheet);
        });
    }
    catch (err) {
        log.e(err);
    }
};
const importL10nFile = async (xlsxFile) => {
    try {
        const xlsxBasename = path.basename(xlsxFile, path.extname(xlsxFile));
        const locale = xlsxBasename.substr("l10n.".length).toLowerCase();
        log.w(`============ ${locale} ============`);
        const localeMap = exports.l10nMaps[locale];
        if (!localeMap) {
            log.e(`不在可导出语言范围内！`);
            return;
        }
        log.i(`============ ${xlsxBasename} ============`);
        const workbook = new Excel.Workbook();
        await workbook.xlsx.readFile(xlsxFile);
        // 实测以下是同步方法，请放心使用
        workbook.eachSheet((worksheet, sheetId) => {
            const originalSheetName = worksheet.name.trim();
            log.i(`------ ${originalSheetName} ------`);
            if (!originalSheetName) {
                log.e(`${originalSheetName} 不是合法的sheet名。`);
                return;
            }
            const worksheetName = camelize(originalSheetName);
            const columnCount = worksheet.columnCount;
            const rowCount = worksheet.rowCount;
            log.i(`worksheetName: ${worksheetName}`);
            log.i(`xlsxBasename: ${xlsxBasename}`);
            log.i(`columnCount: ${columnCount}`);
            log.i(`rowCount: ${rowCount}`);
            if (columnCount < 3 || rowCount < 2) {
                return;
            }
            for (let rowNo = 2; rowNo <= rowCount; rowNo++) {
                const row = worksheet.getRow(rowNo);
                const zhCnStr = CellValue.asString(row.getCell(1));
                const localStr = CellValue.asString(row.getCell(2));
                const id = CellValue.asString(row.getCell(3));
                if (id.length <= 0) {
                    // 视作文档已经结束
                    break;
                }
                if (localStr.length > 0 && exports.zhCnMap.has(id)) {
                    localeMap.set(id, {
                        "zh-cn": zhCnStr,
                        local: localStr
                    });
                }
            }
        });
    }
    catch (err) {
        log.e(err);
    }
};
exports.doImportXlsxFiles = async (xlsxFiles) => {
    try {
        const promises = [];
        for (const xlsxFile of xlsxFiles) {
            promises.push(importXlsxFile(xlsxFile));
        }
        await Promise.all(promises);
    }
    catch (err) {
        log.e(err);
    }
};
exports.doImportL10nFiles = async (l10nFiles) => {
    try {
        const promises = [];
        for (const l10nFile of l10nFiles) {
            promises.push(importL10nFile(l10nFile));
        }
        await Promise.all(promises);
    }
    catch (err) {
        log.e(err);
    }
};
//# sourceMappingURL=xlsx-importer.js.map