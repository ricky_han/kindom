"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const prettier = require("prettier");
const UnderscoreString = require("underscore.string");
const fsp = require("./helpers/fs-promises");
const log = require("./helpers/log");
const CellValue = require("./xlsx-cell-value");
const Importer = require("./xlsx-importer");
const isAlphanumeric = require("is-alphanumeric");
const xlsxDeclareFilename = "xlsx.d.ts";
let xlsxSheetTypeDeclareContent = "";
let xlsxDeclareContent = "";
const xlsxJsFilename = "xlsx.js";
let xlsxJsContent = "";
let xlsxGetterJsContent = "";
const prettierOptionsForTs = {
    printWidth: 120,
    tabWidth: 4,
    useTabs: true,
    semi: true,
    trailingComma: "none",
    arrowParens: "always",
    parser: "typescript",
    proseWrap: "always"
};
const columnIsExportableInJs = (column) => {
    if (Importer.ExportTarget.None == column.exportTarget) {
        return false;
    }
    if (Importer.ExportTarget.ServerOnly == column.exportTarget) {
        return false;
    }
    return true;
};
const columnIsExportableInDeclare = (column, sheet) => {
    if (column.i18nPairNo > 0 && !column.isI18nPairKey) {
        const keyColNo = sheet.i18nPairs.get(column.i18nPairNo).keyColumnNo;
        return columnIsExportableInJs(sheet.columns[keyColNo]);
    }
    return columnIsExportableInJs(column);
};
const addSheet = (sheet) => {
    try {
        const sheetName = sheet.name;
        const className = UnderscoreString.classify(sheetName);
        const underscoredName = UnderscoreString.underscored(sheetName).toUpperCase();
        log.i(`客户端：${className} & ${underscoredName}`);
        let fileContent = "";
        let propertyCount = 0;
        let idIsString = false;
        // 生成定义
        fileContent += `// Generated from ${sheet.originalFilename}\n\n`;
        fileContent += `declare type ${className} = {`;
        for (const [columnNo, column] of sheet.columns.entries()) {
            if (0 === columnNo) {
                continue;
            }
            if (!columnIsExportableInDeclare(column, sheet)) {
                if (1 === columnNo) {
                    log.w(`ID列不导出，本表无效。`);
                    return;
                }
                else {
                    continue;
                }
            }
            if (1 === columnNo) {
                if (CellValue.Type.String === column.type) {
                    idIsString = true;
                }
            }
            else {
                // ID列不在属性列表中，但是计入属性数量，这样允许只有ID列的表
                if (column.description.length > 0) {
                    fileContent += `\t/**\n`;
                    fileContent += `\t * ${column.description.replace(/\n/g, "")}\n`;
                    fileContent += `\t */\n`;
                }
                if (isAlphanumeric(column.name)) {
                    fileContent += `readonly ${column.name}: ${CellValue.typeToTsTypeString(column.type)};\n\n`;
                }
                else {
                    fileContent += `readonly ["${column.name}"]: ${CellValue.typeToTsTypeString(column.type)};\n\n`;
                }
            }
            propertyCount++;
        }
        fileContent += `};\n\n`;
        if (propertyCount <= 0) {
            log.w(`${sheetName} 没有任何可导出属性。`);
            return;
        }
        xlsxSheetTypeDeclareContent += fileContent;
        fileContent = undefined;
        // 生成根节点的属性声明
        xlsxDeclareContent += `\n\t/**\n`;
        xlsxDeclareContent += `\t * Generated from ${sheet.originalFilename}\n`;
        xlsxDeclareContent += `\t */\n`;
        if (idIsString) {
            xlsxDeclareContent += `\treadonly ${underscoredName}: { readonly [id: string]: ${className}; };\n`;
        }
        else {
            xlsxDeclareContent += `\treadonly ${underscoredName}: { readonly [id: number]: ${className}; };\n`;
        }
        // 生成JS数据
        xlsxJsContent += `\n\t/**\n`;
        xlsxJsContent += `\t * Generated from ${sheet.originalFilename}\n`;
        xlsxJsContent += `\t */\n`;
        xlsxJsContent += `\t${underscoredName}: `;
        xlsxJsContent += "{\n";
        for (const rowData of sheet.data.values()) {
            const obj = {};
            for (const [columnNo, cellData] of rowData.entries()) {
                if (0 === columnNo) {
                    continue;
                }
                if (1 === columnNo) {
                    xlsxJsContent += `[${JSON.stringify(cellData)}]: `;
                    continue;
                }
                const column = sheet.columns[columnNo];
                if (!columnIsExportableInJs(column)) {
                    continue;
                }
                obj[column.name] = cellData;
            }
            xlsxJsContent += `${JSON5.stringify(obj)},\n`;
        }
        xlsxJsContent += "},\n";
        if (0 === sheet.i18nPairs.size) {
            return;
        }
        xlsxGetterJsContent += `\n\nfor (var key in window.XLSX.${underscoredName}) {\n`;
        xlsxGetterJsContent += `Object.defineProperties(window.XLSX.${underscoredName}[key], {\n`;
        for (const i18nPair of sheet.i18nPairs.values()) {
            const keyColumnName = sheet.columns[i18nPair.keyColumnNo].name;
            const valueColumnName = sheet.columns[i18nPair.valueColumnNo].name;
            xlsxGetterJsContent += `${JSON.stringify(valueColumnName)}: {
					get: function () {
						return $l10n(this[${JSON.stringify(keyColumnName)}]);
					}
				},`;
        }
        xlsxGetterJsContent += `});}`;
    }
    catch (err) {
        log.e(err);
    }
};
const localesDeclareFilename = "locales.d.ts";
let localesDeclareContent = "";
const localesJsFilename = "locales.js";
let localesJsContent = "";
let currentLocaleDeclare = "";
/*
declare const LOCALES: {
    readonly ["zh-cn"]: {
        readonly [id: string]: string
    }
};

LOCALES = {
    ["zh-cn"]: {
        ["A"]: "b"
    }
};
*/
const addLocale = (localeMap, localeName) => {
    try {
        // 将zh_CN等转为zh-cn
        localeName = UnderscoreString.dasherize(localeName).toLowerCase();
        localesDeclareContent += `\n\treadonly [${JSON.stringify(localeName)}]: {\n`;
        localesDeclareContent += `\treadonly [id: string]: string\n`;
        localesDeclareContent += `\t},`;
        if (0 === currentLocaleDeclare.length) {
            currentLocaleDeclare += `declare let currentLocale: `;
        }
        else {
            currentLocaleDeclare += "|";
        }
        currentLocaleDeclare += `${JSON.stringify(localeName)}`;
        localesJsContent += `[${JSON.stringify(localeName)}]: {`;
        const keys = [];
        for (const key of localeMap.keys()) {
            keys.push(key);
        }
        keys.sort();
        keys.forEach(key => {
            const value = localeMap.get(key).local;
            localesJsContent += `\n\t[${JSON.stringify(key)}]: ${JSON.stringify(value)},`;
        });
        localesJsContent += `\n\t},\n\n`;
    }
    catch (err) {
        log.e(err);
    }
};
exports.generate = async (clientCfgDir, clientLocalesDir) => {
    try {
        const sheets = Importer.sheets;
        const zhCnMap = Importer.zhCnMap;
        const zhTwMap = Importer.l10nMaps["zh-tw"];
        // 保证输出目录存在
        await Promise.all([fsp.mkdirp(clientCfgDir), fsp.mkdirp(clientLocalesDir)]);
        // 移除旧文件
        await Promise.all([
            fsp.del(path.join(clientCfgDir, xlsxDeclareFilename)),
            fsp.del(path.join(clientCfgDir, xlsxJsFilename)),
            fsp.del(path.join(clientLocalesDir, localesDeclareFilename)),
            fsp.del(path.join(clientLocalesDir, localesJsFilename))
        ]);
        // 配置表初始化
        xlsxDeclareContent = `declare const XLSX: {\ngenerationDate: Date,\n`;
        xlsxJsContent = `window.XLSX = {\ngenerationDate: new Date(${Date.now()}),\n`;
        // 配置表逐个加入
        const sheetNames = [];
        for (const sheetName of sheets.keys()) {
            sheetNames.push(sheetName);
        }
        sheetNames.sort();
        sheetNames.forEach(sheetName => {
            addSheet(sheets.get(sheetName));
        });
        // 配置表解析结束
        let comment = `\n\t/**\n`;
        comment += `\t * 将参数以“-”连接作为ID在指定sheet中找到匹配的行\n`;
        comment += `\t * @param sheet XLSX中的表\n`;
        comment += `\t * @param args 将被“-”连接的参数\n`;
        comment += `\t * @returns 匹配行的数据\n`;
        comment += `\t */\n`;
        xlsxDeclareContent += comment;
        xlsxDeclareContent += `readonly findOne: (sheet: any, ...args: any[]) => any;\n`;
        xlsxDeclareContent += `};`;
        xlsxJsContent += comment;
        xlsxJsContent += `findOne: function (sheet) {
		var args = [];
		for (var _i = 1; _i < arguments.length; _i++) {
			args[_i - 1] = arguments[_i];
		}
		var key = args.join("-");
		return sheet[key];
		}\n`;
        xlsxJsContent += `};`;
        // 本地化初始化
        localesDeclareContent += `declare const LOCALES: {`;
        localesJsContent += `if(!window.currentLocale) {\nwindow.currentLocale = \"zh-cn\";}\n\nvar LOCALES = {`;
        // 本地化逐个加入
        addLocale(zhCnMap, "zh-cn");
        addLocale(zhTwMap, "zh-tw");
        // 本地化结束
        localesDeclareContent += `};\n\n${currentLocaleDeclare};\n\ndeclare const $l10n: (stringId: string) => string;`;
        localesJsContent += `};
		window.$l10n = function (stringId) {
			var str;

			if ("zh-cn" === currentLocale) {
				str = LOCALES["zh-cn"][stringId];
			} else {
				str = LOCALES[currentLocale][stringId] || LOCALES["zh-cn"][stringId];
			}

			if ("" === str) {
				return "";
			} else if (str){
				return str;
			} else {
				return stringId;
			}
		}`;
        // 写文件，需要避免同时写入导致Prettier内存占用过高
        await fsp.writeFile(path.join(clientCfgDir, xlsxDeclareFilename), prettier.format(xlsxSheetTypeDeclareContent + xlsxDeclareContent, prettierOptionsForTs));
        await fsp.writeFile(path.join(clientCfgDir, xlsxJsFilename), prettier.format(xlsxJsContent + xlsxGetterJsContent, prettierOptionsForTs));
        await fsp.writeFile(path.join(clientLocalesDir, localesDeclareFilename), prettier.format(localesDeclareContent, prettierOptionsForTs));
        await fsp.writeFile(path.join(clientLocalesDir, localesJsFilename), prettier.format(localesJsContent, prettierOptionsForTs));
    }
    catch (err) {
        log.e(err);
    }
};
//# sourceMappingURL=client-code-generator.js.map