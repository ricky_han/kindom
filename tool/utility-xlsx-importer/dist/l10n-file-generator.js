"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
const Excel = require("exceljs");
const log = require("./helpers/log");
const fsp = require("./helpers/fs-promises");
const xlsx_importer_1 = require("./xlsx-importer");
const json_importer_1 = require("./json-importer");
const compare = (a, b) => {
    if (a[2] < b[2]) {
        return -1;
    }
    else if (a[2] > b[2]) {
        return 1;
    }
    else {
        return 0;
    }
};
exports.generateL10nFiles = async (xlsxDir) => {
    for (const locale in xlsx_importer_1.l10nMaps) {
        const l10nMap = xlsx_importer_1.l10nMaps[locale];
        const l10nFile = path.join(xlsxDir, `l10n.${locale}.xlsx`);
        try {
            await fsp.del(l10nFile);
        }
        catch (err) {
            log.e(err);
        }
        try {
            const nowDate = new Date();
            const workbook = new Excel.Workbook();
            workbook.creator = "Van Darkholme";
            workbook.lastModifiedBy = "Van Darkholme";
            workbook.created = nowDate;
            workbook.modified = nowDate;
            workbook.lastPrinted = nowDate;
            const worksheet = workbook.addWorksheet(locale);
            const borders = {
                top: {
                    style: "thin"
                },
                left: { style: "thin" },
                bottom: { style: "thin" },
                right: { style: "thin" }
            };
            worksheet.columns = [
                {
                    header: "zh-cn",
                    width: 80,
                    style: {
                        border: borders,
                        fill: {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: {
                                argb: "FFFFAAAA"
                            }
                        }
                    }
                },
                {
                    header: locale,
                    width: 80,
                    style: {
                        border: borders,
                        fill: {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: {
                                argb: "FFAAFFAA"
                            }
                        }
                    }
                },
                {
                    header: "id",
                    width: 60,
                    style: {
                        border: borders,
                        fill: {
                            type: "pattern",
                            pattern: "solid",
                            fgColor: {
                                argb: "FFAAAAFF"
                            }
                        }
                    }
                },
                {
                    header: "from",
                    width: 30,
                    style: {
                        border: borders
                    }
                }
            ];
            const rows = [];
            xlsx_importer_1.zhCnMap.forEach((zhCnValue, id) => {
                const localStr = l10nMap.has(id) ? l10nMap.get(id).local : "";
                const row = [zhCnValue.local, localStr, id];
                if (json_importer_1.fromMap.has(id)) {
                    row.push(json_importer_1.fromMap.get(id));
                }
                rows.push(row);
            });
            rows.sort(compare);
            worksheet.addRows(rows);
            await workbook.xlsx.writeFile(l10nFile);
        }
        catch (err) {
            log.e(err);
        }
    }
};
//# sourceMappingURL=l10n-file-generator.js.map