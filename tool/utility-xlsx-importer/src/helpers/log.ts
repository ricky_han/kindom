import * as clc from 'cli-color';
import * as os from 'os';
import * as log4js from 'log4js';

const logger = log4js.getLogger();
logger.level = "all";

const objectStringify = (obj: any) => {
	return 'object' === typeof obj ? JSON.stringify(obj, null, '  ') : obj;
};

export const setLoggerLevel = (level: string) => {
	logger.level = level;
};

export const configure = (config: log4js.Configuration) => {
	/*
	log4js.configure({
		appenders: {
			console: {
				type: 'console',
				layout: {
					type: 'basic'
				}
			}
		},
		categories: { default: { appenders: ['console'], level: 'all' } }
	});
	*/
	log4js.configure(config);
};

export const f: (message: any, ...args: any[]) => void = logger.fatal.bind(logger);

export const e: (message: any, ...args: any[]) => void = logger.error.bind(logger);

export const w: (message: any, ...args: any[]) => void = logger.warn.bind(logger);

export const i: (message: any, ...args: any[]) => void = logger.info.bind(logger);

export const d: (message: any, ...args: any[]) => void = logger.debug.bind(logger);

export const t: (message: any, ...args: any[]) => void = logger.trace.bind(logger);

export const red = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.red(...message));
	} else {
		console.log(clc.red(objectStringify(message[0])));
	}
};

export const redBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.redBright(...message));
	} else {
		console.log(clc.redBright(objectStringify(message[0])));
	}
};

export const green = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.green(...message));
	} else {
		console.log(clc.green(objectStringify(message[0])));
	}
};

export const greenBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.greenBright(...message));
	} else {
		console.log(clc.greenBright(objectStringify(message[0])));
	}
};

export const blue = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.blue(...message));
	} else {
		console.log(clc.blue(objectStringify(message[0])));
	}
};

export const blueBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.blueBright(...message));
	} else {
		console.log(clc.blueBright(objectStringify(message[0])));
	}
};

export const yellow = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.yellow(...message));
	} else {
		console.log(clc.yellow(objectStringify(message[0])));
	}
};

export const yellowBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.yellowBright(...message));
	} else {
		console.log(clc.yellowBright(objectStringify(message[0])));
	}
};

export const white = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.white(...message));
	} else {
		console.log(clc.white(objectStringify(message[0])));
	}
};

export const whiteBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.whiteBright(...message));
	} else {
		console.log(clc.whiteBright(objectStringify(message[0])));
	}
};

export const magenta = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.magenta(...message));
	} else {
		console.log(clc.magenta(objectStringify(message[0])));
	}
};

export const magentaBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.magentaBright(...message));
	} else {
		console.log(clc.magentaBright(objectStringify(message[0])));
	}
};

export const cyan = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.cyan(...message));
	} else {
		console.log(clc.cyan(objectStringify(message[0])));
	}
};

export const cyanBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.cyanBright(...message));
	} else {
		console.log(clc.cyanBright(objectStringify(message[0])));
	}
};

export const black = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.black(...message));
	} else {
		console.log(clc.black(objectStringify(message[0])));
	}
};

export const blackBright = (...message: any[]) => {
	if (1 !== message.length) {
		console.log(clc.blackBright(...message));
	} else {
		console.log(clc.blackBright(objectStringify(message[0])));
	}
};

export const raw: (message?: any, ...optionalParams: any[]) => void = console.log.bind(console);