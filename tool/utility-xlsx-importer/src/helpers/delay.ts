// ====================================
// 提供方便的延时函数
// ====================================

export const ms = (milliseconds: number) => {
	return new Promise((resolve, reject) => {
		setTimeout(resolve, milliseconds);
	});
};

export const s = (seconds: number) => {
	return new Promise((resolve, reject) => {
		setTimeout(resolve, seconds * 1000);
	});
};