import * as path from 'path';

import * as fsp from './helpers/fs-promises';
import * as log from './helpers/log';

export const xlsxFiles = new Set<string>();
export const l10nFiles = new Set<string>();

export const findXlsxFilesInDir = async (xlsxDir: string) => {
	try {
		const SUBFILES = await fsp.readDir(xlsxDir);

		for (const SUBFILE of SUBFILES) {
			if (SUBFILE.startsWith(".")) {
				continue;
			}

			try {
				const FULL_PATH = path.join(xlsxDir, SUBFILE);
				const STATS = await fsp.getFsStat(FULL_PATH);
				const IS_DIR = STATS.isDirectory();
				const IS_FILE = STATS.isFile();

				if (IS_DIR) {
					await findXlsxFilesInDir(FULL_PATH);
				} else if (IS_FILE) {
					const extname = path.extname(SUBFILE);
					if (".xlsx" == extname) {
						// 排除旧版的L10n文件
						if (SUBFILE.toLowerCase().startsWith(`l10n.`)) {
							l10nFiles.add(FULL_PATH);
						} else {
							xlsxFiles.add(FULL_PATH);
						}
					}
				}
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		log.e(err);
	}
};