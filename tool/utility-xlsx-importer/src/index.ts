import * as os from "os";
import * as path from "path";
import * as process from "process";
import * as util from "util";
import * as fsp from "./helpers/fs-promises";
import * as fs from "fs";
import * as clc from "cli-color";
import * as commandLineArgs from "command-line-args";
import * as commandLineUsage from "command-line-usage";

import * as log from "./helpers/log";

import { xlsxFiles, findXlsxFilesInDir } from "./xlsx-file-list";
import * as XlsxImporter from "./importXlsx";
import * as ClientCodeGenerator from "./generator-js";
import {
	createExcelByAction,
	createPlistFile,
	createUnitFlyAction,
	createUnitXlslData,
	createUnitActionPrefab,
	createUnitPrefab
} from "./createPlistFile";

const opencc = require("node-opencc");

const IS_WINDOWS = "Windows_NT" == os.type();
const IS_MAC = "Darwin" == os.type();
const IS_LINUX = "Linux" == os.type();
const CONSOLE_ENCODING: string = IS_WINDOWS ? "gbk" : "utf8";
const ROOT_PATH = path.join(__dirname, "..");
const PACKAGE_INFO = require(path.join(ROOT_PATH, "package.json"));
const APP_NAME: string = PACKAGE_INFO.fullName;
const APP_VERSION: string = PACKAGE_INFO.version;
const APP_DESCRIPTION: string = PACKAGE_INFO.description;

const optionDefinitions = [
	{
		name: "version",
		alias: "v",
		type: Boolean,
		description: `显示版本号。`
	},
	{
		name: "help",
		alias: "h",
		type: Boolean,
		description: `显示此帮助。`
	},
	{
		name: "xlsx-dir",
		alias: "x",
		type: String,
		typeLabel: "{underline xlsx directory path}",
		description: "XLSX文件所在目录。"
	},
	{
		name: "client-cfg-dir",
		type: String,
		typeLabel: "{underline client config directory path}",
		description: "客户端配置目录。"
	},
	{
		name: "client-role-dir",
		type: String,
		typeLabel: "{underline client role directory path}",
		description: "客户端角色资源目录。"
	}
];

const sections = [
	{
		header: APP_NAME,
		content: ["Ver. " + APP_VERSION, APP_DESCRIPTION]
	},
	{
		header: "选项",
		optionList: optionDefinitions,
		group: ["_none"]
	}
];

const printLogTitle = (title: string) => {
	log.yellowBright(`▶▶▶▶▶▶ ${title} ◀◀◀◀◀◀`);
};

(async () => {
	try {
		// 打印版本号和使用帮助
		log.green(commandLineUsage(sections));

		// 配置相关变量

		let xlsxDir: string = undefined;
		let clientCfgDir: string = undefined;
		let clientLocalesDir: string = undefined;
		let clientRoleDir: string = undefined;

		let options: any = undefined;

		try {
			options = commandLineArgs(optionDefinitions);
		} catch (err) {
			log.e(err);
		}

		// 显示版本号或显示帮助则直接退出

		if (options && (options.version || options.help)) {
			return;
		}

		// 解析启动参数

		printLogTitle("启动参数");

		if (options) {
			log.i(options);

			if (options["xlsx-dir"]) {
				xlsxDir = <string>options["xlsx-dir"];

				if (0 == xlsxDir.length) {
					xlsxDir = undefined;
				}
			}

			if (options["client-cfg-dir"]) {
				clientCfgDir = <string>options["client-cfg-dir"];

				if (0 == clientCfgDir.length) {
					clientCfgDir = undefined;
				}
			}

			if (options["client-role-dir"]) {
				clientRoleDir = <string>options["client-role-dir"];
			}
		} else {
			log.e("无启动参数。");
			return;
		}

		xlsxDir = "../../design/config";
		clientCfgDir = "../../client/Kindom/assets/scripts/xlsx";
		if (!xlsxDir || !clientCfgDir) {
			log.e(`缺少必需的启动参数。`);
			return;
		}

		log.i(`Excel配置表目录：${xlsxDir} 。`);

		await findXlsxFilesInDir(xlsxDir);
		// 导入;
		printLogTitle("导入");
		await XlsxImporter.doImportXlsxFiles(xlsxFiles);
		// 导出
		printLogTitle("导出");
		const promises = [ClientCodeGenerator.generate(clientCfgDir)];
		await Promise.all(promises);
		// 完成
		printLogTitle("完成");

		// printLogTitle("生成texture文件");
		// createPlistFile(clientRoleDir);
		// 生成对应的动作配置
		// createExcelByAction(clientRoleDir);

		// // 快速生产角色预制模板;
		// createUnitActionPrefab();

		// // 生成飞行物预制
		// createUnitPrefab();

		// // 生成动作信息
		// createExcelByAction("./");

		// // 生成Unit表数据
		// createUnitXlslData("");
		// // 飞行物动画
		// createUnitFlyAction("");
	} catch (err) {
		log.e(err);
	}
})();
