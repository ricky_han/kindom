import * as fsp from "./helpers/fs-promises";
import * as log from "./helpers/log";
import * as path from "path";
import * as fs from "fs";
import json5 = require("json5");

export const createPlistFile = async (clientResDir: string, clientOutDir?: string) => {
	try {
		const SUBFILES = await fsp.readDir(clientResDir);

		for (const SUBFILE of SUBFILES) {
			if (SUBFILE.startsWith(".")) {
				continue;
			}
			try {
				const FULL_PATH = path.join(clientResDir, SUBFILE);
				const isExist = fs.existsSync(FULL_PATH);
				if (!isExist) {
					fsp.readTextFromFile(path.join(clientResDir, "hero40001.tps")).then(data => {
						data = fsp.replaceAll("hero40001", SUBFILE, data);
						fsp.writeFile(FULL_PATH + ".tps", data);
					});
					console.log("FULL_PATH:", FULL_PATH);
				}
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		console.log(err);
	}
};

// copy ActionUnit 预制模板
export const createUnitActionPrefab = async () => {
	try {
		const actionUnitPath = "../../Temple/assets/resources/images/action_unit";
		const actionUnitPrefabPath = "../../Temple/assets/resources/prefabs/action_units/";
		const SUBFILES = await fsp.readDir(actionUnitPath);
		for (const SUBFILE of SUBFILES) {
			if (!SUBFILE.endsWith(".png")) {
				continue;
			}
			try {
				const FULL_PATH = path.join(actionUnitPath, SUBFILE);
				const isExist = fs.existsSync(FULL_PATH);
				if (!isExist) {
					console.log("FULL_PATH Not Exist:", FULL_PATH);
					continue;
				}
				console.log("SUBFILE:", SUBFILE.replace(".png", ""));
				const CREATE_PREFAB_PATH = path.join(actionUnitPrefabPath, SUBFILE.replace(".png", ".prefab"));
				const PLIST_PATH = path.join(actionUnitPath, SUBFILE.replace(".png", ".plist.meta"));

				const tempPrefabPath = path.join(actionUnitPrefabPath, "hero_template.prefab");
				if (fs.existsSync(CREATE_PREFAB_PATH)) {
					console.log("exist:", CREATE_PREFAB_PATH);
				} else {
					fs.copyFile(tempPrefabPath, CREATE_PREFAB_PATH, () => {
						fsp.readTextFromFile(CREATE_PREFAB_PATH).then(data => {
							fsp.readTextFromFile(PLIST_PATH).then(plistData => {
								// console.log("plistData", plistData);
								const plistJson = json5.parse(plistData);
								const resUuid = plistJson.userData.uuid;
								// console.log("plistData", plistJson.userData.uuid);
								data = fsp.replaceAll("7ded63fc-ee82-4c15-b758-9863043d57c4", resUuid, data);
								fsp.writeFile(CREATE_PREFAB_PATH, data);
							});
						});
					});
				}
				// break;
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		console.log(err);
	}
};

// 飞行物
export const createUnitPrefab = async () => {
	try {
		const actionUnitPath = "../../Temple/assets/resources/images/unit";
		const actionUnitPrefabPath = "../../Temple/assets/resources/prefabs/units/";
		const SUBFILES = await fsp.readDir(actionUnitPath);
		for (const SUBFILE of SUBFILES) {
			if (!SUBFILE.endsWith(".png")) {
				continue;
			}
			try {
				const FULL_PATH = path.join(actionUnitPath, SUBFILE);
				const isExist = fs.existsSync(FULL_PATH);
				if (!isExist) {
					console.log("FULL_PATH Not Exist:", FULL_PATH);
					continue;
				}
				console.log("SUBFILE:", SUBFILE.replace(".png", ""));
				const CREATE_PREFAB_PATH = path.join(actionUnitPrefabPath, SUBFILE.replace(".png", ".prefab"));
				const PLIST_PATH = path.join(actionUnitPath, SUBFILE.replace(".png", ".plist.meta"));

				const tempPrefabPath = path.join(actionUnitPrefabPath, "unit_template.prefab");
				if (fs.existsSync(CREATE_PREFAB_PATH)) {
					console.log("exist:", CREATE_PREFAB_PATH);
				} else {
					fs.copyFile(tempPrefabPath, CREATE_PREFAB_PATH, () => {
						fsp.readTextFromFile(CREATE_PREFAB_PATH).then(data => {
							fsp.readTextFromFile(PLIST_PATH).then(plistData => {
								// console.log("plistData", plistData);
								const plistJson = json5.parse(plistData);
								const resUuid = plistJson.userData.uuid;
								// console.log("plistData", plistJson.userData.uuid);
								data = fsp.replaceAll("8b009e6d-d44c-4ec4-80b7-4b468cd33c29", resUuid, data);
								fsp.writeFile(CREATE_PREFAB_PATH, data);
							});
						});
					});
				}
				// break;
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		console.log(err);
	}
};
// 各个动作的帧数和名字
export const createExcelByAction = async (clientResDir: string, clientOutDir?: string) => {
	try {
		clientResDir = "../../../art/美术素材 库/角色";
		const SUBFILES = await fsp.readDir(clientResDir);
		// const exportActionExcelArray = [["id", "unitId", "action", "frames"]];
		const exportActionUnitExcelArray = [
			[
				"id",
				"name",
				"avatar",
				"type",
				"speed",
				"moveLine",
				"hp",
				"targetSide",
				"targetNum",
				"visionRange",
				"attackRange",
				"fireGap",
				"fireUnitId"
			]
		];
		// hero40001	hero40001	hero40001		40	{type:"land",range:160,toAttackTarget:true}	20	0		160	160	2	needle
		for (const SUBFILE of SUBFILES) {
			if (SUBFILE.startsWith(".") || SUBFILE.endsWith(".tps")) {
				continue;
			}
			try {
				const FULL_PATH = path.join(clientResDir, SUBFILE);
				const isDir = fsp.isDirectory(FULL_PATH);
				const unitId = SUBFILE;
				if (isDir) {
					console.log("FULL_PATH:", FULL_PATH);
					// const actionDir = await fsp.readDir(FULL_PATH);
					// for (const actionName of actionDir) {
					// 	const action_path = path.join(FULL_PATH, actionName);
					// 	// console.log("action_path:", action_path);
					// 	// if (actionName === "idle") {
					// 	// 	fsp.copyDir(action_path, path.join(FULL_PATH, "move"));
					// 	// 	fsp.copyDirFiles(action_path, path.join(FULL_PATH, "move"));
					// 	// 	// fs.copyFileSync(action_path, path.join(FULL_PATH, "move"));
					// 	// }
					// 	const actionFiles = await fsp.readDir(action_path);
					// 	const unitId = SUBFILE;
					// 	const id = unitId + "_" + actionName;
					// exportExcelArray.push([id, unitId, actionName, actionFiles.length + ""]);
					// fsp.writeXls(exportActionExcelArray);
					// }
					const rangeNum = String(Math.floor(160 + Math.random() * 160));
					exportActionUnitExcelArray.push([
						unitId,
						unitId,
						unitId,
						"",
						String(Math.floor(40 + Math.random() * 50)),
						'{type:"land",range:160,toAttackTarget:true}',
						String(Math.floor(20 + Math.random() * 20)),
						"0",
						"",
						rangeNum,
						rangeNum,
						"2",
						"needle"
					]);

					fsp.writeXls(exportActionUnitExcelArray, "unitaction");
				}
				// break;
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		console.log(err);
	}
};

// unit表数据
export const createUnitXlslData = async (clientResDir: string, clientOutDir?: string) => {
	try {
		clientResDir = "../../../art/美术素材 库/特效/ATTACK-packs";
		const SUBFILES = await fsp.readDir(clientResDir);
		const exportUnitExcelArray = [
			[
				"id",
				"name",
				"avatar",
				"type",
				"speed",
				"moveLine",
				"hp",
				"targetSide",
				"canBeAttack",
				"hitEffect",
				"hitUnitNum",
				"hitRange",
				"hitHurtHp"
			]
		];
		// needle	毒针	needle	normal	30	{type:"line"}	1	0	FALSE	red_light	0	0	0
		for (const SUBFILE of SUBFILES) {
			if (SUBFILE.startsWith(".") || !SUBFILE.endsWith(".png")) {
				continue;
			}
			try {
				const FULL_PATH = path.join(clientResDir, SUBFILE);
				const unitId = SUBFILE.replace(".png", "");
				console.log("FULL_PATH:", FULL_PATH);
				const rangeNum = String(Math.floor(160 + Math.random() * 160));
				exportUnitExcelArray.push([
					unitId,
					unitId,
					unitId,
					"normal",
					String(Math.floor(30 + Math.random() * 30)),
					'{type:"line"}',
					String(Math.floor(1 + Math.random() * 2)),
					"0",
					"FALSE",
					"red_light",
					"-1",
					String(Math.floor(32 + Math.floor(Math.random() * 2) * 32)),
					"1"
				]);

				fsp.writeXls(exportUnitExcelArray, "unit");
				// break;
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		console.log(err);
	}
};

// Unit 发射物动画
export const createUnitFlyAction = async (clientResDir: string, clientOutDir?: string) => {
	try {
		clientResDir = "../../../art/美术素材 库/特效/HIT-packs";
		const SUBFILES = await fsp.readDir(clientResDir);
		const exportActionExcelArray = [["id", "unitId", "action", "frames"]];
		for (const SUBFILE of SUBFILES) {
			if (SUBFILE.startsWith(".") || !SUBFILE.endsWith(".plist")) {
				continue;
			}
			try {
				const FULL_PATH = path.join(clientResDir, SUBFILE);
				console.log("FULL_PATH:", FULL_PATH);
				const plistDataStr: string = await fsp.readTextFromFile(FULL_PATH);
				const actionName = "hit";
				const frameCount = fsp.findSunStrCount(plistDataStr, "<key>frame</key>");
				const unitId = SUBFILE.replace(".plist", "");
				const id = unitId + "_" + actionName;
				exportActionExcelArray.push([id, unitId, actionName, frameCount + ""]);
				fsp.writeXls(exportActionExcelArray, "unit_hit");
				// break;
			} catch (err) {
				log.e(err);
			}
		}
	} catch (err) {
		console.log(err);
	}
};
